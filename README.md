[![pipeline status](https://gitlab.com/w4tweaks/wiki/badges/master/pipeline.svg)](https://gitlab.com/w4tweaks/wiki/commits/master)

# W4Tweaks Wiki

![logo](logo.png)

Перенос сайта w3d.wiki-site.com и worms3d.wiki-site.com на [MkDocs](http://www.mkdocs.org)

Этот сайт посвящён игре [Worms 3D](http://ru.wikipedia.org/wiki/Worms_3D) и [Worms 4: Mayhem](http://ru.wikipedia.org/wiki/Worms_4:_Mayhem).
