# Configuration

!!! warning "This article is unfinished"
    Work in progress.

<table>
  <tr>
    <td><code>/LOG:ALL</code></td>
    <td></td>
  </tr><tr>
    <td><code>+connect_lobby</code></td>
    <td>Steam Command line to direct to lobby</td>
  </tr><tr>
    <td><code>/RUN</code></td>
    <td></td>
  </tr><tr>
    <td><code>/CRC</code></td>
    <td></td>
  </tr><tr>
    <td><code>/DISABLEHARDWAREAA</code></td>
    <td>Hardware AA disabled, using SSAA instead</td>
  </tr><tr>
    <td><code>/FXAA</code>
    <td>NVidia FXAA enabled</td>
  </tr><tr>
    <td><code>/NOSSAA</code></td>
    <td>Super-sampled anti-aliasing disabled</td>
  </tr><tr>
    <td><code>/SSAA</code></td>
    <td>
      Super-sample size set to 4x4
      <br>Super-sample size set to 2x4
      <br>Super-sample size set to 2x2
      <br>Super-sample size set to 2x1
      <br>Unrecognised super-sampling size, super-sampled anti-aliasing disabled
    </td>
  </tr><tr>
    <td><code>/DONTOPTIMIZEATTRIBUTES</code></td>
    <td>Optimsation of attributes disabled</td>
  </tr><tr>
    <td><code>/IMPORTTGAS</code></td>
    <td>Importing TGAs and Materials</td>
  </tr><tr>
    <td><code>/EXPORTTGAS</code></td>
    <td>Exporting TGAs and Materials</td>
  </tr><tr>
    <td><code>/SEPIA</code></td>
    <td>Activate Sepia mode</td>
  </tr><tr>
    <td><code>/WIREFRAME</code></td>
    <td>Running in wireframe mode!</td>
  </tr><tr>
    <td><code>/CREATESHADOWCOLOURMAP</code></td>
    <td>Creating a colour texture for the lighting pass</td>
  </tr><tr>
    <td><code>/SHADOWMAP</code></td>
    <td>Shadow map size set to</td>
  </tr><tr>
    <td><code>/NOWORMOUTLINES</code></td>
    <td>Worm outlines disabled</td>
  </tr><tr>
    <td><code>/AMBIENTOCCLUSION</code></td>
    <td>Ambient Occlusion Enabled</td>
  </tr><tr>
    <td><code>/NOPOSTPROCESS</code></td>
    <td>Post processing disabled</td>
  </tr><tr>
    <td><code>/STARTUP</code></td>
    <td>Startup service</td>
  </tr><tr>
    <td><code>/NOHARDWARESOUND</code></td>
    <td></td>
  </tr><tr>
    <td><code>/NET_LOG</code></td>
    <td>Net log enabled</td>
  </tr><tr>
    <td><code>/AITEST</code></td>
    <td>AI test set on</td>
  </tr><tr>
    <td><code>/NOATTRACT</code></td>
    <td>No attract loop</td>
  </tr><tr>
    <td><code>/NOFRONTEND</code></td>
    <td></td>
  </tr><tr>
    <td><code>/TRIGGERSINVISIBLE</code></td>
    <td>Switch of debug display of trigger spheres.</td>
  </tr><tr>
    <td><code>/ALTJUMP</code></td>
    <td>Using alternate jumping system.</td>
  </tr><tr>
    <td><code>/SELFTEST</code></td>
    <td>Self test mode enabled.</td>
  </tr><tr>
    <td><code>/NEW_CONTROL_BOX</code></td>
    <td>New control box input configuration.</td>
  </tr><tr>
    <td><code>/GAMEOVERMENU</code></td>
    <td>FE game over menu = Expected a name for the game over menu</td>
  </tr><tr>
    <td><code>/STARTMENU</code></td>
    <td>FE start menu = Expected a name for the start menu</td>
  </tr><tr>
    <td><code>/MENU</code></td>
    <td>Loading extra menu file:</td>
  </tr><tr>
    <td><code>/WEATHER</code></td>
    <td>
      Setting weather to
      <br>Expected a name for the weather, NONE, RAIN or SNOW
    </td>
  </tr><tr>
    <td><code>/LEVELNAME</code></td>
    <td>Expected a name for the level</td>
  </tr><tr>
    <td><code>/DATABANK</code></td>
    <td></td>
  </tr><tr>
    <td><code>/SCRIPT</code></td>
    <td></td>
  </tr><tr>
    <td><code>/TIMEOFDAY</code></td>
    <td></td>
  </tr><tr>
    <td><code>/THEME</code></td>
    <td></td>
  </tr><tr>
    <td><code>/LEVEL</code></td>
    <td></td>
  </tr><tr>
    <td><code>/SHUTUP</code></td>
    <td>Worm voices disabled.</td>
  </tr><tr>
    <td><code>/NOCAMERALOAD</code></td>
    <td>Camera tweak file ignored.</td>
  </tr><tr>
    <td><code>/DELETECLOUDSAVE</code></td>
    <td>Obliterating saved game</td>
  </tr><tr>
    <td><code>/NOSOUND</code></td>
    <td>No Sound</td>
  </tr><tr>
    <td><code>/NOMUSIC</code></td>
    <td>Music off</td>
  </tr><tr>
    <td><code>/SHRINKHUD</code></td>
    <td>Shrinking the HUD, scale =</td>
  </tr><tr>
    <td><code>/ZIP</code></td>
    <td>zip files enabled</td>
  </tr><tr>
    <td><code>/GAMETIMESTAMP</code></td>
    <td>as timestamp to confirm identity of game to join</td>
  </tr><tr>
    <td><code>/GAME</code></td>
    <td>as game to host/join</td>
  </tr><tr>
    <td><code>/NAME</code></td>
    <td>GSA - Using as login nick</td>
  </tr><tr>
    <td><code>/JOIN</code></td>
    <td>GSA triggered - joining a game</td>
  </tr><tr>
    <td><code>/HOST</code></td>
    <td>GSA triggered - hosting a game</td>
  </tr><tr>
    <td><code>/ENABLEPORTFORWARDING</code></td>
    <td>Universal Plug + Play port forwarding enabled.</td>
  </tr><tr>
    <td><code>/AIMINGRINGS</code></td>
    <td>Aiming rings turned on</td>
  </tr><tr>
    <td><code>/CACHEDATA</code></td>
    <td>Caching data</td>
  </tr><tr>
    <td><code>/NODEBUGINFO</code></td>
    <td>Debugging info changed</td>
  </tr><tr>
    <td><code>/SHOWDEBUGINFO</code></td>
    <td>Showing Debug Info</td>
  </tr><tr>
    <td><code>/ALLAIPLAYERS</code></td>
    <td>All AI players set on</td>
  </tr><tr>
    <td><code>/CLIFFSTOP</code></td>
    <td>Cliff stop</td>
  </tr><tr>
    <td><code>/JUMP2</code></td>
    <td>Jump Mode 2</td>
  </tr><tr>
    <td><code>/BUILDPS2SOUND</code></td>
    <td>Build PS2 sound banks</td>
  </tr><tr>
    <td><code>/ALLOWSCREENSHOTS</code></td>
    <td>Screenshots enabled</td>
  </tr><tr>
    <td><code>/NOHUD</code></td>
    <td>HUD disabled</td>
  </tr><tr>
    <td><code>/NODEBUGUPDATE</code></td>
    <td>Debug update disabled</td>
  </tr><tr>
    <td><code>/DEBUGUPDATE</code></td>
    <td>Debug update enabled</td>
  </tr><tr>
    <td><code>/SKIPMENU</code></td>
    <td>Skipping the menu</td>
  </tr><tr>
    <td><code>/SHOWMENU</code></td>
    <td>Showing the menu</td>
  </tr><tr>
    <td><code>/AUTORUN</code></td>
    <td>AutoRunGame</td>
  </tr><tr>
    <td><code>/TIMESCALE</code></td>
    <td>Time scale set to</td>
  </tr><tr>
    <td><code>/LANDSEED</code></td>
    <td>Land Seed:</td>
  </tr><tr>
    <td><code>/GAMESEED</code></td>
    <td></td>
  </tr><tr>
    <td><code>/LOGICSEED</code></td>
    <td>Logic Seed:</td>
  </tr><tr>
    <td><code>/KEY</code></td>
    <td>Software Keyboard enabled</td>
  </tr><tr>
    <td><code>/NOMOVIES</code></td>
    <td>No movie playing!</td>
  </tr><tr>
    <td><code>/NOLOGOMOVIE</code></td>
    <td>Not Playing Logo Movies!</td>
  </tr><tr>
    <td><code>/NOTELNET</code></td>
    <td>Telnet deactivated</td>
  </tr><tr>
    <td><code>/TELNET</code></td>
    <td>Telnet activated</td>
  </tr><tr>
    <td><code>/SHOWNETBANDWIDTH</code></td>
    <td>Network bandwidth enabled</td>
  </tr><tr>
    <td><code>/NOOVERLAY</code></td>
    <td>Overlay disabled</td>
  </tr><tr>
    <td><code>/SHOWOVERLAY</code></td>
    <td>Overlay enabled</td>
  </tr><tr>
    <td><code>/AUTOSCREENSHOTS</code></td>
    <td>Auto screenshots enabled</td>
  </tr><tr>
    <td><code>/QUITAFTERBUILD</code></td>
    <td>Quit after build</td>
  </tr><tr>
    <td><code>/BUNDLE</code></td>
    <td>Using Bundles</td>
  </tr><tr>
    <td><code>/MANUALCAMERA</code></td>
    <td>Manual Camera set on</td>
  </tr><tr>
    <td><code>/INVERTMOUSETP</code></td>
    <td>Mouse Y-axis inverted for third person</td>
  </tr><tr>
    <td><code>/INVERTMOUSEFP</code></td>
    <td>Mouse Y-axis inverted for first person</td>
  </tr>
</table>
