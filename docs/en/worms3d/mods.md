# Mods

## Multiplayer Map Pack 4.0

Included:

1. PNM 4.420
2. TweakPack
3. NTXPack 1.2
4. OpenSpy Support
5. 433 maps for multiplayer

> Link for [Multiplayer Map Pack 4.0](http://www.moddb.com/mods/worms-3d-multiplayer-mappack/downloads/worms-3d-multiplayer-map-pack-v40) (197.21mb)

## NTXPack 1.2

![NTXPack12.jpg](../../assets/worms3d/NTXPack12.jpg)

List of Maps:

1.  All Systems Oh No! (WUM)
2.  Blue Dragon (WF by Woitek)
3.  Cards Of Alice (by AlexBond)
4.  Christmas House (by Woitek)
5.  Fishes (By AlexBond)
6.  Maim Frame (WUM)
7.  Monster Bash (WUM)
8.  Mordred And Morgana (WF by Woitek)
9.  Pesky Little Breeders (WUM)
10. Question of Faith (WF by Woitek)
11. Terminal Ferocity (WUM)
12. Time Flies (WUM)
13. Towering Infernal (WUM)

All Maps with original textures and look very quality. For run need in W3DLauncher select Scripts_NTXPack.xom

> [Link for NTXPack 1.2](http://yadi.sk/d/p_z4HZ6k4gnLk)

## Tweaks Pack + W3DLauncher

This TweakPack for Worms3D

![greenhud01.jpg](../../assets/worms3d/greenhud01.jpg)

Included:

- W3DLauncher 4.420

![w3dloader.png](../../assets/worms3d/w3dloader.png)

- ArmyHUD
- ArmyHUD+Sky
- DarkSky
- GreenHUD
- GreenHUD+Sky
- MKSky

> [Link on TweaksPack](http://yadi.sk/d/W1Fnc1Kq3neB4)

## PNM Packs 4.365

![PNM.jpg](../../assets/worms3d/PNM.jpg)

All version Patch's for New Maps:

> [PNM 4.250](http://www.playground.ru/files/patch_worms_3d_new_maps_v_4_250_wormpot_2-12240/) (250 maps)

> [PNM 4.315](http://yadi.sk/d/Cn76uTWJ6Sq2C) (315 maps)

> [PNM 4.365](http://yadi.sk/d/sn0cTRmq6Spoq) (365 maps)
