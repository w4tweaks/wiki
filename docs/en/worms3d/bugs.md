# Bugapedia

**Bugapedia** is the most detailed compilation of bugs from Worms 3D.

Bugs can be experienced both in Wormnet servers and during gameplay, being the latter the most common ones.

## Server Bugs

Bugs that occur in the Wormnet Lobby or inside a host room.

- **[Red Host](bugs/red-host.md)**
- **[Invisible Host](bugs/invisible-host.md)**
- **[Invisible Player](bugs/invisible-player.md)**
- **[Clockbug](bugs/clockbug.md)**
- **[Team Crash](bugs/team-crash.md)**
- **[Autocrash](bugs/autocrash.md)**

## Game Bugs

Bugs that take place during online gameplay. These bugs are classified according to the situation in which they appear.

### Visual Bugs

Bugs that cause just a visual abnormality, without affecting the gameplay.

- **[Big Hands](bugs/big-hands.md)**
- **[Hot Worm](bugs/hot-worm.md)**
- **[Mini-scope](bugs/mini-scope.md)**
- **[Fallen Parachutist](bugs/fallen-parachutist.md)**

### Rope Bugs

Bugs that occur while using Ninjarope, or as a consequence of using it.

- **[Rocketworm](bugs/rocketworm.md)**
- **Rodeo Bug**
- **Copperfield Trick**
- **Trembling Worm**
- **Extra-speed**
- **Frog Jump**
- **Sticky Ropewire**
- **Wallwalker**

### Weapon Bugs

Bugs related to the utilization of a weapon.

- **Shotgun glitch**

### Movement Bugs

Bugs that cause problems in the movement of a worm.

- **Underground Bug**
- **Suffering Worm**
- **Sticky Head**
- **Surfing Worm**

### Crashing Bugs

Bugs that cause the online game to crash or makes it impossible to continue.

- **Waterbug**
- **Spawnbug**
- **Randombug**
- **Mapbug**
- **Crash**
- **Countdown Over**
- **Eternal**
- **Splash & Crash**

### Other Bugs

- **.exe Crash**
- **Gameover Bug**
