# Autocrash

**Autocrash** is a Server Bug that occurs when a player enters a host room, causing 2 possible errors:

1. Player can be automatically kicked from the host room and returned to the lobby.
2. Player can get an .exe error that crashes the whole application.

## Causes

No specific causes are known. It's a **random error while connecting to a host**.

## Consequences

There are no further consequences to future gameplay. The player is either **kicked from the host room** or gets an **.exe error** that returns him to the O.S.

## Solutions

Since this is a random bug, and its effects don't last too long, there is **no available solution to it**.
