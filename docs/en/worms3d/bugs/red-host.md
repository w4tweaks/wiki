# Red Host

**Red Host** is a Server Bug that makes a host room to be inaccessible. It is also called *Noob host* because most of these hosts are created by novice players.

## Causes

This bug is caused by the creation of a host room **without having Worms 3D Service Patch 2** installed.

## Consequences

This bug **makes the host room totally inaccessible** to those who have already patched their version of Worms 3D.

## Solutions

The only solution to this bug is **to download Worms 3D Service Patch 2** from Worms 3D's official website and install it in the system.
