# Hot Worm

**Hot Worm** is a Visual Bug that makes a worm belch smoke from its tail while it's roping (Using the Ninjarope). 

## How to make it

Unfortunately, **little is known about this bug** because it's very rare. The smoke is supposedly made from the dust cloud effect that appears when the worm collides against a solid surface, but the exact way this visual bug is made is still a mystery. It's been suggested that this bug can be created **when the Ninjarope hook sticks a surface at the same time the worm is colliding with another wall**, but it's such an improbable situation to be checked.
