# Fallen Parachutist

**Fallen parachutist**, also called the *Helmet on the head* bug is a Visual Bug that shows the parachutist helmet on the head of a worm injured by fall.

## How to make it

This bug occurs when **a worm falls down and crashes** against the floor **in the moment the player selects parachute** (in order to save it).

It must be noticed that this bug is not very appropriate to be done, unless you want to lose one precious turn.
