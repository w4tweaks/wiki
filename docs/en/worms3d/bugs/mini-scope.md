# Mini-scope

**Mini-Scope** is a Visual Bug that alters aim scope's size when aiming with a Ninjarope, making it more precise for accurate shots. 

## How to make it

Simply **press Q repeated and insistently**, holding the key when you want to set a certain size. 

**Trick:** The faster you press the Q key, the smaller the scope will be.
