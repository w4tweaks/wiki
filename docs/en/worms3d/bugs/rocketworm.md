# Rocketworm

**Rocketworm** is a Rope Bug which has evolved within time into a technique in Ninjarope's mastery. It causes the worm to make an incredibly powerful bounce when roping against a solid surface, without injuring the worm.

Rocketworms have a wide range of intensity and can be performed vertically, horizontally or even diagonally. They're used mainly to impress another players. Nevertheless, a well-performed Rocketworm can be really useful to reach higher or distant platforms.

## How to make it

One of the main characteristics of the Rocketworm is that it's really hard to perform. Firstly, performing a Rocketworm requires more than a good level in Ninjarope's management. Besides, it requires a lot of practice and nice reflexes.

The main steps to make a Rocketworm are the following:

1. Shoot the Ninjarope against a wall (if you want to make a horizontal Rocketworm) or against the floor (if you're up to make a vertical one).
2. Start bouncing against the surface constantly, to increase the speed of the worm.
3. When you've got a good speed, left click the mouse to release the worm **in the very precise moment when the worm touches the surface**.

Trick: The longer the rope wire is when you click to release the worm, the more powerful the Rocketworm will be.
