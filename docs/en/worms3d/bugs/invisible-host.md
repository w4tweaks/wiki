# Invisible Host

**Invisible Host** (room) is a Server Bug that makes a recently-made host room to become invisible to the rest of the players.

## Causes

At the moment, **there is not any known cause for this bug**. It seems to happen only the first time a player hosts a room, and only once per player. It is often associated to a limited bandwith, but there is no evidence for this.

## Consequences

Because of this bug, **the rest of the players cannot see or enter the affected host room**, and the hosts don't even notice their host rooms are buggy.

## Solutions

The only way for the host to solve this bug is **to abort the current host room** and to make a new one, which surely won't have the same problem.
