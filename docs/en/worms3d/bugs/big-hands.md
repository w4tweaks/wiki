# Big Hands

**Big Hands** is a Visual Bug that causes a worm's hands (or hands + hold Ninjarope) to become incredibly big.

## How to make it

1. While you are falling in the air, select **Jetpack**, then quickly select **Ninjarope** and shoot it against a solid surface. Your worm should look fatter than usual (although this is not a requirement).
2. **Crash** your worm against the solid surface. The worm should fall down waving his **BIG HANDS** desperately.
