# Clockbug

**Clockbug** is a Server Bug that prevents the affected player from interacting in the host room, by showing the loading clock in the cursor.

## Causes

This bug usually appears in a player whose **bandwith is limited or overloaded**, or whose **connection with the host** is not properly set.

## Consequences

This bug needs to be solved, because **it does not allow the affected player to add team or turn the light bulb on**, and consequently, the players cannot start the game.

## Solutions

Three solutions are known for this bug, being the first one the most effective, and the last one the less effective:

1. The host or another player can solve this bug by **adding another team**.
2. This bug is solved **when someone else enters** the host room.
3. The host can also solve this bug by **changing the current map or scheme**.
