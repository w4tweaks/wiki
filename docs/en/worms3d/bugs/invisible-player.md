# Invisible Player

**Invisible Player** is a Server Bug that makes a player not to appear in the Players List inside a host room, even though that this player is actually in.

## Causes

There is no certainty about the causes of this bug, but it is known to appear **when 2 or more players enter a host room at the same time**.

## Consequences

There are **not verified** consequences of this bug in online gameplay, since it is easy to detect and it's immediately solved.

## Solutions

There is only one solution for this bug: **The invisible player (and those players who didn't see it), must leave the host room** and come back again.
