# Team Crash

**Team Crash** is a Server Bug that makes a player to be automatically
kicked from the host room when he adds a team.

## Causes

Players that experience this bug **have not installed Worms 3D Service
Patch 2**. No other causes are known.

## Consequences

There are **no further consequences** appart from being kicked from the
host room when adding a team.

## Solutions

The only solution to this bug is to **download Service Patch 2 from the
Worms 3D official website and to install it** in the system.
