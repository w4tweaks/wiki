# Wormpot 2

## Wormpot 2 Combinations

For activate New Weapons Use combinations:

<table>
<tr>
<th width="240px">Combination</th>
<th>Result</th>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot063.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot065.jpg" width="64px"></td>
<td><img src="../../assets/worms3d/wormpot2/Wormpot066.jpg" width="64px" align="right">Earthquake Mode</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot067.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot068.jpg" width="64px"></td>
<td>Drakula Mode</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot069.jpg" width="64px"></td>
<td>
<img src="../../assets/worms3d/wormpot2/Wormpot070.jpg" width="64px" align="right">
Bazooka X 5<br>
ShotGun X 4<br>
Laser Sight Uzi
</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot071.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot072.jpg" width="64px"></td>
<td>Laser (Blowpipe)</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot069.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot071.jpg" width="64px"></td>
<td>
Mega Cluster Mine (Cluster Grenade)<br>
Magic Bullet (Homing Missile)<br>
Impulse Stike (Gas Canister)
</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot073.jpg" width="64px"></td>
<td>
Zoom Camera<br>
Petrol X 5<br>
Grenade X 5<br>
Super Baseball (Banana Bomb)
</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot074.jpg" width="64px"></td>
<td>Mega Prod (Prod 10)</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot073.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot074.jpg" width="64px"></td>
<td>
Super Jar (Cluster Grenade)<br>
Cannonball (Holy Hand Grenade)<br>
Fire PetrolBomb<br>
Extra Strikes: Napalm - Mine - Canonball - Sheep - Super Baseball - Dynamite - Petrol
</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot075.jpg" width="64px"></td>
<td>
Minomet (Bazooka)<br>
Cluster Bomb X 16<br>
Mortar X 10<br>
Airstrike X 10<br>
Banana Bomb X 5
</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot075.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot076.jpg" width="64px"></td>
<td>
Super Banana Bomb (Cluster Grenade)<br>
Mega Grenade (Grenade)<br>
Cluster Sticky Bomb (Gas Canister)
</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot077.jpg" width="64px"></td>
<td>
BazookaSheep (Bazooka)<br>
Pigeon Bomb (Banana Bomb)
</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot077.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot078.jpg" width="64px"></td>
<td>Sheep Strike (Airstrike)</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot077.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot078.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot065.jpg" width="64px"></td>
<td>Donkey Day</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot079.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot074.jpg" width="64px"></td>
<td>Speed Weapons</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot079.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot080.jpg" width="64px"></td>
<td>
Repeating Strike: Cluster - Holy - Banana - Mine - Dynamite - Sheep - Petrol
</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot081.jpg" width="64px"></td>
<td>Regeneration Mode</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot063.jpg" width="64px"></td>
<td>Worm DeathBoom</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot063.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot082.jpg" width="64px"></td>
<td>Gravity X 10</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot083.jpg" width="64px"></td>
<td>Latent Weapons (SuperSheep Count)</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot083.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot084.jpg" width="64px"></td>
<td>Full Weapons (SuperSheep Count)</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot083.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot077.jpg" width="64px"></td>
<td>Donkey Mode (SuperSheep Count)</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot082.jpg" width="64px"></td>
<td>Unlimited Rope Bazooka (Dynamite)</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot085.jpg" width="64px"></td>
<td>
Unlimited Rope<br>
Zoom Rope Mode<br>
Super Speed Worm (SkipGo 10)<br>
Super JetPack
</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot085.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot086.gif" width="64px"></td>
<td>Super Length Rope</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot085.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot087.jpg" width="64px"></td>
<td><img src="../../assets/worms3d/wormpot2/Wormpot088.jpg" width="64px" align="right">Super Speed Rope</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot085.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot086.gif" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot087.jpg" width="64px"></td>
<td>Super Ninja Rope</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot089.jpg" width="64px"> <img src="../../assets/worms3d/wormpot2/Wormpot068.jpg" width="64px"></td>
<td>No «Q» mode</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot084.jpg" width="64px"></td>
<td>Unlimited time</td>
</tr>
<tr>
<td><img src="../../assets/worms3d/wormpot2/Wormpot087.jpg" width="64px"></td>
<td>Smoke Bomb (Gas Canister 3)</td>
</tr>
</table>
