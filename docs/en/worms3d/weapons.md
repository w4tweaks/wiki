# Weapons

## Weapons

With each new Worms sequel the array of weapons gets a bit more sophisticated each time, with some old items removed due to popular demand, and some new ones added.

With the launch of Worms3D everyone was expecting the new weapon roster to be something special. And we weren't disappointed! One look at the updated arsenal is enough to send any worms fan misty-eyed with anticipation.

### Bazooka

<img src="../../assets/worms3d/weapons/Bazooka.png" align="right">

The mainstay of any self-respecting Worms team, the Bazooka has the potential to unleash more than its fair share of damage when used properly.

Affected by wind, a skilful player must read the path a shell will take when fired, and adjust the trajectory accordingly.

### Grenade

<img src="../../assets/worms3d/weapons/Grenade.png" align="right">

Another weapon that no worm should be without, Grenades can reach parts other weapons can't. With their adjustable fuses Grenades can be thrown and timed to detonate after they've rolled or bounced into a hard-to-reach spot.

While extremely useful, standard Grenades don't offer much in the way of inflicting damage. Because of this, players should always look for something more powerful...

### Homing Missile

<img src="../../assets/worms3d/weapons/Homing.png" align="right">

Generally thought of as a precision bazooka, the homing missile zeros on its target once fired. This means that you have to "paint" your victim with a crosshair before hand.

When using the Homing Missile it is important to check that there is nothing in the way of the weapon's guided trajectory.

### Mortar

<img src="../../assets/worms3d/weapons/Mortar.png" align="right">

What the Mortar lacks in the way of punch, it makes up for in tactical benefits. This is due to its clutch of bomb clusters which are released on impact.

The Mortar always fires at full power and with skill can be highly effective.

### Homing Pigeon

<img src="../../assets/worms3d/weapons/Pigeon.png" align="right">

The Homing Pigeon works in much the same way as the Homing Missile with two major differences: firstly it is more intelligent and can fly around simple obstacles (although complex structures such as caves usually defeat it). Secondly, it packs a much greater punch and can inflict 70 points in damage if it manages to find its mark.

### Cluster Bomb

<img src="../../assets/worms3d/weapons/Cluster.png" align="right">

Thrown and operated in the same way as the Grenade, the Cluster Bomb spews out a shower of smaller bomblets (in a similar manor to the Mortar) when it explodes.

The actual explosion caused the Cluster Bomb is very weak with most of the destructive power lying in the clusters themselves. Skilled use can cause plenty of damage.

### Fire Punch

<img src="../../assets/worms3d/weapons/DragonPunch.png" align="right">

Summon up the legendary powers of the Martial arts and unleash the fury of the Fire Punch.

Any worms in the immediate vicinity are sent flying in all directions and have have 30 points deducted from their health.

### Shotgun

<img src="../../assets/worms3d/weapons/Shotgun.png" align="right">

A firm favourite that has stood the test of time throughout the Worms series, the Shotgun is the only weapon in the game that gives you two goes!

Each shot inflicts a maximum of 25 points damage and can be used against multiple targets.

### Baseball Bat

<img src="../../assets/worms3d/weapons/Baseball.png" align="right">

Hit a home run for your team with a mighty swing of your trusty Baseball Bat.

Any worms that are unfortunate enough to be on the receiving end of a swipe from the bat will be sent hurtling through the air, landing bruised with 30 points less health.

### Uzi

<img src="../../assets/worms3d/weapons/Uzi.png" align="right">

The Uzi fires a spray of bullets which radiate out and send worms skidding along the landscape.

While the Uzi has the capability to inflict up to 50 points in damage, this rarely happens because of the inaccuracy of the weapon over medium to long distances.

### Air Strike

<img src="../../assets/worms3d/weapons/Airstrike.png" align="right">

Take cover! At least, that's what your enemies will be thinking as you call in your bombers to rain terror from the skies.

Each bomb dropped can inflict up to 30 points damage.

### Land Mine

<img src="../../assets/worms3d/weapons/Mine.png" align="right">

You can use Land Mines either as cheap Dynamite or to set a trap for your unwary foe (for instance, at the mouth of a tunnel).

### Banana Bomb

<img src="../../assets/worms3d/weapons/Banana.png" align="right">

A favourite of pyromaniacs, the Banana Bomb is far superior to the conventional Grenade when it comes to dolling out the damage. Upon detonation, the Banana Bomb splits into five smaller bananas which rain down upon the heads of your enemy. Each banana causes massive damage and entire groups of worms can be wiped out with one shot, particularly if they are in a confined space such a cave.

### Sheep & Super Sheep

<img src="../../assets/worms3d/weapons/SuperSheep.png" align="right">
<img src="../../assets/worms3d/weapons/Sheep.png" align="right">

Strike terror into the hearts of your enemies by unleashing the fearsome power of the Sheep. Upon release, the Sheep gambles towards your enemy and can be detonated by a press of the space bar.

If the sheep comes up against an obstacle it will attempt to jump over it. Not being very bright however, it will make no attempt to go around objects and will simply explode if left for too long.

The Super Sheep packs the same punch as its conventional counterpart, but can take to the skies with an additional press of the space bar. Steer your fluffy friend down onto your unwitting target and it's lamb casserole all round!

### Holy Hand Grenade

<img src="../../assets/worms3d/weapons/Holy.png" align="right">

"First thou shalt pull out the Holy Pin. Then thou shalt count to three; no more, no less."

Wreak havoc on the battlefield with a weapon of Monty Python fame. The Holy Hand Grenade can cause a maximum of 100 points damage and hurl your enemy over vast distances. Just lobbest thy Holy Hand Grenade at thou foe, who being naughty in my sight, shall snuff it. Amen!

### Old Woman

<img src="../../assets/worms3d/weapons/OldWoman.png" align="right">

Wind her up and watch her go! Yes, Worms fans, Ethel's back and she's packin' heat!

Upon release, Ethel will totter towards your enemy and explode after a short interval. Unlike the Sheep however, you have no control over her detonation so be careful she doesn't come tottering back into your lap!

### Mad Cow

<img src="../../assets/worms3d/weapons/MadCow.png" align="right">

You'll never look at a dairy herd the same way again!

Release wave after wave of frothing mad cows towards your target then site back and watch the action. Each mad cow can cause a massive 75 points damage which is more than enough to dent the confidence of even the most battle-hardened team of worms.

### Concrete Donkey

<img src="../../assets/worms3d/weapons/Donkey.png" align="right">

From a lowly garden ornament to a weapon of mass destruction. How many things in your back yard can boast such a distinction?

One of the rarest and most coveted weapons in the Worms 3D arsenal, the Concrete Donkey pile-drives its way through anything that's unfortunate enough to be underneath; land and worms alike.

## Utilites

To aid you in your maniacal destruction of the opposition is a selection of gadgets that would make James Bond jealous.

### Jet Pack

<img src="../../assets/worms3d/weapons/JetPack.png" align="right">

Take to the skies in style. Don the jet pack and soar above the heads of your enemies to reach those handy vantage points.

Just make sure you don't run out of fuel!

### Ninja Rope

<img src="../../assets/worms3d/weapons/NinjaRope.png" align="right">

Ninnnnjaaaaaa! You can leap! You can swing! You can drop weapons onto the heads of your enemies! You can make a complete tit yourself in front of everybody on WormNet!

We love it!

### Parachute

<img src="../../assets/worms3d/weapons/Parachute.png" align="right">

Drop safely from heights that would otherwise pulp you like a ripe banana.

Check that the parachute is selected before you leap to avoid making a complete ass of yourself on the rocks below.

### Skip Go

<img src="../../assets/worms3d/weapons/SkipGo.png" align="right">

Errrrrrmmmm....do you really need this explaining?
