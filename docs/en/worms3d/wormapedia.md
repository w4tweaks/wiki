# Wormapedia

## Agent Dennis

"Rescue Agent Dennis" was one of the more difficult missions in "Worms Armageddon" and the briefing ran along the lines that Agent Dennis was in a rather sticky situation and needed a helping hand. The titular Agent Dennis is in fact the loveable head of the Team17 Design Department, John Dennis, known for his monkey-like football skills and amazing ball-balancing.

## Air Strike

The Air Strike is a great weapon for attacking quite large areas of landscape and provide a method for a sure-fire kill if the enemy is out in the open when sudden death begins. Each missile can achieve a maximum of 30 points of damage.

## Anonymous Chat

By making use of a certain IRC style command in the "Worms World Party" chat box, it was possible to send messages anonymously to everyone who you were playing against. The message appeared in the chat box in yellow text and had no one's name attached to it. During the testing stages of the previous "Worms” games, it was popular for certain cheeky testers to make use of this feature to make it look as though a programmer or someone else of importance to the project had appeared and caught them messing around and using bad language (shock)! It made it even funnier when this actually did happen.

## Artists

Worms 3D was brought to you by the following artists: Dave Smith; Patrick Romano; Andy Morriss; Mike Green; Rico Holmes; Enrique Llopis; Mar Hernandez; Tom Gluckman; Tony Landais and Javier Leon.

## Viking Axe

This is a weapon quite unlike all the others. The Viking Axe is a close-combat weapon which splits your enemy's health in two and buries the poor victim into the landscape. If a worm's health is down to a single point then the Viking Axe will kill them.

## Banana Bomb

The Soft Fruit of Doom! This is like a heavily pumped-up version of the Cluster Bomb. After a Banana Bomb's fuse burns down, it splits into five Banana-bomblets, which shower the landscape and cause mass devastation. Each of these bombs can cause up to a maximum of 75 points of damage. Watch out though, if you aim this high into the air there is a chance that the bombs will spread out far enough to damage one of your own worms.

## Baseball Bat

The Baseball Bat is an excellent weapon for removing unwanted enemies far and lengthy distances across the landscape often with wet results. And remember not to waste the Baseball Bat when you can knock worms into the water using other, less useful weapons, such as the Prod. The Baseball Bat can cause up to 30 points of damage.

## Bazooka

In the right hands, the Bazooka is a skillful weapon. It is always in plentiful supply and has a few neat tricks to learn. When inflicting maximum damage, the Bazooka can claim a moderate 55 points of health from your foe! To make the best of the Bazooka you must learn from the environment. It is greatly affected by the strength and direction of the wind and in some situations it can become almost aquatic in its abilities.

## Spadge and Music

Spadge's Beatbox. Music on the go? Music wherever you are? Sounds like a lot of fun until you have to sit next to him! Over the past several months Spadge has enjoyed showing us boys up in a variety of ego-shrivelling locations, from fancy Manchester penthouse suites right down to the streets of sleazy Ossett. If you're ever on the train and you faintly hear the all too familar licks of "In The Year 2525", there's a more than likely chance that you're about to be attacked by Spadge and his Beatbox. Head for the toilets.

## Binoculars

The Binoculars are the tool to use when scoping out the position of enemy worms, as well as various crates and potential problems.

## Blow Pipe

Many, many years ago, ancient worm ancestors used to do battle with very primitive yet effective weapons and tools. Only recently did the possibility of sustained damage of the Blow Pipe come to light. After aiming the weapon, BLAM! A rush of air exhaled from the worm causes a sharp, dart-like object to shoot from the pipe, into the desired enemy worm. Slowly but surely, the poison will sap the enemy worms health, thus leaving them in the very worst of conditions. Yuk!

## Boggy Pete

Mmmmm well hello there, darlings. I am the loveable rogue that is Boggy Pete. People go on about Boggy B this and Boggy B that but at the end of day I'm a bigger, better worm than he'll ever be. When I'm not involved in combat I like nothing more than to tend to my veg, down on my farm - why not check out my crops the next time you're playing Crate Britain.

## Brightside

Worms Religions - Brightside. Co-founded in 2002 by Paul.Power and Apollo, the Brightside is a movement dedicated to... well, nothing. Instead of pledging allegiance to a side (Lightside, Darkside, Greyside, Sewerside, Flipside, etc.), they play according to the situation. If it favours Light play, they play that way. If it favours Dark play, they go and dig in. Brightsiders are so called because, in their own opinion, they are smarter than other players by not sticking to any particular code. They have a badge, a motto ("No shame in using your brain"), and an "anti-oath" which essentially affirms their commitment to having no commitments.

## The Buffalo of Lies

Buffalo of Lies... the truth! After all these years of waiting the Buffalo of Lies finally makes it into the game. Was he worth the wait? Do we really need dirty disgusting tricks like this in the game? We hope that most of you will choose not to follow the path of the Buffalo... we really do!

## Concrete Donkey

The Concrete Donkey... oh yeah this is the one to use alright when the enemy just won't come out of that nifty little bunker they've created for themselves.

## Chatter

A popular trick in the days of 2D "Worms", between Wormers around the World and particular members of Team17's Testing Department when playing online, is to annoy their opponents by pestering them with chat messages. This causes the phone icon in the top right hand corner of the screen to ring, and you'll find you cannot help but smile weakly as you're concentrating on trying to reach safety before your retreat time runs out with this minor, mocking distraction bugging you ever so slightly.

## Cluster Bomb

This Grenade-like weapon explodes after its fuse burns down, and showers the surrounding area with powerful cluster fragments. The actual explosion from the Cluster Bomb doesn't do much damage, it's the clusters that contain all the power, and so the key to this weapon is trying to make all the clusters hit the same place. The amount of damage the Cluster Bomb does is dependent on how many clusters hit the target.

## The Programmers...

Worms 3D didn't program itself, these people did it instead (we asked it to program itself but it declined): Charles Blessing; Andy Clitheroe; Martin Swaine; Colin Surridge; Paul Scargill; Phil Carlisle; Kev La'Mont; Damien Stones; Ian 'AfroHorse' Lindsey; James Cowlishaw; Paul 'Micro-scooter' Tapper and Steve Eckles.

## Mad Cows

The Mad Cow is a mobile, explode on impact no-nonsense weapon. They do tend to work on the principle of "safety in numbers", so if you come across these characters it could be you who ends up as chops! Each Mad Cow can cause up to 75 points of damage.

## Darkside

Worms Religions - Darkside. A totally different side of Worming, the way of the Darkside. An evil to the good that is Lightside. Fancy becoming a follower of the Buffalo of Lies? Then it's time to start hiding instead of fighting! Passage taken from 'Beige and Tight' the hip-novel by the leader of the Darkside religion, The Buffalo of Lies, take it away: Chapter 36, Verse 1 "If thou has takken a royal gangin' beat. Then i allow thee to do how thoust sees fitxxxor to dispose of thine and mine enemeeees. A Bazooka? Pray no, whip out teh Homing Missilexxor!"

## Double Damage

Double Damage is an "instant effect utility"... this means that it's activated as soon as you collect it. This one does exactly what it says on the tin. All weapons inflict twice the amount of damage as they did before and everything that explodes has a larger blast radius.

## The Designers...

The Worms 3D Design Team (also known as the 'Spine of Team17') consists of: John 'Agent' Dennis; Porl Dunstan; John 'Jeggett' Eggett; Grant Towell; Kevin 'Chesh' Carthew; Kelvin 'Workhorse' Aston; Mark 'Duke' Dimond. These guys were responsible for the original design document, mission creation, all text, screen layout, schemes, weapon tweaks and a million other things which we cannot remember at 4pm on a Friday afternoon.

## Doctors Strike

The Doctors Strike can prove really helpful but at other times can prove quite a hindrance! Launching the Doctors Strike will cause a mass of health crates to float down onto the landscape readily available for your collection.. Or your opponents!

## Concrete Donkey

Andy Davidson, the loony who originally created the concept that we know and love as "Worms", was as a young boy led to believe by his parents that a concrete donkey stood in his garden was in fact a real donkey, encased in concrete and held at their leisure. The Concrete Donkey is a most destructive garden ornament and a rare sight to behold in a game of "Worms".

## Double Turn Time

This gives you more time to cross the landscape, a bit more time to think about your move, or more time to line that shot up: the choice is yours.

## Earthquake

This causes the landscape to shake from side to side for a few seconds, making the worms slide around, sometimes onto mines and sometimes off the edge of the land altogether! The Earthquake will also make all the other objects on the land slide around, Crates, Oil Drums and Land Mines!

## Fakes and April Fools

There have been several hoaxes perpetrated by Team17 over the years: from a mission expansion pack which would allow people to play 'Night Fight' games to the fake Spectrum screenshot that appeared in many popular gaming press magazines and on sites across the World Wide Web. Development head honcho Martyn Brown was quoted at the time as saying 'It may come as a surprise that we've developed for the Spectrum, but it only cost us 150 quid to do!'. Team17 fooled many people within the industry, and gained respect from certain members of the press as a result of this elaborate hoax. Will those Team17 jokers ever give it a rest?

## Fire Punch

The Fire Punch sends your enemies flying in all directions, and in the best circumstances, onto mines or off the landscape! The Fire Punch must be activated while standing directly next to your arch-enemy. When so positioned, it sends them flying in the direction that you're facing and chops 30 points from their health.

## Freeze

The Freeze utility is great to use when one is about to be involved in a Sudden Death battle, as it not only provides an impenetrable shield around your worm, but also protects frozen worms from the energy-sapping effects of Nukes. Pretty cool huh? A word of warning though, be careful not to use Freeze when you haven't got much land beneath you, because if you're not too far above sea level and the enemy plant a well placed Dynamite near your toes, it could be you who looks worried as your icy wriggler has a paddle in the drink!

## Friendly Fire

I'm sick of those close proximity weapon attacks!" said one of the worms on camp, a deafening cheer could be heard throughout the whole place. "We need something to try and stop such cheap attacks!" - Hence, the birth of 'Friendly Fire'. Circling the defending worm, a ring of fire would emerge, protecting the worm from any close proximity attacks. This meant any would be assailant would be burnt as they tried to use such a cheap method of attack. Bliss.

## Gas Canister

The Gas Canister is essentially a Grenade weapon that releases a deadly gas to make even the meanest of worms turn green. Removing a steady 3 health points each turn, it's well worth using.

## That Angular Chap...

Aaaah yes, the Angular Wooden Giraffe. Still a mystery (even to us guys who work with Worms) however, just recently did one of our many scouts seek out the Angular Wooden Giraffe in its natural habitat. Very weird looking it is, anyway - check it out!

## Girder

The main use of the Girder is to bridge gaps allowing your worms to get to places they otherwise couldn't reach. However, it can also be used to provide a small defensive shield that will withstand one direct hit of the Bazooka before leaving your worms exposed.

## Grenade

This weapon makes up the basic "Worms" arsenal along with weapons such as the Bazooka and the Shotgun. The player can set the Grenade fuse time, while how long the fire button is pressed for decides how far the Grenade is thrown. Mastering the Grenade is an essential skill, and is all part of becoming a top Wormer! A direct hit can result in 45 points of health being claimed from your opponent.

## Holy Hand Grenade

The Holy Hand Grenade is a super-strong weapon that can wreak absolute havoc when used in the best possible situation. The key to getting the most from the Holy Hand Grenade is to use it in a bunched area of enemy worms. Don't worry either if they're in the centre of the landscape, the Holy Hand Grenade will catapult the enemy high and wide... often resulting in watery deaths. The Holy Hand Grenade can cause up to 100 points of damage. Excellent!

## Homing Missile

The Homing Missile gives a helping hand to those players trying to pick off a hard target. Before firing the Homing Missile, you must first mark on the landscape their desired point of impact. Make sure that it is fired with enough power to make it past any obstacles that may be in its way as it travels towards its target. Very useful when there's no direct line of sight to your opponent! A direct hit can result in 45 points of damage.

## The Horror Theme

The Horror Theme.Worms being the nasty little, low-life critters they are like nothing more than do battle over such nastiness as crypts, spiders and even the gravestones of their fallen comrades. If you find yourself out there, on a Horror themed landscape listen very carefully and you might just hear Grant's very own 'crow' sound effect... "HAAAAWWWWR!", indeed.

## Hunger Strike

You won't find this weapon in a restaurant nearby you anytime soon! Let's hope the enemy's hungry 'cos it looks like today's menu is serving up a real special. Upon launch, various types of food will be scattered down upon the opposing Worms causing them huge health problems. A far cry from indigestion, I can tell you!

## Jet Pack

Who would have thought that worms would have been able to fly without the aid of explosives? Amazingly, equipping the Jet Pack allows your team of squishy troops to take to the skies! Once the Jet Pack is active, the amount of fuel remaining can be seen displayed in numbers above your worm. Pressing the fire button fires the engines. Usefully, most weapons can also be dropped off of the Jet Pack, giving your high-flying worm plenty of time to beat a hasty retreat!

## Lightside and Darkside

The hardcore "Worms" players that you will stumble across on the Internet are regularly divided into two factions. There are those that adopt what is called an "underhand" style of play, and their games revolve around digging through the landscape to safety, hiding, planting Girders and generally being very sneaky; these are known as the "Darksiders". Then, there are those that believe themselves to be courageous and true: the "Lightsiders”, who never cower in fear of incoming missiles and always fight bravely unto the end! Which side are you drawn to?

## Last Worm Syndrome

Oh yes, first created in the murky depths of the Team17 Quality Assurance Department, we come across the dirty lie-disease of "Last Worm Syndrome". This stinking move is simply executed by a gentle cry of "NO, THAT'S MY LAST WORM". But of course, it isn't their last worm at all, this move buys the filthy player a few seconds of relief and if lucky enough, a total let-off. Only for them on their next turn to return the favour with a stab-in-the-back kill. Ignore their pleas, please.

## Low Gravity

Low Gravity affects almost all of the weapons in some way or another. Weapons can be thrown further, everything bounces higher and the effects of explosions are more dramatic: worms can be blown all over the landscape! But as well as using this utility for offensive purposes, it's also useful if you need to get somewhere without using a Ninja Rope or Jet Pack, as your worms can jump and back-flip over greater distances!

## Lightside

Worms Religions - Lightside. When it comes to Worming, there are many paths that one can take in their perilous journey. The way of the Lightside is one that is adopted by many a Wormer and is to be a true follower of the Concrete Donkey and his teachings. If I could lend your ear for five, i'd like to take this opportunity to lay down a couple of our favourite 'Donkey Licks'. Chapter 3, Verse 14 from "Grey and Worn" "Hast thoust bin follows teh way Bazookxxor? Thenest not shalt down mein brothers". Don't try understanding it, we've had nine top biblical translators on it for eight years.. Fat chance!

## The Lost Missions...

Not all missions make it through the development process, a few are altered to suit the powers that be, others may be chopped due to a lack of time. Movie Mayhem for example, this mission changed from a funny little tale of hilarious film studio mishaps into a Deathmatch style affair... probably down to the hilarious programming mishaps. Still, we managed to keep a shot of the original mission. Enjoy!

## Lottery Strike

Here we find the Lottery Strike, now this really is a bit of a lottery (haha) - so what will the damage be? Falling tickets could cause anything between 1 to 49 damage points. Is it really worth such a gamble?

## Land Mine

Your standard army issue Land Mine, works like every other Land Mine on the landscape. Ensure that you have enough time to run away after dropping the Land Mine... you don't want to get caught by your own weapons!

## Miner Bird

Aaah yes, never before has nature seen such a wonderful bird. Swooping overhead, the Miner Bird gently lays lethal Land Mines on the landscape for our pink heroes to cope with whilst doing battle.

## Mole

I'm afraid to say that we've been bullied into creating a Darkside Digging Tool by the Buffalo Of Lies. As sad as this sounds to the Lightsiders, you must hand it to this handy piece of kit. Create the perfect, safe haven and just generally stay out of trouble. One for the Darksiders this one, best used between early to mid-game and make full use of the Strike weapons to whittle down your opponents. This weapon will provide safety as well as potential for attacking methods.

## Mortar

The Mortar is a weak weapon at face value, the shell that is fired does hardly any damage to your opponent and has a small blast radius. The real power behind this weapon lies in its spread of clusters. The trick to maximising the effect of these little beauties is to try and make them detonate all in the same place.

## Napalm Strike

The Napalm Strike is an incredibly deadly weapon when dealt into the hands of a worthy player. Dubbed "The blanket of death”, don't forget to keep a careful eye on the wind strength and direction because the actual placement of the flames is in the hands of the elements. For full effect, plant this baby onto the soft heads of a nice clump of wrigglers (preferably the enemies) and watch our pink-pals get hot feet! Damage from a direct hit can range from 50 to 70 points of damage.

## Niagra

I bet you're sick of your worms being so soft at times. Well not to worry, it's time get those little punks whipped into shape. Niagra will make even the most softest of worms hard. For three turns, this worm will be ROCK SOLID, preventing it from sustaining any health damage whatsoever.

## Noah's Bark

This is one flood that Noah couldn't predict! Causing the water level to rise, Noah's Bark will make even the highest positioned of worms feel queasy with seasickness!

## Nuclear Test

The Nuclear Test is a weapon that when used, makes the water rise quite a considerable amount. Not only that but it will also make each worm on the landscape ill. This means 5 health points will be taken from each worm per turn. Remember though, if your worms do get ill they can be cured easily by collecting a Health Crate.

## Parachute

The Parachute is great for drifting from high to low landscape. Ensure that the wind is blowing in the direction you wish to travel and simply launch yourself into the air. The way to get the most out of your Parachute is to open it when you are at the peak of your jump. If the wind is only blowing mildly in the desired direction of travel and the ground below is water-filled, don't risk it. More often than not, this will end in the unfortunate demise of your worm.

## Petrol Bomb

The Petrol Bomb is a great weapon! Remember that the flames will remain for a short while where they land, which means your enemy will more than likely be trapped by the surrounding flames, unable to move without using a utility such as the Ninja Rope. The Petrol Bomb can do serious damage when used correctly.

## Homing Pigeon

The Homing Pigeon is a very powerful yet dim bird. It acts rather like the Homing Missile in that the user has to mark the landscape before firing it and when fired it makes its own way towards the target. However, the Homing Pigeon is not only more intelligent in avoiding obstacles between you and your opponent, but it can also inflict a devastating 70 points of damage if it manages to find the target correctly!

## Addiction 2?

Worms 3D Pinball. During the development of Worms 3D a whole new, real-life game was discovered. It turned out that when a producer or designer entered the Worms 3D office to try and get a question answered they'd be passed from person to person with no one wanting to actually deal with the question or problem at hand. Programmers would quite happily pass the buck onto an artist and so on. This was called 'Worms 3D Pinball', the aim of the game became to enter the Worms 3D office and see how many members of the team you could be passed along before finally getting an answer. The record as it stands was a nine man combo, as recorded in the memoirs of the late John Eggett, Chapter 8 Verse 2 "This whole day I have spent wandering helplessly around the Worms 3D office and I'll be damned if an answer to my problem can be found. But joy behold, I managed a Nine man score at Worms 3D Pinball".

## Pink Beard

Avast ye yer scurvy sea-dogs! I be the most feared pirate on the six wormy seas, so if you see me on your adventures then you better scarper or else ye'll be walking the plank before you can say "Sorry about that, Pink Beard". Oooh Ahhh!

## Pizza Delivery

The Pizza Delivery is a real feast of destruction that when served up for the enemy is sure to make them go all doughy-eyed! Definitely a weapon of choice for the players amongst us with no manners. Mama Mia!

## Prod

Ack!! You want to upset somebody!?! What're you playing at with this one then, eh?? Oh alright... we'll let you off... it is kind of funny. The Prod move is the ultimate insult, even more so when you get it right. It doesn't do any damage, just gives your opponent a nasty little push in the intended direction.

## The Producers...

If Team17 were a finely oiled machine, the following chaps would be the oil: Martyn 'Spadge' Brown; Paul Kilburn; Mark Baldwin and Craig Jones.

## Worms 3D Prototype

The first prototype of Worms 3D was created by none other than Team17's very own 'Tin Man', Alaric Binney. This nifty prototype pointed us in the right direction, showing us that Worms 3D could be done, AND it would work. So a huge round of applause please, for Mr. Alaric Binney.

## Puppet Master

The "Puppet Master" has skills that far transcend plain old "Worms" playing skills: he or she is also a master of psychology! They are the ones who are able to befriend enemies within a game, and trick other people into becoming allied to aid the task of the destruction of a certain foe. The Puppet Master's word can never be trusted, and they will always turn on their so called "friends" at important moments in the game. Beware encounters with Puppet Masters, their silky smooth voice will trickle through even the most ardent of minds, and before you know it you might find yourself complying with their evil wishes.

## The Playtesters...

From the murky depths of Team17 we find ourselves face to face with the testing phenomenon that is, the Team17 QA Department. They are as follows: Paul Field (QA Manager); Andy Aveyard; Brian 'Cracker' Fitzpatrick; Jax Li; Adrian 'Charlie' Evans; Lee Varley and John 'Eggman' Egginton.

## Ninja Rope

When the Ninja Rope hit the streets in the original "Worms", it created various new styles of gameplay and amassed thousands of its own fans with its incredible amounts of flexibility and high fun content. The Ninja Rope in "Worms 3D” provides a classy looking travel tool and can be used to deploy weapons such as the Dynamite in mid-air.

## The Sally Army

Where are they now..? \#1. A wet, Ossett morning. Nothing different, status normal. That was until we bumped into a fiesty old trampette, imagine our surprise that on further inspection it turned out to be none other than our old friend Sally Army. Pleased to see us? Fairly. Pleased to have been axed from Worms 3D? No way! We had to run like the wind to catch our bus and avoid a tambourine-flavoured beating!

## Scales of Justice

Ah yes, the Scales of Justice, a cruel, cruel weapon to use when you only have one worm left and the enemy has many worms. Just as the enemy is counting their chickens. Slam! They get hit with the Scales of Justice. Grin smugly as your pink friend gets a healthy top up from the soon to be unhealthy enemy and laugh with glee as your enemy winces! The maximum damage that can be incurred varies at all times.

## Sheep

Exactly what goes on inside the Sheep's mind? Best not to think about it, just let him go and watch him bounce across the landscape. If the Sheep comes across an obstacle or ravine then he will do his best to get across it. Pressing fire once sends the Sheep on his way, and then a second press detonates him, if he's left too long jumping around then he will get tired and do something about it himself. A Sheep in the face can do as much as 75 points of damage!

## Shotgun

One of the most useful weapons in the game, the Shotgun comes loaded with two rounds of ammunition, making it a rather tactical weapon in the right hands. The weapon is activated as soon as the fire button is pressed, one shot is taken and then the second one after that. Be careful though, if your worm falls too far or stumbles onto a Land Mine and gets injured, your turn is taken off them and you do not get to use your second shot. Disaster! Each round from the Shotgun blows a hole the size of 25 points in your enemy's health.

## Shrines

In the World of Worms, the power of the Shrines are that of an unknown quantity. As of yet, there are no known facts regarding the Shrines.

## Sniper Rifle

Hailing straight from the same 'High-Accuracy' gun camps as the 'Shotgun' and so on, emerges this high-powered, precision weapon. This has been a choice firearm for the worms back at camp recently and is simply unsurpassed when it comes down to those who require the ultimate in long-range accuracy. Always a favourite over long distances, the Sniper Rifle can easily hold its own when used in such instances. Best used on a worm with medium to low energy, ensuring a huge energy blow to the enemy worm or even better.. A kill!

## Crate Spy

This nifty little utility allows the team collecting it to see instantly the contents of all the Crates on the landscape. Imagine! However, this can add a little desperation to the game, knowing that there's a Concrete Donkey in a Crate just a small distance away, and not having enough time left to collect it. You wouldn't want that falling into enemy hands now would you?

## Super Sheep

Our favourite farmyard animal dons his cape and takes to the sky! This Sheep can fly! After being released, a second tap of the fire button will send him shooting upwards into the sky, where he can be flown around the landscape. How cool is that?

## Sticky Bomb

The Sticky Bomb is a welcome addition to the Worms portfolio of new weapons. Never before has launching a bomb vertically upwards so that it sticks to the landscape beneath an unsuspecting worm been possible - until now that is! One of the biggest strengths of such a weapon is the fact that there are no rebound factors to worry about when aiming this weapon at a target. An absolute essential in Sudden Death situations, if a Worm is stood on an edge of the landscape, Sticky Bomb, full power. Hey Presto!

## The Worms Story

The story so far... In 1995 we saw the arrival of the first 2D outing, WORMS. Later, in 1996, WORMS REINFORCEMENTS was to follow (packaged with WORMS it was known as WORMS UNITED). WORMS DIRECTOR'S CUT, also out around the same time, exclusive to the Amiga A1200, was the pinnacle of the original graphical style and saw the introduction of such favourites as the Super Sheep. Then came WORMS 2, a giant leap forward in terms of its online capability and its fresh cartoon-like graphical style. After WORMS ARMAGEDDON and WORMS WORLD PARTY came WORMS 3D, we don't need to tell you much about this beauty since you're sat playing it right now!

## Mega Mine

After years of being perfected, the Mega Mine is now an essential piece of kit in any good worm's armoury. But as soon as it gets dropped, you'd better run for it! With this new breed of Land Mine being much more powerful, there can still be no room for error. ALWAYS ensure you release the Mega Mine right next to an enemy worm, the amount of times I've seen a mine be dropped next to an enemy and then it not arm.. Sheesh. You've been warned!

## Enough in-jokes?

The Tapper Files... Mid-afternoon on a lovely Summer's afternoon in the Design Suite. What we didn't need was confident young programmer, Tapper (AI coder for W3D), to come into the office and attempt to display his micro-scooter skills. Needless to say, Tapper (as lovely as he is) got carried away with the smaller 'bunny-hops' and decided he was ready for a real ramp. To cut a long story short, Tapper (plus his sore behind and bruised ego) won't be attempting anymore extreme sports in the Team17 building for a very long time.

## Teleport

Ideal for getting around, simply select where you want your little guy to go, press the fire button, and hey presto! You're there!

## Dynamite

Do we really need to say much about this one? It's Dynamite... so be careful with it! Once you've dropped it, you'd better pray you make good with your escape plan, because the Dynamite takes no prisoners! It has a 5 second fuse and a nasty habit of blowing a huge 75 points of health off anything caught in the blast.

## Uzi

The weapon of choice for gunning worms about the landscape. The Uzi fires a stream of rounds off in one go, while the direction of firing can be strafed around the target area. It can send worms sliding long distances, and is perfect for grouping your enemies together. All in all, the Uzi can claim about 50 points of energy from your unwitting foe.

## Ming Vase

The Ming Vase is an extremely powerful cluster-based weapon that can severely wreak havoc when deployed into a bunched area of enemy worms. But beware, there could be a stray shard which could wipe out a few of your worms if placed in a risky area. Each fragment that explodes from the Ming Vase can inflict up to 75 points of damage.

## Worm Wear

Also known as "The Cloth of Gods". Worm Wear has been around since the very start of the whole "Worms" era and is set to stay with us for quite a while. Worm Wear is created from only the finest of yarn available and is the only sure-fire way to survive Armageddon. Remember though, if you see someone in the street donning Worm Wear, don't approach them... it's better to silently acknowledge their superiority from a distance. Stay safe folks.

## Old Woman

The Old Woman is a deadly timed explosive weapon that has to be precisely used otherwise you could end up being the worm on the receiving end of the grumbling pensioner. Best used in situations where you are above the enemy worms and well out of harm's way. Simply press the fire button to release her and enjoy the carnage she brings. The Old Woman can cause up to 75 points of damage.

## Worm Select

Ever thought to yourself "Ooh I wish I was controlling \*that\* worm"? Well now you can! This helpful little utility gives you the opportunity... but just what is it you want to do? Get closer to a certain enemy worm? Perhaps move your damaged soldiers to a safer place? Oh the choices! With Worm Select, the landscape is your oyster!
