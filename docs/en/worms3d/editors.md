# Editors

## ToolsPack v 2.0

Edit Tools for game Worms3D 

![ToolPack.png](../../assets/worms3d/ToolPack.png)

For creating own map need have next tools: 

- **W3DMapEditor v.2.4** - Map editor, more info [Making own map](../maps/making-own-map.md)
- **W3DLauncher v.4.420** - Loader for load map list and tweaks (include in [PNM](mods.md)). 
- **W3DIconEdit v.1.0** - Making icons for maps. 
- **ScriptsEdit v.1.4** - Making Map List and add in List.

For other resource next tools: 

- **XomView v.2.9** - View and Edit worms xom-resource (HUD/Textures/3DModels). 
- **W3DLanguageEditor v2.0** - Edit language file of game.

All of this collected in one pack!!!

> [Link on ToolPack v.2.0](http://yadi.sk/d/QJHFHCVn6SffA)
