# Map Editor FAQ

!!! warning "This article is unfinished"
    Work in progress.

## When I want to edit a map, A massage eather saying "Bad XOM File name" Or "Access Violation"

File have not MAP structure.

## How come the Map is all White and I cant see the Colors and Textures?

Read Manual, For Textures you should load it in *Texture Panel*.

## Where is the Main Ground?

[Height Map](height-map.md) need open *File - Open Height Map...*

## Is there a Tut/Help for this?

Use Manual (F1) and see [Making own map](making-own-map.md)

## How do I change the Shape of a new box? Like to a chunk of land or a pipe?

Use Layers Mode for make form of [Poxel](poxel.md)

## How do I Insert the map into the game?

Worms3D or Worms4? For Worms3D read this [Making own map](making-own-map.md)
