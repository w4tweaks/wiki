# Height Map

Height map is type of **Objects Map** (Used for W4). This type used for creation of landscape of map.

For import from image Height map use *Properties Panel - Height Map - Import Hmap*. Will opened Import Height Map form.

You can also Export your Height Map for edit. For export your Height map to bmp-image use *Properties Panel - Height Map - Export Hmap*.

Landscape W4 have also Mask for Second Texture. You can Import/Export your Mask for Texture Height Map. For import your Mask for Texture from bmp/jpg image use *Properties Panel - Height Map Texture - Import Image*. For export your Height map to bmp-image use *Properties Panel - Height Map Texture - Export Image*.

All commands analogic to Import Height Map Form.

In Worms 4 Mayhem you can also change Mask textures of Height Map. You can find Name of Base/Second Texture from **Worms 4 Mayhem\\data\\databanks\\mapname.xml** file.
