# Tutorial #2 - Complex Figures

3D Import Tutorial

![Imp076.gif](../../assets/maps/Imp076.gif)

## 1. **Create Primitives figures**

Open **3DSMAX** to create **Primitives Objects** like on the next scheme:

### Ellipse

Select in *Menu - Create - Standard Primitives - Sphere.*

(Radius: 12, Segments: 32, Move: [-18,-8,0], Rotate: [0,105,0], Scale: [210,125,100])

### Torus

Select in *Menu - Create - Standard Primitives - Torus.*

(Radius 1: 14, Radius 2: 5, Segments: 24, Sides 12, Move: [24,-24,0], Rotate: [-5,-28,2], Scale: [100,100,100])

### Teapot

Select in *Menu - Create - Standard Primitives - Teapot.*

(Radius: 16, Segments: 4, Move: [18,16,0], Rotate: [0,0,0], Scale: [100,100,100])

### Tube

Select in *Menu - Create - Standard Primitives - Tube.*

(Radius 1: 9, Radius 2: 7, Height:32.5 Segments: [5,1,18], Move: [42,40,0], Rotate: [0,0,0], Scale: [100,100,100])

### Helix

Select in *Menu - Create - Shapes - Helix.*

(Radius 1: 11, Radius 2: 3, Height: 40, <Turns:5.3>, Enabple in Viewport, Radial Thickness: 2.21,

Move: [-22,21.5,0], Rotate: [0,0,0], Scale: [100,100,100])

You should get primitive objects like ones shown on the screenshot:

![Imp001.gif](../../assets/maps/Imp001.gif)

Now Export the current file to \*.3ds. Select *Menu - File - Export...*

![Imp002.gif](../../assets/maps/Imp002.gif)

Save this file as **ImportsObjects.3ds**

![Imp003.gif](../../assets/maps/Imp003.gif)

Now open W3DEditor v2.2 and select *Menu - File - Import 3ds...*

![Imp004.gif](../../assets/maps/Imp004.gif)

In the first time, do not change defaults values, and open the file. Press *Open.*

![Imp005.gif](../../assets/maps/Imp005.gif)

Select **ImportObjects.3ds** in the folder where it was saved.

![Imp006.gif](../../assets/maps/Imp006.gif)

The first result of the Objects imported has a very bad quality. As you can see Poxels have not the shape of Primitive objects, but that is not a problem.

In next steps we will solve it.

![Imp007.gif](../../assets/maps/Imp007.gif)

## 2. **Ellips**

For clear your workplace, press in *W3DEditor v2.2 Menu - File - New Map...*

![Imp008.gif](../../assets/maps/Imp008.gif)

Change **Options** for **Apoxelation** process. Check**Top Smooth** and **Side Smooth**,

change **Quality Layers Smooth** to **0.001**. And *Open* file.

![Imp009.gif](../../assets/maps/Imp009.gif)

Now a more nice result is obtain, but not for all primitives. Only **Ellipse** it perfect.

In next steps we will improve the rest of the Objects.

![Imp010.gif](../../assets/maps/Imp010.gif)

## 3. **Torus**

As you can see, **Torus** in poxel shape has not a hole in middle, therefore we should divide the object into parts.

![Imp012.gif](../../assets/maps/Imp012.gif)

In order to do so, go to **3DSMAX**, select **Torus** and **Convert** to **Editable Mesh**, in **Modify Panel.**

![Imp013.gif](../../assets/maps/Imp013.gif)

For making a division, use polygon method.

![Imp014.gif](../../assets/maps/Imp014.gif)

To be able to select an object, choose a way to **Circular Selection Region**.

![Imp015.gif](../../assets/maps/Imp015.gif)

We must divide **torus** into 3 parts. Go to **Top View** and select polygons as it's shown on the screenshot:

![Imp016.gif](../../assets/maps/Imp016.gif)

Disconnecting the selected part, click the **Edit Geometry - Detach**

![Imp017.gif](../../assets/maps/Imp017.gif)

Named the object as **Torus_p1** and press **OK.**

![Imp018.gif](../../assets/maps/Imp018.gif)

The next part requeires selecting a similar method.

![Imp019.gif](../../assets/maps/Imp019.gif)

Try to separate identical fragments.

![Imp020.gif](../../assets/maps/Imp020.gif)

Name the second part as **Torus_p2** and press **OK.**

![Imp021.gif](../../assets/maps/Imp021.gif)

Select all 3 parts of **Torus** to export them to .3ds.

![Imp022.gif](../../assets/maps/Imp022.gif)

*3DSMAX - Menu - File - Export Selected...*

![Imp023.gif](../../assets/maps/Imp023.gif)

Save as **Torus.3ds.** Then, go to *W3DEditor - Menu - File - Import 3ds...*

Select **Torus.3ds** and watch the results:

![Imp024.gif](../../assets/maps/Imp024.gif)

As you can see, now the **Torus** is more similar to the original one.

![Imp025.gif](../../assets/maps/Imp025.gif)

## 4. **Tube**

**Tube** shape has to be corrected too. It needs to do divided into 3 parts.

![Imp027.gif](../../assets/maps/Imp027.gif)

In 3DSMAX select **Tube**, Convert to **Editable Mesh** and Detach to identical parts.

![Imp029.gif](../../assets/maps/Imp029.gif)

Select all 3 parts of the **Tube** to Exports them to .3ds. *3DSMAX - Menu - File - Export Selected...*

![Imp030.gif](../../assets/maps/Imp030.gif)

Save as **Tube.3ds.** Then go to *W3DEditor - Menu - File - Import 3ds...*

Select **Tube.3ds** and watch on result.

![Imp031.gif](../../assets/maps/Imp031.gif)

The results are not so good. It needs extra corection.

![Imp032.gif](../../assets/maps/Imp032.gif)

If **Apoxelation** works badly, you should change the size of the Original Object.

After **Apoxelation,** it will come back to its previous size.

In **3DSMAX** use *Select and Uniform Size,* select all 3 parts of the **Tube,**

Then keep **Ctrl** pressed and change Y size in **Front View**.

Reduces the size ot the object to about the size shown in the screenshot:

![Imp033.gif](../../assets/maps/Imp033.gif)

In the current opened window write name of the Copy: **Tube_Short**.

![Imp034.gif](../../assets/maps/Imp034.gif)

Select New and old tube parts and Export them to .3ds.

*3DSMAX - Menu - File - Export Selected...*

Save as **Tube2.3ds**.

In **W3DEditor** in **3DS Import Form** change options:

Off **Top Smooth**, Off **Side Smooth**. Press *Open...*

![Imp035.gif](../../assets/maps/Imp035.gif)

You should get some strange poxels, but it's alright.

![Imp036.gif](../../assets/maps/Imp036.gif)

In order to delete the incorrect parts go to **Map Tree Panel**, hold **Ctrl** and **Select** Tube01, Tube_p1, Tube_p2.

Then in context menu press **Delete Poxels**.

![Imp037.gif](../../assets/maps/Imp037.gif)

To get the previous shape of the Tube, you need to change the size of the poxels.

Select a part of the Tube and resize it.

![Imp038.gif](../../assets/maps/Imp038.gif)

The new Size must be about **17** on Z Axis.

![Imp039.gif](../../assets/maps/Imp039.gif)

If all steps were correctly done, you should obtain a result like this.

This not a perfect Tube, but it's the most similar to the original you can make.

![Imp040.gif](../../assets/maps/Imp040.gif)

## 5. **Helix**

As you can see, the Helix has a strange shape after Apoxelation.

![Imp042.gif](../../assets/maps/Imp042.gif)

In 3DSMAX select Helix, Convert to **Editable Mesh** and Detach to identical parts.

![Imp043.gif](../../assets/maps/Imp043.gif)

The best way to do it is by Detaching it to 3 parts from **Top View**.
Select each part and detach it.

![Imp045.gif](../../assets/maps/Imp045.gif)

Select all parts of the helix, then go to *3DSMAX - Menu - File - Export Selected...*

Save it as **Helix.3ds**.

![Imp046.gif](../../assets/maps/Imp046.gif)

In **W3DEditor** in **3DS Import Form,** change options:

Off **Top Smooth**, On **Side Smooth**. Press *Open...*

![Imp047.gif](../../assets/maps/Imp047.gif)

Open **Helix.3ds** to import.

![Imp048.gif](../../assets/maps/Imp048.gif)

A nicer result should be obtained. Uncheck *Menu - View - Show 3D Model* to hide Mesh.

![Imp050.gif](../../assets/maps/Imp050.gif)

## 6. **Teapot**

A Teapot it a complex model, that's why it should be divided into simple objects.

![Imp052.gif](../../assets/maps/Imp052.gif)

Go to **3DSMAX**, select **Teapot** and **Convert** to **Editable Mesh**, in **Modify Panel.**

For the division, you have to use **Element** method.

![Imp053.gif](../../assets/maps/Imp053.gif)

Select the center of the teapot, as shown in screenshot, and Press **Detach**

![Imp054.gif](../../assets/maps/Imp054.gif)

Save part as **Teapot_01**.

![Imp055.gif](../../assets/maps/Imp055.gif)

Change select to **Polygon** mode.

And selects the lower part of the handle **Teapot**. Detach that part.

![Imp056.gif](../../assets/maps/Imp056.gif)

Again change select to **Element** Mode. And select the lid **Teapot**.

Press Detach and named as **Teapot_Top**. Also check **Detach As Clone** and press **OK**.

![Imp057.gif](../../assets/maps/Imp057.gif)

Select **Teapot_Top** and use **Select and Uniform Size** change size.

This must be done to obrtain the desired shape of future caps, so we need to enlarge it on the Y axis.

![Imp058.gif](../../assets/maps/Imp058.gif)

The next part to change is the **Center** of the **Teapot**, also change its size on the Y axis, don't forget to hold **Ctrl** to make a copy of this part.

Save the copy as **Teapot_cen**.

![Imp059.gif](../../assets/maps/Imp059.gif)

When all parts are made, select them and export.

![Imp060.gif](../../assets/maps/Imp060.gif)

Save as **Teapot.3ds** from *3DSMAX - Menu - File - Export Selected...*

![Imp061.gif](../../assets/maps/Imp061.gif)

Then go to *W3DEditor - Menu - File - Import 3ds...* And open **Teapot.3ds**

In order to get a better view, select **Teapot_cen** in the **Map Tree** and press *Edit Mode Panel - Hide Parent*

Changing the size of Y in such a way as to align it with the grid 3D model.

![Imp064.gif](../../assets/maps/Imp064.gif)

Its size should be about **15x6x15**

![Imp063.gif](../../assets/maps/Imp063.gif)

Next part for change is **Teapot_Top**.

Change size to obtain the desired shape. It should be about **10x1x10**

![Imp066.gif](../../assets/maps/Imp066.gif)

Remove unnecessary parts (old center and old caps)

Clear the view of the mesh press **Menu - View - Show 3D Model**

![Imp067.gif](../../assets/maps/Imp067.gif)

Turn off **Hide Patent** and then watch your full **Teapot**.

![Imp068.gif](../../assets/maps/Imp068.gif)

## 7. **Coloring**

In order to have a good view and learn about Color mode, you need to paint your objects.

![Imp070.gif](../../assets/maps/Imp070.gif)

The best way to paint many pieces of one figure in a single color is by applying the **Replacer**.

Beforehand, it is necessary to paint a piece in the desired color, and then you have to copy the ID of the poxel.

![Imp071.gif](../../assets/maps/Imp071.gif)

Then, select the root object, and through Replacer, apply color to all pieces.

![Imp072.gif](../../assets/maps/Imp072.gif)

If you don't have proper results you should **Update** the model, Save it and Open it again or simply apply textures.

![Imp074.gif](../../assets/maps/Imp074.gif)

Paint the other objects in a similar way.

![Imp076.gif](../../assets/maps/Imp076.gif)

When you obtain results similar to those in the screenshot you can save your map as **ComplexForm.xom**.

Now you can work in 3DImport of W3DEditor to create comlex objects.
