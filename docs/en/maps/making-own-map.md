# Making Own Map

## Tools

To create your own maps should be located following tools:

- W3DEditor v2.2 - Map Editor.
- W3DLauncher - Loader to load the list of maps and Mods.
- W3DIconEdit v1.0 - The creation of icons to maps.
- ScriptsEdit v 1.4 - Creating the Map List and add it to.

## Process

- In Map Editor (W3DEditor v2.2) making map **(\*.xom)**, texture list **(\*.txt)**. It files put in folder **Data\\Maps\\**.
- Then map add to Game (ScriptsEdit v 1.4), create Map List, there write name of map, texture list name, icon name, sky and others. Output file **Scripts\*.xom** saving to folder **Data\\MapPacks\\**.
- Run game with Launcher (W3DLauncher) select user mode and Script list.
- Next make screenshot of map. Open it in Icon Editor (W3DIconEdit v1.0) and get it from buffer, crop icon and save it **\*.tga** in **Data\\Frontend\\Levels\\**.
- Return to game and see icon .

## Tutorials

- [W3DEditor - Tutorial 1](tutorial-1.md)
- [W3DEditor - Tutorial 2](tutorial-2.md)

## Video Tutorials

The most obvious learning establish their video maps are examples:

- ~~[Video Tutorial as Make Map in W3DEditor v1.0](http://worms3d-portal.com/portal.php?topic_id=523)~~ - First Example, Import Objects, Make Land, Save map
- ~~[Video Tutorial 2 as Add Map](http://worms3d-portal.com/portal.php?topic_id=526)~~ - Add Map in game from ScriptsEdit.
- ~~[Video Tutorial Change Textures](http://worms3d-portal.com/portal.php?topic_id=558)~~ - Change textures and texture list.
- ~~[Video W3DEditor v1.8 Preview Copy Modes](http://worms3d-portal.com/portal.php?topic_id=770)~~ - Fast copy by mouse.
- ~~[Video W3DEditor v2.0 Preview](http://worms3d-portal.com/portal.php?topic_id=776)~~ - Change Layers and show the capacity of editor.
- [W3DEditor Tutorial 1: Poxels](http://www.youtube.com/watch?v=_bEg7XpU0Xk) - W3DEditor Tutorial 1: Poxels
- [W3DEditor Tutorial 2: Objects](http://www.youtube.com/watch?v=ltfpG4kC16I) - W3DEditor Tutorial 2: Objects
- [W3DEditor Tutorial 3: Textures](http://www.youtube.com/watch?v=ob7xtUCdoGU) - W3DEditor Tutorial 3: Textures
- [W3DEditor Tutorial 4: Script](http://www.youtube.com/watch?v=nk_7u5poc04) - W3DEditor Tutorial 4: Script
- [W3DEditor Tutorial 5: Playing](http://www.youtube.com/watch?v=0ROKlGX4QQw) - W3DEditor Tutorial 5: Playing
- [W3DEditor Tutorial 6: Emitters](http://www.youtube.com/watch?v=tpnSn4IH4-0) - W3DEditor Tutorial 6: Emitters

## Worms 4 Mayhem

How Add Map in Worms 4 list:

1\. in folder **data/maps/** put **yourmap.xom, yourhmap.hmp,
yourtexturelist.txt**

2\. in folder **data/databanks/** put **yourdata.xml**

``` xml
<?xml version='1.0'?>
<xomArchive>
<xomObjects>

<XDataBank id='id-0'>
<Section>0</Section>
<UintResources href='Databank.CustomDetailBank'/>
<UintResources href='Databank.CustomTextureBank'/>
<StringResources href='Databank.MaterialFile'/>
<StringResources href='Databank.Theme'/>
<StringResources href='Databank.Theme'/>
<StringResources href='Databank.TimeOfDay'/>
<StringResources href='Heightmap.BaseTexture'/>
<StringResources href='Heightmap.SecondTexture'/>
</XDataBank>

<XUintResourceDetails id='Databank.CustomDetailBank'>
<Value>1</Value>
<Name>Databank.CustomDetailBank</Name>
<Flags>72</Flags>
</XUintResourceDetails>

<XUintResourceDetails id='Databank.CustomTextureBank'>
<Value>6</Value>
<Name>Databank.CustomTextureBank</Name>
<Flags>72</Flags>
</XUintResourceDetails>

<XStringResourceDetails id='Databank.MaterialFile'>
<Value>Maps\\yourtexturelist.txt</Value>
<Name>Databank.MaterialFile</Name>
<Flags>64</Flags>
</XStringResourceDetails>

<XStringResourceDetails id='Databank.Theme'>
<Value>BUILDING</Value>
<Name>Databank.Theme</Name>
<Flags>64</Flags>
</XStringResourceDetails>

<XStringResourceDetails id='Databank.TimeOfDay'>
<Value>NIGHT</Value>
<Flags>64</Flags>
</XStringResourceDetails>

<XStringResourceDetails id='Heightmap.BaseTexture'>
<Value>C01</Value>
<Name>Heightmap.BaseTexture</Name>
<Flags>64</Flags>
</XStringResourceDetails>

<XStringResourceDetails id='Heightmap.SecondTexture'>
<Value>C03</Value>
<Name>Heightmap.SecondTexture</Name>
<Flags>64</Flags>
</XStringResourceDetails>

</xomObjects>
```

3\. change **scripts.xml** file in folder **data/Tweak**

``` xml
<?xml version='1.0'?>
<xomArchive>
<xomObjects>

<XDataBank id='id-0'>
<Section>0</Section>
...
<ContainerResources href='yourIDMAP'/>
<StringResources href='txt.yourMapName'/>
...
</XDataBank>
...
<XContainerResourceDetails id='yourIDMAP'>
<Value href='yourMapID-0'/>
<Name>YourMapName</Name>
<Flags>80</Flags>
</XContainerResourceDetails>

<WXFE_LevelDetails id='yourMapID-0'>
<Frontend_Name>txt.yourMapName</Frontend_Name>
<Frontend_Briefing></Frontend_Briefing>
<Frontend_Image>YourIcon.tga</Frontend_Image>
<Level_ScriptName>stdvs,wormpot</Level_ScriptName>
<Level_FileName>YourFileBaseName</Level_FileName>
<Objectives></Objectives>
<Level_Number>6</Level_Number>
<Level_Type>0</Level_Type>
<Lock></Lock>
<Theme_Type>5</Theme_Type>
<Preview_Type>0</Preview_Type>
<BonusTime>165</BonusTime>
</WXFE_LevelDetails>

<XStringResourceDetails id='txt.yourMapName'>
<Value>YourMapName</Value>
<Name>txt.yourMapName</Name>
<Flags>64</Flags>
</XStringResourceDetails>
...
```

It should work, good lucky!!!
