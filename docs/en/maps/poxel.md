# Poxel

**Poxel object** is type of Objects Map. First word "poxel" I hear by K^2. It as pixel but in 3D =) . This main type, all active objects in map it **Poxel object**.

**Structure**. Poxel object have parameters as position, orientation, size. You can move it as you want. Poxel object is many boxes (voxels). Poxel size parameter establishes amount voxels in Poxel objects.

![Poxel_1.gif](../../assets/maps/Poxel_1.gif)

**Layers** used for giving to Poxel object of necessary form.

![Poxel_2.gif](../../assets/maps/Poxel_2.gif)

**Height map** used for giving to Poxel object form noise surface.

![Poxel_3.gif](../../assets/maps/Poxel_3.gif)

**Poxel Texture** used for definitely view.

![Poxel_64.gif](../../assets/maps/Textures_64.gif)

**Texture Coordinate** for corect view textures.

![Poxel_4.gif](../../assets/maps/Poxel_4.gif)

**Surface** type Smooth Top/Side.

![Poxel_5.gif](../../assets/maps/Poxel_5.gif)
