# Change Gameplay

You can change the gameplay scripts that are loaded with the game.

## Worms 3D

For edit Map List used ScriptsEdit v1.4 (last version) by AlexBond, update you can found ~~[here](http://worms3d-portal.com)~~.

For change weapons need use scripts.

Example minomet weapon:

``` lua
-- Minomet
SetData("FE.Wormapedia.VaseTitle", "Minomet")
local ContainerLock,
Container = EditContainer("kWeaponBazooka")
Container.DisplayName = "FE.Wormapedia.VaseTitle"
Container.HasAdjustableHerd = true
Container.NumBomblets = 1
Container.LandDamageRadius = 1
Container.WormDamageMagnitude = 0
Container.ImpulseMagnitude = 0
Container.WormDamageRadius = 1
Container.ImpulseRadius = 1
Container.IsAffectedByWind = false
Container.PayloadGraphicsResourceID = "Landmine"
Container.BombletWeaponName = "kWeaponLandmine"
Container.DetonationSfx = ""
Container.DetonationFx = ""
Container.BombletMaxSpeed = 0.1
Container.BombletMaxConeAngle = 0.4
CloseContainer(ContainerLock)
```

If you find easy way W3D WeaponEditor for you. It can change properties of weapons, but not full . Download W3DWeapEdit can ~~[here](http://worms3d-portal.com/viewtopic.php?t=612)~~. Official page utill - ~~[here](http://www.megamods.de/_data/overview.php?gui=1&game=7)~~.

### Objects Worms Lua

List of global values of Worms3D

- `Worm.AimMouseLR.Sensitivity = 0.00`
- `Worm.AimMouseUD.Sensitivity = 0.00`
- `Worm.Angry ???`
- `Worm.Antidote ???`
- `Worm.ApplyLightside ???`
- `Worm.ApplyPoison = Message`
- `Worm.Backflip ???`
- `Worm.Blink = 1`
- `Worm.BounceMultiplier = 0.30`
- `Worm.BounceMultiplierDefault = 0.30`
- `Worm.Brows ???`
- `Worm.CamTrackMe ???`
- `Worm.Chute ???`
- `Worm.ClearUtilities ???`
- `Worm.CollidedID ???`
- `Worm.Collision.ZOffset = -5.00`
- `Worm.CollisionNotification ???`
- `Worm.CreepAnimSpeedScale = 30.00`
- `Worm.DamageComplete ???`
- `Worm.Damaged ???`
- `Worm.Damaged.Current ???`
- `Worm.Data%.2d = Container - array of worm`
- `Worm.Dazed ???`
- `Worm.DeathImpulseMagnitude = 0.50`
- `Worm.DeathImpulseRadius = 30.00`
- `Worm.DeathLandDamageRadius = 0.00`
- `Worm.DeathWormDamageMagnitude = 30.00`
- `Worm.DeathWormDamageRadius = 50.00`
- `Worm.Die ???`
- `Worm.Died ???`
- `Worm.DieQuietly ???`
- `Worm.DisableMovementRef = 0`
- `Worm.Drown.HeightOffset = 7.00`
- `Worm.DrunkRedbull ???`
- `Worm.Explode ???`
- `Worm.FallDamageRatio = 100.00`
- `Worm.Flap ???`
- `Worm.FPHands ???`
- `Worm.Gfx.AftertouchSmooth = 0.75`
- `Worm.GoodShot ???`
- `Worm.Headers ???`
- `Worm.HeadScratch ???`
- `Worm.Hide ???`
- `Worm.Hop.Velocity = \<x:0.00 y:0.11 z:0.02\>`
- `Worm.HopTest.Front = 13.00`
- `Worm.HopTest.Height = 25.00`
- `Worm.IgnoreCollisionID = 0`
- `Worm.IsSpeaking ???`
- `Worm.Jump.Backflip = \<x:-0.04 y:0.20 z:-0.04\>`
- `Worm.Jump.Backward = \<x:-0.04 y:0.15 z:-0.04\>`
- `Worm.Jump.Forward = \<x:0.07 y:0.15 z:0.07\>`
- `Worm.Jump.Upward = \<x:0.00 y:0.17 z:0.00\>`
- `Worm.JumpAftertouch.Multiplier = 0.02`
- `Worm.Knock2 ???`
- `Worm.Knock3 ???`
- `Worm.Knock4 ???`
- `Worm.Land.Back ???`
- `Worm.Land.Flip ???`
- `Worm.Land.Front ???`
- `Worm.Land.Up ???`
- `Worm.LandDeath ???`
- `Worm.MaxSlopeAngleRadians = 1.45`
- `Worm.MaxSlopeBlastLandAngRads = 0.90`
- `Worm.MaxSlopeJumpLandAngRads = 1.50`
- `Worm.Mesh ???`
- `Worm.NewPosition ???`
- `Worm.Nips ???`
- `Worm.NowUpdateGraphic ???`
- `Worm.ParabolaIntersect ???`
- `Worm.Periscope ???`
- `Worm.Poison ???`
- `Worm.Poison.Default = 10`
- `Worm.Pop ???`
- `Worm.QueueAnim = IntMessage`
- `Worm.RemoveFromTeamQueue ???`
- `Worm.Reorient ???`
- `Worm.ResetAnim = IntMessage`
- `Worm.Respawn ???`
- `Worm.Sad ???`
- `Worm.Say.Bummer ???`
- `Worm.Say.Collect ???`
- `Worm.Say.ComeOnThen ???`
- `Worm.Say.Coward ???`
- `Worm.Say.Drop ???`
- `Worm.Say.Fatality ???`
- `Worm.Say.Fire ???`
- `Worm.Say.Hurry ???`
- `Worm.Say.Laugh ???`
- `Worm.Say.Missed ???`
- `Worm.Say.No ???`
- `Worm.Say.Ouch ???`
- `Worm.Say.Revenge ???`
- `Worm.Say.RunAway ???`
- `Worm.Say.StupidFirstBlood ???`
- `Worm.Say.Victory ???`
- `Worm.Scale = 1.00`
- `Worm.ScriptAnim = Set me`
- `Worm.ScriptDrawAnim ???`
- `Worm.ScriptSpeech = Laugh`
- `Worm.SetBlink ???`
- `Worm.Show ???`
- `Worm.ShowHealth = 1`
- `Worm.ShowName = 1`
- `Worm.SlideStopVel = 0.06`
- `Worm.SlideStopVelDefault = 0.06`
- `Worm.StepUpHeight = 5.00`
- `Worm.StillWithinIgnored = 0`
- `Worm.SurrenderAnim ???`
- `Worm.Talk ???`
- `Worm.Tash ???`
- `Worm.TimeToDie ???`
- `Worm.Track.Projectile ???`
- `Worm.UpdateGraphicalOrientation ???`
- `Worm.UtilityUsed ???`
- `Worm.VectorIntersect ???`
- `Worm.Walk ???`
- `Worm.Walk.Speed = 0.00`
- `Worm.WalkAnimSpeedScale = 30.00`
- `Worm.WalkOffCliffVelMulti = 0.70`
- `Worm.WaterDeath ???`
- `Worm.Wriggle ???`

- `Ninja.DetachVelocityMulti = 1.00`
- `Ninja.GravityMultiplier = 1.00`
- `Ninja.LengthenShortenRate = 0.20`
- `Ninja.MaxLength = 450.00`
- `Ninja.MaxUnreducedSwingLength = 40.00`
- `Ninja.MinBendDistFromWorm = 30.00`
- `Ninja.MinLength = 30.00`
- `Ninja.NumRaycastRefinements = 8`
- `Ninja.NumShots = 5`
- `Ninja.RotationDamping = 0.99`
- `Ninja.SnapOffAngleRadians = 7.50`
- `Ninja.SwingAmount = 0.00`
- `Ninja.WormMass = 100.00`

- `Jetpack.AnimSmoothFB = 0.15`
- `Jetpack.AnimSmoothLR = 0.04`
- `Jetpack.FwdThrustRotation = 0.20`
- `Jetpack.InitFuel = 7500`
- `Jetpack.MaxAltitude = 1500.00`
- `Jetpack.MaxGfxXOrient = 1.05`
- `Jetpack.OverCeilingThrustMod = 0.05`
- `Jetpack.SuperThrustAccel = 0.30`
- `Jetpack.SuperThrustMax = 1.00`
- `Jetpack.SuperThrustMod = 0.20`
- `Jetpack.SuperThrustReduct = 0.97`
- `Jetpack.SuperThrustShutOff = 0.01`
- `Jetpack.ThrustScale = 0.00`
- `Jetpack.TurnRotationSpeed = 0.02`
- `Jetpack.XZWindResNoThrust = 0.98`
- `Jetpack.XZWindResThrust = 1.00`

- `Crate.ImpulseMagnitude = 0.50`
- `Crate.ImpulseRadius = 65.00`
- `Crate.LandDamageRadius = 45.00`
- `Crate.WormDamageMagnitude = 30.00`
- `Crate.WormDamageRadius = 45.00`

- `Crate.CanDropFromChute = 1`
- `Crate.Contents = kWeaponSuperSheep`
- `Crate.DelayMillisec = 0`
- `Crate.DisableMessage = 0`
- `Crate.ExplicitSpawnPos = \<x:0.00 y:0.00 z:0.00\>`
- `Crate.FallSpeed = 0.00`
- `Crate.Gravity = 1`
- `Crate.GroundSnap = 0`
- `Crate.HealthInCrates = 25`
- `Crate.Hitpoints = 25`
- `Crate.HitpointsMultiplier = 0.50`
- `Crate.Index = 65535`
- `Crate.LifetimeSec = -1.00`
- `Crate.LifetimeTurns = 65535`
- `Crate.NumContents = 1`
- `Crate.Parachute = 1`
- `Crate.Pushable = 1`
- `Crate.RandomSpawnPos = 0`
- `Crate.Scale = 1.00`
- `Crate.Showered = 0`
- `Crate.ShowerSpawnPos = \<x:0.00 y:500.00 z:0.00\>`
- `Crate.Spawn = CrateSpawn`
- `Crate.TeamCollectable = 65535`
- `Crate.TeamDestructible = 65535`
- `Crate.TrackCam = 1`
- `Crate.Type = Weapon`
- `Crate.UXB = 0`
- `Crate.WaitTillLanded = 1`

- `String.DestDataName =`
- `String.ReplaceString =`
- `String.SearchString =`
- `String.SourceDataName =`

- `Game.BriefingText =`
- `Game.DefaultLevel = FE.Level.Multiapplecore`
- `Game.Health = 100`
- `Game.MaxTeams = 4`
- `Game.NextLevel = FE.Level.Multiapplecore`
- `Game.NumPlayers = 0`
- `Game.NumTeams = 0`
- `Game.RoundTime = 900`
- `Game.SelectWorm = 0`
- `Game.Start.Text =`
- `Game.TeleportIn = 0`
- `Game.TurnTime = 60000`
- `Game.Username =`
- `Game.HostIsLocal = 0`
- `Game.hostname =`
- `Game.League =`
- `Game.MaxPlayers = 4`
- `Game.Scope = 0`
- `Game.TimeStamp = 0`
- `Game.Victories = 0`
- `Game.winner =`

- `GameLogic.AboutToApplyDamage = Message`
- `GameLogic.ActivateNextWorm = Message`
- `GameLogic.ActivateSuddenDeath`
- `GameLogic.ActiveWormChanged`
- `GameLogic.AddInventory`
- `GameLogic.AddInventory.Arg0 =`
- `GameLogic.AddInventory.Arg1 =`
- `GameLogic.AddMeToDeathQueue`
- `GameLogic.AITurn.Started`
- `GameLogic.AllTeamsHadTurn = 0`
- `GameLogic.ApplyDamage = Message`
- `GameLogic.ArtilleryMode = 0`
- `GameLogic.Challenge.Failure`
- `GameLogic.Challenge.Result`
- `GameLogic.ChangeWind`
- `GameLogic.ClearInventories = Message`
- `GameLogic.CrateShower = Message`
- `GameLogic.CreateAirstrike`
- `GameLogic.CreateChicken`
- `GameLogic.CreateCrate = Message`
- `GameLogic.CreateNuclearEffect`
- `GameLogic.CreateRandomMine`
- `GameLogic.CreateRandomOildrum`
- `GameLogic.CreateTrigger = Message`
- `GameLogic.CurrentScript = stdvs`
- `GameLogic.DecrementInventory`
- `GameLogic.DecrementInventory.Id`
- `GameLogic.DecrementWeaponDelays`
- `GameLogic.DestroyTrigger`
- `GameLogic.DoubleTurnTime`
- `GameLogic.Draw = Message`
- `GameLogic.DrawImmediately`
- `GameLogic.DropRandomCrate`
- `GameLogic.EndTurn`
- `GameLogic.EndTurn.Immediate`
- `GameLogic.EnemyDamage`
- `GameLogic.ForceSuddenDeath`
- `GameLogic.FriendlyDamage`
- `GameLogic.GameLoadComplete`
- `GameLogic.GetAllTeamsHadTurn`
- `GameLogic.GotoFrontEnd`
- `GameLogic.GunWaiting`
- `GameLogic.IncrementInventory`
- `GameLogic.LoadPowerWeaponTweaks`
- `GameLogic.Mission.Failure = Message`
- `GameLogic.Mission.Success = Message`
- `GameLogic.NoActivity`
- `GameLogic.PauseGame = Message`
- `GameLogic.PlaceMine`
- `GameLogic.PlaceObjects = Message`
- `GameLogic.QuitGame`
- `GameLogic.RequestWeaponIndex`
- `GameLogic.RequestWeaponName`
- `GameLogic.ResetChickenParams`
- `GameLogic.ResetCrateParameters`
- `GameLogic.ResetTriggerParams`
- `GameLogic.ResumeGame = Message`
- `GameLogic.RoundTime.Pause = Message`
- `GameLogic.RoundTime.Resume = Message`
- `GameLogic.SetGameScope`
- `GameLogic.StartGame`
- `GameLogic.SuddenDamageMode = 0`
- `GameLogic.Timeout`
- `GameLogic.Timer0`
- `GameLogic.Timer1`
- `GameLogic.Timer2`
- `GameLogic.Timer3`
- `GameLogic.Timer4`
- `GameLogic.Timer5`
- `GameLogic.Timer6`
- `GameLogic.Timer7`
- `GameLogic.Timer8`
- `GameLogic.Timer9`
- `GameLogic.TriggerGoodShot`
- `GameLogic.Turn.Ended = Message`
- `GameLogic.Turn.Started = Message`
- `GameLogic.Turn.WormUpdated`
- `GameLogic.TurnTime.Pause = Message`
- `GameLogic.TurnTime.Resume = Message`
- `GameLogic.Tutorial.End`
- `GameLogic.Tutorial.Failure`
- `GameLogic.Tutorial.Success`
- `GameLogic.WeaponIndex = 0`
- `GameLogic.WeaponName =`
- `GameLogic.Win = IntMessage`

- `Land.File = flattest.xom`
- `Land.LightGradientResource = LightGradient_E_DAY`
- `Land.Materials = ThemeEngland\\ThemeEngland.txt`
- `Land.SkyBoxResource = ENGLAND.DAYSky`
- `Land.Theme = ENGLAND`
- `Land.TimeOfDay = DAY`
- `Land.WaterDetailResource =`
- `Land.WaterOffsetResource = ENGLAND.DAYWaterOffs`
- `Land.WaterResource = ENGLAND.DAYWater`
- `Land.OverrideScale = 20.00`
- `Land.Center = \<x:0.00 y:0.00 z:0.00\>`
- `Land.Changed = 0`
- `Land.DebugCount1 = 0`
- `Land.DebugCount2 = 0`
- `Land.Finished = 0`
- `Land.FringeLength = 0.40`
- `Land.HighResCollision = 0`
- `Land.IgnoreInitialCollision = 0`
- `Land.ImpactNorm = \<x:0.00 y:0.00 z:0.00\>`
- `Land.ImpactPos = \<x:0.00 y:0.00 z:0.00\>`
- `Land.ImpactTime = 0`
- `Land.ImpactTimeScale = 0.00`
- `Land.Indestructable = 0`
- `Land.InitialMaxHeight = 0.00`
- `Land.LocalImpactPos = \<x:0.00 y:0.00 z:0.00\>`
- `Land.MaxBounds = \<x:0.00 y:0.00 z:0.00\>`
- `Land.MaxHeight = 1.00`
- `Land.MaxImpactTime = 0`
- `Land.MaxParabolaRange = 0.00`
- `Land.MinBounds = \<x:0.00 y:0.00 z:0.00\>`
- `Land.Parabola_A = \<x:0.00 y:0.00 z:0.00\>`
- `Land.Parabola_I = \<x:0.00 y:0.00 z:0.00\>`
- `Land.Parabola_V = \<x:0.00 y:0.00 z:0.00\>`
- `Land.ParabolaPlaneNormal = \<x:0.00 y:0.00 z:0.00\>`
- `Land.Permanent = 0`
- `Land.ProjectileRadius = 0.00`
- `Land.Radius = 1.00`
- `Land.UniqueID = 0`
- `Land.VoxelContents = 0`
- `Land.VoxelImpactPos = 0`

- `Water.ExpiryDepth = -200.00`
- `Water.Level = 0.00`
- `Water.SwapBlends = 0`
- `Water.TweakName = Water.ENGLAND.DAY`
- `Water.CentreColour = <r:160 g:160 b:160 a:240>`
- `Water.InnerColour = <r:160 g:160 b:160 a:210>`
- `Water.MiddleColour = <r:160 g:160 b:160 a:128>`
- `Water.OuterColour = <r:160 g:160 b:160 a:100>`
- `Water.RimColour = <r:160 g:160 b:160 a:0>`
- `Water.RiseAmount = 25.00`
- `Water.Density = 5.83`
- `Water.Direction = 0.00`
- `Water.RiseSpeed.Current = 0`
- `Water.RiseSpeed.Fast = 16`
- `Water.RiseSpeed.Graphic = 0.10`
- `Water.RiseSpeed.Medium = 8`
- `Water.RiseSpeed.Slow = 4`
- `Water.Speed = 1.00`

### Main scripts-functions of Worms Lua

- `SendMessage("Earthquake.End")` - for end earthquake
- `worm = QueryWormContainer()` - query current worm to "worm"
- `WormContainerName = GetWormContainerName(WormIndex)` - Get container of worm from index. The function sets the index instead of the word from the array worm01..worm16
- `Weapon_Fired_End()` - function called when fire end, weapon was used.

Default code:

``` lua
function Weapon_Fired_Start()
  SendMessage("Timer.StartRetreatTimer")
  SendMessage("Weapon.Delete")
end
```

- `TurnEnded()` - function call when turn end.

Default code:

``` lua
function TurnEnded()
  CheckOneTeamVictory()
end
```

- `EditContainer(ContainerName)` - fucntion for get data container from name, also for edit this data. Read and write.

Example:

``` lua
local lock, worm = EditContainer(WormContainerName)
  -- change value
  worm.Energy = worm.Energy - 10
CloseContainer(lock)
```

- `QueryContainer(StringName)` - function for read data, only read.
- `SendMessage("CommentaryPanel.ScriptText")` - put massage to screen.

Example:

``` lua
SetData("Text.TestComment", "Hello World!!!")
SetData("CommentaryPanel.Comment", "Text.TestComment")
SendMessage("CommentaryPanel.ScriptText")
```

- `Weapon_Created()` - function called when weapon was choose.

Example:

``` lua
function Weapon_Created()
  worm = QueryWormContainer()
  if worm.WeaponIndex == "WeaponNuclearBomb" then
    -- your code here
  end
end
```

- `SendMessage("GameLogic.CreateRandomMine")` - create random mine.
- `DoWormpotOncePerTurnFunctions()` - function called before start game for wormpot actions.
- `SendMessage("RandomNumber.Get")` - get random value.
- `RandNumb = GetData("RandomNumber.Uint")` - Get random number.
- `StartTimer("FuncName", 1000)` - make timer on 1 second for call function FuncName.
- `SendMessage("GameLogic.CrateShower")` - call many crates in sky.
- `SendMessage("Earthquake.Start")` - call earthquake.
- `SetData("DoubleDamage", 1)` - put Double Damage
- `SendMessage("GameLogic.CreateCrate")` - create crate.

Example:

``` lua
SetData("Crate.Type", "Weapon") -- type of crate box, weapon
SetData("Crate.GroundSnap", 1) -- ground snap
SetData("Crate.Contents", "kWeaponBazooka") -- weapon name
SetData("Crate.Parachute", 0) -- without parachute
SetData("Crate.LifetimeSec", -1) -- life time unlimited
SendMessage("GameLogic.CreateCrate")
```

- `SendIntMessage("Worm.QueueAnim", iWormIndex)` - call animation of worm iWormIndex.

Example:

``` lua
SetData("Worm.ScriptAnim", "AimBazooka")
SendIntMessage("Worm.QueueAnim", iWormIndex)
```

- `worm.Position` - read vector value:

Example:

``` lua
SetData(    "Text.TestComment",   worm.Position)   --instead worm.Position can be any vector
SubString( "Text.TestComment",   "Text.TestComment" , "(" , "Vector = {" )
SubString( "Text.TestComment",   "Text.TestComment" , ")" , "}" )
local TextTable = GetData("Text.TestComment")
assert(loadstring(TextTable))()

--Vector имеет поля x,y,z
SetData("Text.TestComment", "Position Y = "..Vector.y)
SetData("CommentaryPanel.Comment", "Text.TestComment")
SendMessage("CommentaryPanel.ScriptText")
```

## Worms 4: Mayhem

Here put list of functions for change lua in Worms 4:Mayhem

- `function EFMV_Terminated()` - переопределяемая функция. Вызывается после того, как проигран мультик, вызванный к примеру таким способом:

``` lua
SetData("EFMV.Unskipable", 0)
SetData("EFMV.MovieName", "EFMV.Intro")
SendMessage("EFMV.Play")
```

- `function Initialise()` - redefine the functions. Called at run script, initialization is carried out here, placement of worms, team building and so on. For example:

``` lua
function Initialise() 
  kVariables()                    -- Lets have a butchers at the variables we're gonna use before the deathmatch begins.    
  kDialogueBoxTimeDelay = 0500    -- This is the delay between selecting a weapon and having the game display a dialogue box.
  SendMessage("Commentary.NoDefault")
  SetData("Mine.DetonationType", 1)
  SetData("Mine.DudProbability", 0) SetData("Mine.MinFuse", 5000) SetData("Mine.MaxFuse", 5000) 
  SendMessage("GameLogic.PlaceObjects")
  SetData("HotSeatTime", 0) SetData("RoundTime", -1) SetData("TurnTime", 90000) 
  lib_SetupTeam(0, "Team_Human")
  lib_SetupTeam(1, "Team_Enemy")
  lib_SetupTeam(2, "Team_EFMV")  
  lib_SetupWorm(0, "Worm.Game1.Human0")
  lib_SetupWorm(1, "Worm.Game1.Human1")
  lib_SetupWorm(2, "Worm.Game1.Human2")
  lib_SetupWorm(3, "Worm.Game1.Human3")
  lib_SetupTeamInventory(0, "Inventory_Human")
  lib_SetupTeamInventory(1, "Inventory_Enemy")
  SendMessage("WormManager.Reinitialise")
  SetData("Trigger.Visibility",0)
  SetData("Land.Indestructable",1)
  SetData("Wind.Speed", 0)
  lock, scheme = EditContainer("GM.SchemeData")
  scheme.HelpPanelDelay = 0          
  CloseContainer(lock)
  SetData("EFMV.Unskipable", 0)
  -- Kick the game off by running the first little EFMV snippet.
  kPlayEFMV("EFMV.Intro")
end 
```

- `function TurnStarted()` - redefine the functions. Called at the beginning of turn, there is usually conducted by individual tuning of progress

-----

This not end...
