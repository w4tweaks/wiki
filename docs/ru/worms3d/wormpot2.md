# Wormpot 2

## Комбинации Wormpot 2

Для активаций оружия используются следующие комбинации:

<table>
<tr>
<th width="240px">Комбинация</th>
<th>Результат</th>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot063.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot065.jpg" width="64px"></td>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot066.jpg" width="64px" align="right"> Режим Землетрясения</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot067.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot068.jpg" width="64px"></td>
<td>Режим Дракулы</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot069.jpg" width="64px"></td>
<td>
<img src="../../../assets/worms3d/wormpot2/Wormpot070.jpg" width="64px" align="right">
Базука X 5<br>
Дробовик X 4<br>
Узи c Лазерным прицелом
</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot071.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot072.jpg" width="64px"></td>
<td>Лазер (Духовая Трубка)</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot069.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot071.jpg" width="64px"></td>
<td>
Кластерная мина (Кластерная граната)<br>
Магическая пуля (Самонаводящаяся ракета)<br>
Импульсовый сброс (Канистра с газом)
</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot073.jpg" width="64px"></td>
<td>
Отъезжающая камера<br>
Топливо X 5<br>
Граната X 5<br>
Супер Бита (Банановая Бомба)
</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot074.jpg" width="64px"></td>
<td>Мега Толчок (Толчок 10-й)</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot073.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot074.jpg" width="64px"></td>
<td>
Супер Банка (Кластерная Граната)<br>
Ядро (Святая Граната)<br>
Жгучий Коктель Молотова<br>
Экстра бомбардировки: Напалмавая - Минная - Ядровая - Овцевая - Бейсбольная - Динамитная - Бутылочная
</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot075.jpg" width="64px"></td>
<td>
Миномёт (Базука)<br>
Кластерная Бомба X 16<br>
Мортира X 10<br>
Воздушная бомбардировка X 10<br>
Банановая бомба X 5
</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot075.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot076.jpg" width="64px"></td>
<td>
Супер Банановая Бомба (Кластерная Граната)<br>
МегаГраната (Граната)<br>
Липкая Кластерная Граната (Канистра с газом)
</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot077.jpg" width="64px"></td>
<td>
ОвцеБазука (Базука)<br>
Голубь Бомба (Банановая Бомба)
</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot077.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot078.jpg" width="64px"></td>
<td>Овце Бомбардировка (Воздушная бомбардировка)</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot077.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot078.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot065.jpg" width="64px"></td>
<td>День Осла</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot079.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot074.jpg" width="64px"></td>
<td>Скоростное Оружие</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot079.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot080.jpg" width="64px"></td>
<td>
Перезарядные бомбардировки: Кластерная - Святая - Банановая - Минная - Динамитная - Овцевая - Бутылочная
</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot081.jpg" width="64px"></td>
<td>Режим Регенерации</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot063.jpg" width="64px"></td>
<td>Взрывной Червь</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot063.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot082.jpg" width="64px"></td>
<td>Десятикратная Гравитация</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot083.jpg" width="64px"></td>
<td>Спрятаное Оружие (Количество равно количеству СуперОвец)</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot083.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot084.jpg" width="64px"></td>
<td>Все Оружие (Количество равно количеству СуперОвец)</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot083.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot077.jpg" width="64px"></td>
<td>Включить в игру Бетонного Осла (Количество равно количеству СуперОвец)</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot082.jpg" width="64px"></td>
<td>
Бесконечная Веревка<br>
Веревка+Базука (Динамит)
</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot085.jpg" width="64px"></td>
<td>
Бесконечная Веревка<br>
Режим расширеного обзора<br>
Веревки Скоростные черви (10-я Скакалка)<br>
Супер Реактивный Ранец
</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot085.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot086.gif" width="64px"></td>
<td>Очень длинная веревка</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot085.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot087.jpg" width="64px"></td>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot088.jpg" width="64px" align="right"> Очень быстрая веревка</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot085.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot086.gif" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot087.jpg" width="64px"></td>
<td>Супер Веревка Ниндзя</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot089.jpg" width="64px"> <img src="../../../assets/worms3d/wormpot2/Wormpot068.jpg" width="64px"></td>
<td>Отключить прицел от первого лица (Q)</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot084.jpg" width="64px"></td>
<td>Бесконечное время</td>
</tr>
<tr>
<td><img src="../../../assets/worms3d/wormpot2/Wormpot087.jpg" width="64px"></td>
<td>Smoke Bomb (Gas Canister 3)</td>
</tr>
</table>
