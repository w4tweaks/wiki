# Моды

## Multiplayer Map Pack 4.0

Включает:

1. PNM 4.420
2. TweakPack
3. NTXPack 1.2
4. Поддержка OpenSpy
5. 433 карты для мультиплеера

> Ссылка на [Multiplayer Map Pack 4.0](http://www.moddb.com/mods/worms-3d-multiplayer-mappack/downloads/worms-3d-multiplayer-map-pack-v40) (197.21 Mb)

## NTXPack 1.2

![NTXPack12.jpg](../../assets/worms3d/NTXPack12.jpg)

Список карт:

1.  All Systems Oh No! (WUM)
2.  Blue Dragon (WF by Woitek)
3.  Cards Of Alice (by AlexBond)
4.  Christmas House (by Woitek)
5.  Fishes (By AlexBond)
6.  Maim Frame (WUM)
7.  Monster Bash (WUM)
8.  Mordred And Morgana (WF by Woitek)
9.  Pesky Little Breeders (WUM)
10. Question of Faith (WF by Woitek)
11. Terminal Ferocity (WUM)
12. Time Flies (WUM)
13. Towering Infernal (WUM)

Все карты с оригинальными текстурами, поэтому выглядят очень четко и красочно. Для запуска нужно в W3DLauncher'е выбрать Scripts_NTXPack.xom

> [Ссылка на NTXPack 1.2](http://yadi.sk/d/p_z4HZ6k4gnLk)

## Tweaks Pack + W3DLauncher

Этот пак модов для Worms 3D

![greenhud01.jpg](../../assets/worms3d/greenhud01.jpg)

Содержит в себе:

- W3DLauncher 4.420

![w3dloader.png](../../assets/worms3d/w3dloader.png)

- ArmyHUD
- ArmyHUD+Sky
- DarkSky
- GreenHUD
- GreenHUD+Sky
- MKSky

> [Ссылка на TweaksPack](http://yadi.sk/d/W1Fnc1Kq3neB4)

## PNM Packs 4.365

![PNM.jpg](../../assets/worms3d/PNM.jpg)

Все версии Patch for New Maps:

> [PNM 4.250](http://www.playground.ru/files/patch_worms_3d_new_maps_v_4_250_wormpot_2-12240/) (250 карт)

> [PNM 4.315](http://yadi.sk/d/Cn76uTWJ6Sq2C) (315 карт)

> [PNM 4.365](http://yadi.sk/d/sn0cTRmq6Spoq) (365 карт)
