# Изменение геймплея

Изменять геймплей можно lua скриптами которые загружаются с игрой.

## Worms 3D

Для редактирования Списка Карт используется ScriptsEdit v1.4 by AlexBond, новые версии можно всегда найти ~~[здесь](http://worms3d-portal.com)~~.

Для изменения оружия нужно использовать скрипты.

Пример таких скриптов:

``` lua
-- Minomet
SetData("FE.Wormapedia.VaseTitle", "Minomet")
local ContainerLock, Container = EditContainer("kWeaponBazooka")
Container.DisplayName = "FE.Wormapedia.VaseTitle"
Container.HasAdjustableHerd = true
Container.NumBomblets = 1
Container.LandDamageRadius = 1
Container.WormDamageMagnitude = 0
Container.ImpulseMagnitude = 0
Container.WormDamageRadius = 1
Container.ImpulseRadius = 1
Container.IsAffectedByWind = false
Container.PayloadGraphicsResourceID = "Landmine"
Container.BombletWeaponName = "kWeaponLandmine"
Container.DetonationSfx = ""
Container.DetonationFx = ""
Container.BombletMaxSpeed = 0.1
Container.BombletMaxConeAngle = 0.4
CloseContainer(ContainerLock)
```

Но это тяжело и довольно неудобно. Легче можно сделать с W3D WeaponEditor. Он позволяет менять характеристики оружия, но некоторые опции отсутствуют. Их придется править вручную. Скачать W3DWeapEdit можно ~~[здесь](http://worms3d-portal.com/viewtopic.php?t=612)~~. Официальная страница утилиты - ~~[тут](http://www.megamods.de/_data/overview.php?gui=1&game=7)~~.

### Объекты Worms Lua

Список глобальных переменных Worms3D

 - ``Worm.AimMouseLR.Sensitivity = 0.00``
 - ``Worm.AimMouseUD.Sensitivity = 0.00``
 - ``Worm.Angry ???``
 - ``Worm.Antidote ???``
 - ``Worm.ApplyLightside ???``
 - ``Worm.ApplyPoison = Message``
 - ``Worm.Backflip ???``
 - ``Worm.Blink = 1``
 - ``Worm.BounceMultiplier = 0.30``
 - ``Worm.BounceMultiplierDefault = 0.30``
 - ``Worm.Brows ???``
 - ``Worm.CamTrackMe ???``
 - ``Worm.Chute ???``
 - ``Worm.ClearUtilities ???``
 - ``Worm.CollidedID ???``
 - ``Worm.Collision.ZOffset = -5.00``
 - ``Worm.CollisionNotification ???``
 - ``Worm.CreepAnimSpeedScale = 30.00``
 - ``Worm.DamageComplete ???``
 - ``Worm.Damaged ???``
 - ``Worm.Damaged.Current ???``
 - ``Worm.Data%.2d = Container - массив червяков``
 - ``Worm.Dazed ???``
 - ``Worm.DeathImpulseMagnitude = 0.50``
 - ``Worm.DeathImpulseRadius = 30.00``
 - ``Worm.DeathLandDamageRadius = 0.00``
 - ``Worm.DeathWormDamageMagnitude = 30.00``
 - ``Worm.DeathWormDamageRadius = 50.00``
 - ``Worm.Die ???``
 - ``Worm.Died ???``
 - ``Worm.DieQuietly ???``
 - ``Worm.DisableMovementRef = 0``
 - ``Worm.Drown.HeightOffset = 7.00``
 - ``Worm.DrunkRedbull ???``
 - ``Worm.Explode ???``
 - ``Worm.FallDamageRatio = 100.00``
 - ``Worm.Flap ???``
 - ``Worm.FPHands ???``
 - ``Worm.Gfx.AftertouchSmooth = 0.75``
 - ``Worm.GoodShot ???``
 - ``Worm.Headers ???``
 - ``Worm.HeadScratch ???``
 - ``Worm.Hide ???``
 - ``Worm.Hop.Velocity = <x:0.00 y:0.11 z:0.02>``
 - ``Worm.HopTest.Front = 13.00``
 - ``Worm.HopTest.Height = 25.00``
 - ``Worm.IgnoreCollisionID = 0``
 - ``Worm.IsSpeaking ???``
 - ``Worm.Jump.Backflip = <x:-0.04 y:0.20 z:-0.04>``
 - ``Worm.Jump.Backward = <x:-0.04 y:0.15 z:-0.04>``
 - ``Worm.Jump.Forward = <x:0.07 y:0.15 z:0.07>``
 - ``Worm.Jump.Upward = <x:0.00 y:0.17 z:0.00>``
 - ``Worm.JumpAftertouch.Multiplier = 0.02``
 - ``Worm.Knock2 ???``
 - ``Worm.Knock3 ???``
 - ``Worm.Knock4 ???``
 - ``Worm.Land.Back ???``
 - ``Worm.Land.Flip ???``
 - ``Worm.Land.Front ???``
 - ``Worm.Land.Up ???``
 - ``Worm.LandDeath ???``
 - ``Worm.MaxSlopeAngleRadians = 1.45``
 - ``Worm.MaxSlopeBlastLandAngRads = 0.90``
 - ``Worm.MaxSlopeJumpLandAngRads = 1.50``
 - ``Worm.Mesh ???``
 - ``Worm.NewPosition ???``
 - ``Worm.Nips ???``
 - ``Worm.NowUpdateGraphic ???``
 - ``Worm.ParabolaIntersect ???``
 - ``Worm.Periscope ???``
 - ``Worm.Poison ???``
 - ``Worm.Poison.Default = 10``
 - ``Worm.Pop ???``
 - ``Worm.QueueAnim = IntMessage``
 - ``Worm.RemoveFromTeamQueue ???``
 - ``Worm.Reorient ???``
 - ``Worm.ResetAnim = IntMessage``
 - ``Worm.Respawn ???``
 - ``Worm.Sad ???``
 - ``Worm.Say.Bummer ???``
 - ``Worm.Say.Collect ???``
 - ``Worm.Say.ComeOnThen ???``
 - ``Worm.Say.Coward ???``
 - ``Worm.Say.Drop ???``
 - ``Worm.Say.Fatality ???``
 - ``Worm.Say.Fire ???``
 - ``Worm.Say.Hurry ???``
 - ``Worm.Say.Laugh ???``
 - ``Worm.Say.Missed ???``
 - ``Worm.Say.No ???``
 - ``Worm.Say.Ouch ???``
 - ``Worm.Say.Revenge ???``
 - ``Worm.Say.RunAway ???``
 - ``Worm.Say.StupidFirstBlood ???``
 - ``Worm.Say.Victory ???``
 - ``Worm.Scale = 1.00``
 - ``Worm.ScriptAnim = Set me``
 - ``Worm.ScriptDrawAnim ???``
 - ``Worm.ScriptSpeech = Laugh``
 - ``Worm.SetBlink ???``
 - ``Worm.Show ???``
 - ``Worm.ShowHealth = 1``
 - ``Worm.ShowName = 1``
 - ``Worm.SlideStopVel = 0.06``
 - ``Worm.SlideStopVelDefault = 0.06``
 - ``Worm.StepUpHeight = 5.00``
 - ``Worm.StillWithinIgnored = 0``
 - ``Worm.SurrenderAnim ???``
 - ``Worm.Talk ???``
 - ``Worm.Tash ???``
 - ``Worm.TimeToDie ???``
 - ``Worm.Track.Projectile ???``
 - ``Worm.UpdateGraphicalOrientation ???``
 - ``Worm.UtilityUsed ???``
 - ``Worm.VectorIntersect ???``
 - ``Worm.Walk ???``
 - ``Worm.Walk.Speed = 0.00``
 - ``Worm.WalkAnimSpeedScale = 30.00``
 - ``Worm.WalkOffCliffVelMulti = 0.70``
 - ``Worm.WaterDeath ???``
 - ``Worm.Wriggle ???``



 - ``Ninja.DetachVelocityMulti = 1.00``
 - ``Ninja.GravityMultiplier = 1.00``
 - ``Ninja.LengthenShortenRate = 0.20``
 - ``Ninja.MaxLength = 450.00``
 - ``Ninja.MaxUnreducedSwingLength = 40.00``
 - ``Ninja.MinBendDistFromWorm = 30.00``
 - ``Ninja.MinLength = 30.00``
 - ``Ninja.NumRaycastRefinements = 8``
 - ``Ninja.NumShots = 5``
 - ``Ninja.RotationDamping = 0.99``
 - ``Ninja.SnapOffAngleRadians = 7.50``
 - ``Ninja.SwingAmount = 0.00``
 - ``Ninja.WormMass = 100.00``



 - ``Jetpack.AnimSmoothFB = 0.15``
 - ``Jetpack.AnimSmoothLR = 0.04``
 - ``Jetpack.FwdThrustRotation = 0.20``
 - ``Jetpack.InitFuel = 7500``
 - ``Jetpack.MaxAltitude = 1500.00``
 - ``Jetpack.MaxGfxXOrient = 1.05``
 - ``Jetpack.OverCeilingThrustMod = 0.05``
 - ``Jetpack.SuperThrustAccel = 0.30``
 - ``Jetpack.SuperThrustMax = 1.00``
 - ``Jetpack.SuperThrustMod = 0.20``
 - ``Jetpack.SuperThrustReduct = 0.97``
 - ``Jetpack.SuperThrustShutOff = 0.01``
 - ``Jetpack.ThrustScale = 0.00``
 - ``Jetpack.TurnRotationSpeed = 0.02``
 - ``Jetpack.XZWindResNoThrust = 0.98``
 - ``Jetpack.XZWindResThrust = 1.00``



 - ``Crate.ImpulseMagnitude = 0.50``
 - ``Crate.ImpulseRadius = 65.00``
 - ``Crate.LandDamageRadius = 45.00``
 - ``Crate.WormDamageMagnitude = 30.00``
 - ``Crate.WormDamageRadius = 45.00``



 - ``Crate.CanDropFromChute = 1``
 - ``Crate.Contents = kWeaponSuperSheep``
 - ``Crate.DelayMillisec = 0``
 - ``Crate.DisableMessage = 0``
 - ``Crate.ExplicitSpawnPos = <x:0.00 y:0.00 z:0.00>``
 - ``Crate.FallSpeed = 0.00``
 - ``Crate.Gravity = 1``
 - ``Crate.GroundSnap = 0``
 - ``Crate.HealthInCrates = 25``
 - ``Crate.Hitpoints = 25``
 - ``Crate.HitpointsMultiplier = 0.50``
 - ``Crate.Index = 65535``
 - ``Crate.LifetimeSec = -1.00``
 - ``Crate.LifetimeTurns = 65535``
 - ``Crate.NumContents = 1``
 - ``Crate.Parachute = 1``
 - ``Crate.Pushable = 1``
 - ``Crate.RandomSpawnPos = 0``
 - ``Crate.Scale = 1.00``
 - ``Crate.Showered = 0``
 - ``Crate.ShowerSpawnPos = <x:0.00 y:500.00 z:0.00>``
 - ``Crate.Spawn = CrateSpawn``
 - ``Crate.TeamCollectable = 65535``
 - ``Crate.TeamDestructible = 65535``
 - ``Crate.TrackCam = 1``
 - ``Crate.Type = Weapon``
 - ``Crate.UXB = 0``
 - ``Crate.WaitTillLanded = 1``



 - ``String.DestDataName = ``
 - ``String.ReplaceString = ``
 - ``String.SearchString = ``
 - ``String.SourceDataName = ``



 - ``Game.BriefingText = ``
 - ``Game.DefaultLevel = FE.Level.Multiapplecore``
 - ``Game.Health = 100``
 - ``Game.MaxTeams = 4``
 - ``Game.NextLevel = FE.Level.Multiapplecore``
 - ``Game.NumPlayers = 0``
 - ``Game.NumTeams = 0``
 - ``Game.RoundTime = 900``
 - ``Game.SelectWorm = 0``
 - ``Game.Start.Text = ``
 - ``Game.TeleportIn = 0``
 - ``Game.TurnTime = 60000``
 - ``Game.Username = ``
 - ``Game.HostIsLocal = 0``
 - ``Game.hostname = ``
 - ``Game.League = ``
 - ``Game.MaxPlayers = 4``
 - ``Game.Scope = 0``
 - ``Game.TimeStamp = 0``
 - ``Game.Victories = 0``
 - ``Game.winner = ``



 - ``GameLogic.AboutToApplyDamage = Message ``
 - ``GameLogic.ActivateNextWorm = Message ``
 - ``GameLogic.ActivateSuddenDeath ``
 - ``GameLogic.ActiveWormChanged ``
 - ``GameLogic.AddInventory ``
 - ``GameLogic.AddInventory.Arg0 = ``
 - ``GameLogic.AddInventory.Arg1 = ``
 - ``GameLogic.AddMeToDeathQueue ``
 - ``GameLogic.AITurn.Started ``
 - ``GameLogic.AllTeamsHadTurn = 0``
 - ``GameLogic.ApplyDamage = Message ``
 - ``GameLogic.ArtilleryMode = 0``
 - ``GameLogic.Challenge.Failure ``
 - ``GameLogic.Challenge.Result ``
 - ``GameLogic.ChangeWind ``
 - ``GameLogic.ClearInventories = Message ``
 - ``GameLogic.CrateShower = Message ``
 - ``GameLogic.CreateAirstrike ``
 - ``GameLogic.CreateChicken ``
 - ``GameLogic.CreateCrate = Message ``
 - ``GameLogic.CreateNuclearEffect ``
 - ``GameLogic.CreateRandomMine ``
 - ``GameLogic.CreateRandomOildrum ``
 - ``GameLogic.CreateTrigger = Message ``
 - ``GameLogic.CurrentScript = stdvs``
 - ``GameLogic.DecrementInventory ``
 - ``GameLogic.DecrementInventory.Id ``
 - ``GameLogic.DecrementWeaponDelays ``
 - ``GameLogic.DestroyTrigger ``
 - ``GameLogic.DoubleTurnTime ``
 - ``GameLogic.Draw = Message``
 - ``GameLogic.DrawImmediately ``
 - ``GameLogic.DropRandomCrate ``
 - ``GameLogic.EndTurn ``
 - ``GameLogic.EndTurn.Immediate ``
 - ``GameLogic.EnemyDamage ``
 - ``GameLogic.ForceSuddenDeath ``
 - ``GameLogic.FriendlyDamage ``
 - ``GameLogic.GameLoadComplete ``
 - ``GameLogic.GetAllTeamsHadTurn ``
 - ``GameLogic.GotoFrontEnd ``
 - ``GameLogic.GunWaiting ``
 - ``GameLogic.IncrementInventory ``
 - ``GameLogic.LoadPowerWeaponTweaks ``
 - ``GameLogic.Mission.Failure = Message ``
 - ``GameLogic.Mission.Success = Message ``
 - ``GameLogic.NoActivity ``
 - ``GameLogic.PauseGame = Message ``
 - ``GameLogic.PlaceMine ``
 - ``GameLogic.PlaceObjects = Message ``
 - ``GameLogic.QuitGame ``
 - ``GameLogic.RequestWeaponIndex ``
 - ``GameLogic.RequestWeaponName ``
 - ``GameLogic.ResetChickenParams ``
 - ``GameLogic.ResetCrateParameters ``
 - ``GameLogic.ResetTriggerParams ``
 - ``GameLogic.ResumeGame = Message ``
 - ``GameLogic.RoundTime.Pause = Message ``
 - ``GameLogic.RoundTime.Resume = Message ``
 - ``GameLogic.SetGameScope ``
 - ``GameLogic.StartGame ``
 - ``GameLogic.SuddenDamageMode = 0``
 - ``GameLogic.Timeout ``
 - ``GameLogic.Timer0 ``
 - ``GameLogic.Timer1 ``
 - ``GameLogic.Timer2 ``
 - ``GameLogic.Timer3 ``
 - ``GameLogic.Timer4 ``
 - ``GameLogic.Timer5 ``
 - ``GameLogic.Timer6 ``
 - ``GameLogic.Timer7 ``
 - ``GameLogic.Timer8 ``
 - ``GameLogic.Timer9 ``
 - ``GameLogic.TriggerGoodShot ``
 - ``GameLogic.Turn.Ended = Message ``
 - ``GameLogic.Turn.Started = Message ``
 - ``GameLogic.Turn.WormUpdated ``
 - ``GameLogic.TurnTime.Pause = Message ``
 - ``GameLogic.TurnTime.Resume = Message ``
 - ``GameLogic.Tutorial.End ``
 - ``GameLogic.Tutorial.Failure ``
 - ``GameLogic.Tutorial.Success ``
 - ``GameLogic.WeaponIndex = 0``
 - ``GameLogic.WeaponName = ``
 - ``GameLogic.Win = IntMessage ``



 - ``Land.File = flattest.xom``
 - ``Land.LightGradientResource = LightGradient_E_DAY``
 - ``Land.Materials = ThemeEngland\ThemeEngland.txt``
 - ``Land.SkyBoxResource = ENGLAND.DAYSky``
 - ``Land.Theme = ENGLAND``
 - ``Land.TimeOfDay = DAY``
 - ``Land.WaterDetailResource = ``
 - ``Land.WaterOffsetResource = ENGLAND.DAYWaterOffs``
 - ``Land.WaterResource = ENGLAND.DAYWater``
 - ``Land.OverrideScale = 20.00``
 - ``Land.Center = <x:0.00 y:0.00 z:0.00>``
 - ``Land.Changed = 0``
 - ``Land.DebugCount1 = 0``
 - ``Land.DebugCount2 = 0``
 - ``Land.Finished = 0``
 - ``Land.FringeLength = 0.40``
 - ``Land.HighResCollision = 0``
 - ``Land.IgnoreInitialCollision = 0``
 - ``Land.ImpactNorm = <x:0.00 y:0.00 z:0.00>``
 - ``Land.ImpactPos = <x:0.00 y:0.00 z:0.00>``
 - ``Land.ImpactTime = 0``
 - ``Land.ImpactTimeScale = 0.00``
 - ``Land.Indestructable = 0``
 - ``Land.InitialMaxHeight = 0.00``
 - ``Land.LocalImpactPos = <x:0.00 y:0.00 z:0.00>``
 - ``Land.MaxBounds = <x:0.00 y:0.00 z:0.00>``
 - ``Land.MaxHeight = 1.00``
 - ``Land.MaxImpactTime = 0``
 - ``Land.MaxParabolaRange = 0.00``
 - ``Land.MinBounds = <x:0.00 y:0.00 z:0.00>``
 - ``Land.Parabola_A = <x:0.00 y:0.00 z:0.00>``
 - ``Land.Parabola_I = <x:0.00 y:0.00 z:0.00>``
 - ``Land.Parabola_V = <x:0.00 y:0.00 z:0.00>``
 - ``Land.ParabolaPlaneNormal = <x:0.00 y:0.00 z:0.00>``
 - ``Land.Permanent = 0``
 - ``Land.ProjectileRadius = 0.00``
 - ``Land.Radius = 1.00``
 - ``Land.UniqueID = 0``
 - ``Land.VoxelContents = 0``
 - ``Land.VoxelImpactPos = 0``



 - ``Water.ExpiryDepth = -200.00``
 - ``Water.Level = 0.00``
 - ``Water.SwapBlends = 0``
 - ``Water.TweakName = Water.ENGLAND.DAY``
 - ``Water.CentreColour = <r:160 g:160 b:160 a:240>``
 - ``Water.InnerColour = <r:160 g:160 b:160 a:210>``
 - ``Water.MiddleColour = <r:160 g:160 b:160 a:128>``
 - ``Water.OuterColour = <r:160 g:160 b:160 a:100>``
 - ``Water.RimColour = <r:160 g:160 b:160 a:0>``
 - ``Water.RiseAmount = 25.00``
 - ``Water.Density = 5.83``
 - ``Water.Direction = 0.00``
 - ``Water.RiseSpeed.Current = 0``
 - ``Water.RiseSpeed.Fast = 16``
 - ``Water.RiseSpeed.Graphic = 0.10``
 - ``Water.RiseSpeed.Medium = 8``
 - ``Water.RiseSpeed.Slow = 4``
 - ``Water.Speed = 1.00``

### Основные скрипт-функции языка Worms Lua

 - ``SendMessage("Earthquake.End")`` - вызов конца землетрясения
 - ``worm = QueryWormContainer()`` - опрос текущего червя, переменная worm принемает параметры червя
 - ``WormContainerName = GetWormContainerName(WormIndex)`` - Получение параметров червя по индексу. Функция ставит вместо индекса слово из массива worm01..worm16
 - ``Weapon_Fired_End()`` - функция вызовающая когда выстрел совершен, оружие использовалось.

По умолчанию ее вид:

``` lua
function Weapon_Fired_Start()
  SendMessage("Timer.StartRetreatTimer")
  SendMessage("Weapon.Delete")
end
```

 - ``TurnEnded()`` - функция вызывающая при конце хода.

По умолчанию ее вид:

``` lua
function TurnEnded()
  CheckOneTeamVictory()
end
```

 - ``EditContainer(ContainerName)`` - функция получения данных контейнера для изменения. Чтение и запись.

Пример использования:

``` lua
local lock, worm = EditContainer(WormContainerName)
-- изменяем переменную	worm.Energy = worm.Energy - 10
CloseContainer(lock)
```

 - ``QueryContainer(StringName)`` - функция опроса данных, только чтение.
 - ``SendMessage("CommentaryPanel.ScriptText")`` - вывод сообщения на экран.

Пример:

``` lua
SetData("Text.TestComment", "Hello World!!!")
SetData("CommentaryPanel.Comment", "Text.TestComment")
SendMessage("CommentaryPanel.ScriptText")
```

 - ``Weapon_Created()`` - функция вызывающая при выборе оружия.

Пример использования:

``` lua
function Weapon_Created()
  worm = QueryWormContainer()
  if worm.WeaponIndex == "WeaponNuclearBomb" then
  -- делаем необходимое
  end
end
```

 - ``SendMessage("GameLogic.CreateRandomMine")`` - создание случайной мины.
 - ``DoWormpotOncePerTurnFunctions()`` - функция вызывающая перед ходом для вормподных действий
 - ``SendMessage("RandomNumber.Get")`` - получение случайного числа.
 - ``RandNumb = GetData("RandomNumber.Uint")`` - Взятие целого полученого случайного числа.
 - ``StartTimer("FuncName", 1000)`` - создание таймера на 1 секунду после чего вызывается функция FuncName.
 - ``SendMessage("GameLogic.CrateShower")`` - вызов кучи ящиков с неба.
 - ``SendMessage("Earthquake.Start")`` - вызов землетрясения.
 - ``SetData("DoubleDamage", 1)`` - вызов Двойного Урона
 - ``SendMessage("GameLogic.CreateCrate")`` - создание Ящика.

Пример создания ящика:

``` lua
SetData("Crate.Type", "Weapon") -- вид оружия
-- SetData("Crate.GroundSnap", 1) -- создается на земле
SetData("Crate.Contents", "kWeaponBazooka")-- что лежит
SetData("Crate.Parachute", 0) -- без парашута
SetData("Crate.LifetimeSec", -1) -- время жизни
SendMessage("GameLogic.CreateCrate")
```

 - ``SendIntMessage("Worm.QueueAnim", iWormIndex)`` - вызов анимации червя с индексом iWormIndex.

Пример:

``` lua
SetData("Worm.ScriptAnim", "AimBazooka")
SendIntMessage("Worm.QueueAnim", iWormIndex)
```

 - ``worm.Position`` - чтение векторных данных:

Пример:

``` lua
SetData("Text.TestComment", worm.Position)   --вместо worm.Position можно любой вектор
SubString("Text.TestComment", "Text.TestComment" , "(" , "Vector = {" )
SubString("Text.TestComment", "Text.TestComment" , ")" , "}" )
local TextTable = GetData("Text.TestComment")
assert(loadstring(TextTable))()

--Vector имеет поля x,y,z
SetData("Text.TestComment", "Position Y = "..Vector.y)
SetData("CommentaryPanel.Comment", "Text.TestComment")
SendMessage("CommentaryPanel.ScriptText")
```

## Worms 4: Mayhem

Тут будет приводиться список функций для изменения lua в Worms 4: Mayhem

 - ``function EFMV_Terminated()`` - переопределяемая функция. Вызывается после того, как проигран мультик, вызванный к примеру таким способом:

``` lua
SetData("EFMV.Unskipable", 0)
SetData("EFMV.MovieName", "EFMV.Intro")
SendMessage("EFMV.Play")
```

 - ``function Initialise()`` - переопределяемая функция. Вызывается в самом начале игры, тут проводится инициализация, расстановка червей, создание команд и т.п.. Например:

``` lua
function Initialise()
  kVariables()                    -- Lets have a butchers at the variables we're gonna use before the deathmatch begins.    
  kDialogueBoxTimeDelay = 0500    -- This is the delay between selecting a weapon and having the game display a dialogue box.
  SendMessage("Commentary.NoDefault")
  SetData("Mine.DetonationType", 1)
  SetData("Mine.DudProbability", 0) SetData("Mine.MinFuse", 5000) SetData("Mine.MaxFuse", 5000)
  SendMessage("GameLogic.PlaceObjects")
  SetData("HotSeatTime", 0) SetData("RoundTime", -1) SetData("TurnTime", 90000)
  lib_SetupTeam(0, "Team_Human")
  lib_SetupTeam(1, "Team_Enemy")
  lib_SetupTeam(2, "Team_EFMV")  
  lib_SetupWorm(0, "Worm.Game1.Human0")
  lib_SetupWorm(1, "Worm.Game1.Human1")
  lib_SetupWorm(2, "Worm.Game1.Human2")
  lib_SetupWorm(3, "Worm.Game1.Human3")
  lib_SetupTeamInventory(0, "Inventory_Human")
  lib_SetupTeamInventory(1, "Inventory_Enemy")
  SendMessage("WormManager.Reinitialise")
  SetData("Trigger.Visibility",0)
  SetData("Land.Indestructable",1)
  SetData("Wind.Speed", 0)
  lock, scheme = EditContainer("GM.SchemeData")
  scheme.HelpPanelDelay = 0          
  CloseContainer(lock)
  SetData("EFMV.Unskipable", 0)
  -- Kick the game off by running the first little EFMV snippet.
  kPlayEFMV("EFMV.Intro")
end
```

 - ``function TurnStarted()`` - переопределяемая функция. Вызывается в начале хода, тут обычно проводится индивидуальная настройка хода

---

Статья еще не закончена, вы можете помогать ее развитию добавляя свой материал и редактирую наши ошибки
