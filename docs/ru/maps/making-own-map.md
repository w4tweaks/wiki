# Создание своей карты

## Инструменты

Для создания своей карты необходимо располагать следующими инструментами:

- [**W3DEditor v.2.4**](http://yadi.sk/d/k0AIufux5697c) - Редактор карт.
- **W3DLauncher v.4.4** - Загрузчик для загрузки списка карт и модов (входит в состав [W3DTweaksPack](http://yadi.sk/d/W1Fnc1Kq3neB4)).
- [**W3DIconEdit v.1.0**](http://yadi.sk/d/QJHFHCVn6SffA) - Создание иконок к картам.
- [**ScriptsEdit v.1.4**](http://yadi.sk/d/QJHFHCVn6SffA) - Создание Списка карт и добавление в Список.

## Процесс создания

- В Редакторе карт создается карта **(\*.xom)**, Текстурный лист **(\*.txt)**. Эти файлы кладутся в папку **Data\Maps\\**.
- После этого карта дабавляется в игру, создается Список карт, там прописывается имя карты, ее текстурный лист, ее иконка, файл карты, окружение и т.п. Выходной файл **Scripts\*.xom** сохраняем в папку **Data\MapPacks\\**.
- Запускается игра через Загрузчик выбирается user редим, и Список карт.
- Затем запускается игра, делается скриншот. Открываем Создание иконок и вставляем скриншот в буфер, подгоняем размер и сохраняем иконку **\*.tga** в **Data\Frontend\Levels\\**.
- Возвращаемся к игре и наблюдаем нашу карту с иконкой.

## Вики руководство

- [Руководство №1](tutorial-1.md) - Пример создания простейших объектов в редакторе.
- ==[Руководство №2](https://w4tweaks.gitlab.io/wiki-en/maps/tutorial-2/)== - Пример работы 3DImport'а и совместно с 3DS Max. Для создания сложных объектов.

## Видео материалы

Наиболее понятным обучение создания своих карт являются видео примеры:

- ~~[Video Tutorial as Make Map in W3DEditor v1.0](http://worms3d-portal.com/portal.php?topic_id=523)~~ - Первый пример, импорт объектов, создание ландшафта, сохранение
- ~~[Video Tutorial 2 as Add Map](http://worms3d-portal.com/portal.php?topic_id=526)~~ - Добавление карты в игру через ScriptsEdit.
- ~~[Video Tutorial Change Textures](http://worms3d-portal.com/portal.php?topic_id=558)~~ - Изменение текстур и текстурного листа.
- ~~[Video W3DEditor v1.8 Preview Copy Modes](http://worms3d-portal.com/portal.php?topic_id=770)~~ - Копирование мышью
- ~~[Video W3DEditor v2.0 Preview](http://worms3d-portal.com/portal.php?topic_id=776)~~ - Изменение слоев и показ возможностей редактора.
- [W3DEditor Tutorial 1: Poxels](http://www.youtube.com/watch?v=_bEg7XpU0Xk) - W3DEditor Tutorial 1: Poxels
- [W3DEditor Tutorial 2: Objects](http://www.youtube.com/watch?v=ltfpG4kC16I) - W3DEditor Tutorial 2: Objects
- [W3DEditor Tutorial 3: Textures](http://www.youtube.com/watch?v=ob7xtUCdoGU) - W3DEditor Tutorial 3: Textures
- [W3DEditor Tutorial 4: Script](http://www.youtube.com/watch?v=nk_7u5poc04) - W3DEditor Tutorial 4: Script
- [W3DEditor Tutorial 5: Playing](http://www.youtube.com/watch?v=0ROKlGX4QQw) - W3DEditor Tutorial 5: Playing
- [W3DEditor Tutorial 6: Emitters](http://www.youtube.com/watch?v=tpnSn4IH4-0) - W3DEditor Tutorial 6: Emitters
