# Поксель

**Поксель** - это главный объект карт Worms 3D/W4. Это "кирпичик" из которого строются все объекты.

Все активные элементы карты это поксели.

**Структура**. Поксельный объект обладает такими параметрами как позиция, положение, размер.

Поксель состоит из [вокселей](voxel.md).

Количество вокселей может быть 30x25x30.

![Poxel_1.gif](../../assets/maps/Poxel_1.gif)

По оси Y поксель имеет [Слои](layers.md) которые можно изменять, придавая нужную форму относительно осей X и Z.

![Poxel_2.gif](../../assets/maps/Poxel_2.gif)

[Высотная карта](height-map.md) используется для придания плоскости покселя нужную форму, относительно оси Y.

![Poxel_3.gif](../../assets/maps/Poxel_3.gif)

Каждый [воксель](voxel.md) покселя имеет свой текстурный код. При нулевом коде, [воксель](voxel.md) не отображается.

![Poxel_64.gif](../../assets/maps/Textures_64.gif)

Управлять текстурами можно с помощью текстурных координат, которые натягивают текстуру нужным образом.

![Poxel_4.gif](../../assets/maps/Poxel_4.gif)

Каждый поксель имеет еще свойство поверхности, а именно сглаживание боков и верха.

![Poxel_5.gif](../../assets/maps/Poxel_5.gif)
