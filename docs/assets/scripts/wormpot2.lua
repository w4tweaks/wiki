-------------------------------------------------------------------------------
-- Script to apply the wormpot modes AlexBond Decomp
-- Lua 5.0.1
-------------------------------------------------------------------------------

function EndEarthquake()
  SendMessage("Earthquake.End")
end

--function SetupWorm(Name, Speech)

--   local scheme = QueryContainer("GM.SchemeData")
--   local lock, worm

--   lock, worm = EditContainer(GetWormContainerName(WormIndex))
--   worm.Active = true
--   worm.IsHatWearer = true
--   worm.Name = Name

--   worm.TeamIndex = TeamIndex
--   worm.SfxBankName = Speech
--   worm.Energy = scheme.WormHealth
--   worm.WeaponFuse = 3
--   worm.WeaponIsBounceMax = false
--   worm.Spawn = "spawn"
--   CloseContainer(lock)

--   WormIndex = WormIndex +1

--end

function GetDrakulaWorms()
  if DrakulaMode == true then
    worm = QueryWormContainer()
    DrakulaTeam = worm.TeamIndex
    DrakulaName = worm.Name
    local WormIndex = 0

    local WormContainerName = GetWormContainerName(WormIndex)
    while WormContainerName ~= "" do
      local lock, worm = EditContainer(WormContainerName)
      DrakulaEnergyWorm[WormIndex] = worm.Energy
      CloseContainer(lock)
      WormIndex = WormIndex + 1
      WormContainerName = GetWormContainerName(WormIndex)
    end
    DrakulaActived = true
  end
end

--function Weapon_Fired_Start()
--   GetDrakulaWorms()
--   SendMessage("Timer.StartRetreatTimer")
--end

function Weapon_Fired_End()
  GetDrakulaWorms()
  SendMessage("Timer.StartRetreatTimer")
  SendMessage("Weapon.Delete")
end

function TurnEnded()
  SetDrakulaWorms()
  CheckOneTeamVictory()
end

function SetDrakulaWorms()

  if (DrakulaMode == true) and (DrakulaActived == true) then
    local WormIndex = 0
    local DrakulaWorm = 0
    local DrakulaIndex = 0
    local WormContainerName = GetWormContainerName(WormIndex)

    while WormContainerName ~= "" do
      local lock, worm = EditContainer(WormContainerName)

      if (worm.TeamIndex ~= DrakulaTeam) and (worm.Energy ~= DrakulaEnergyWorm[WormIndex]) then
        DrakulaWorm = DrakulaWorm + DrakulaEnergyWorm[WormIndex] - worm.Energy
        --			worm.TeamIndex = DrakulaTeam
      end
      if (DrakulaName == worm.Name) then
        DrakulaIndex = WormIndex
      end
      CloseContainer(lock)
      WormIndex = WormIndex + 1
      WormContainerName = GetWormContainerName(WormIndex)

    end

    local Wormpot = QueryContainer("WormPot")
    if Wormpot.DoubleDamage ~= true then
      DrakulaWorm = math.floor(DrakulaWorm*3/4)
    end


    if DrakulaWorm > 0 then
      WormIndex = DrakulaIndex
      WormContainerName = GetWormContainerName(WormIndex)

      local lock, worm = EditContainer(WormContainerName)
      worm.Energy = worm.Energy+DrakulaWorm
      CloseContainer(lock)

      -- Draw Anim
      SetData("Text.TestComment", DrakulaName.." +"..DrakulaWorm.." Health")
      SetData("CommentaryPanel.Comment", "Text.TestComment")

      SendMessage("CommentaryPanel.ScriptText")
    end
    DrakulaActived = false
  end
end


------------------------------
function Weapon_Created()
  worm = QueryWormContainer()

  --   if worm.WeaponIndex == "WeaponNuclearBomb" then
    --   end

    if worm.WeaponIndex == "UtilityNinjaRope" then
      if ZoomRope == true then


        if ZoomRopeOn == true then
          SetData("FE.Wormapedia.NapalmTitle", "Rope Zoom ON")


          local ContainerLock, Container = EditContainer("NinjaCamera")
          Container.DistFromObject = 600
          CloseContainer(ContainerLock)

          ZoomRopeOn = false

        else

          SetData("FE.Wormapedia.NapalmTitle", "Rope Zoom OFF")

          local ContainerLock, Container = EditContainer("NinjaCamera")

          Container.DistFromObject = tempRopeZoom
          CloseContainer(ContainerLock)

          ZoomRopeOn = true
        end

      end

    end
    --- ---
    if worm.WeaponIndex == "WeaponGasCanister" then

      -- Smoke Bomb --
      if SmokeBomb == true then
        if Smokenum == 0 then
          local ContainerLock, Container = EditContainer("kWeaponGasCanister")

          Container.DisplayName = SmokeTempName
          Container.DetonationFx = SmokeTempFx
          Container.ArielFx = SmokeTempAFx
          CloseContainer(ContainerLock)
          Smokenum = Smokenum + 1

        elseif Smokenum == 3 then
          SetData("Text.TestComment", "Smoke Bomb")

          local ContainerLock, Container = EditContainer("kWeaponGasCanister")
          Container.DisplayName = "Text.TestComment"

          Container.DetonationFx = "Part_OilySmoke"
          Container.ArielFx = ""
          CloseContainer(ContainerLock)
          Smokenum = -1
        end

        Smokenum = Smokenum + 1
      end
    end


    if worm.WeaponIndex == "WeaponProd" then

      --	worm.Position="(x = 1.000000, y = 600.000000, z = 1.000000)"

      -- Flood --
      -- 	SetData("Text.TestComment",worm.Position)
      --	SubString("Text.TestComment","Text.TestComment","(","WVector = {")
      --	SubString("Text.TestComment","Text.TestComment",")","}")
      --	local TextTable = GetData("Text.TestComment")
      --	assert(loadstring(TextTable))()
      --	SetData("Water.Level", WVector.y-20)

      --

      -- Mega Prod
      if MegaProd == true then
        if MegaProdnum == 0 then
          local ContainerLock, Container = EditContainer("kWeaponProd")

          Container.DisplayName = "Text.kWeaponProd"
          Container.ImpulseMagnitude = tempMPMagnitude
          Container.Radius = tempMPRadius
          --		Container.Scale = 1.0
          CloseContainer(ContainerLock)

          MegaProdnum = MegaProdnum + 1

        elseif MegaProdnum == 10 then
          SetData("Text.TestComment", "Mega Prod")

          local ContainerLock, Container = EditContainer("kWeaponProd")
          Container.DisplayName = "Text.TestComment"

          Container.ImpulseMagnitude = 0.3
          Container.Radius = 1000
          --		Container.Scale = 2.0
          CloseContainer(ContainerLock)

          -- Draw Anim
          MegaProdnum = -1
        end
        MegaProdnum = MegaProdnum + 1
      end
    end


    if worm.WeaponIndex == "UtilitySkipGo" then
      -- SpeedWorm
      if SpeedWalk == true then
        if SpeedWalknum == 0 then
          --	local ContainerLock, Container = EditContainer("kUtilitySkipGo")

          --	Container.DisplayName = "Text.kUtilitySkipGo"
          --	CloseContainer(ContainerLock)
          SetData("Worm.Walk.Speed", TempWalkSpeed)
          SpeedWalknum = SpeedWalknum + 1

        elseif SpeedWalknum == 10 then

          SetData("Text.TestComment", "Super Speed Worm ON")

          local ContainerLock, Container = EditContainer("kUtilityQuickWalk")
          Container.DisplayName = "Text.TestComment"

          CloseContainer(ContainerLock)

          SetData("Worm.Walk.Speed", 0.05)
          SpeedWalknum = -1

          ActiveWormIndex = GetData("ActiveWormIndex")

          local lock, wormt = EditContainer(GetWormContainerName(ActiveWormIndex))
          wormt.WeaponIndex = "UtilityQuickWalk"

          wormt.LogicAnimState = 10
          CloseContainer(lock)

          SendIntMessage("Worm.ScriptDrawAnim", ActiveWormIndex)
          SendMessage("Weapon.CreateGraphic")


        end
        SpeedWalknum = SpeedWalknum + 1
      end
    end


    --- ---
    if (RepeatingStrike1 == true) or (RepeatingStrike2 == true) then

      if  worm.WeaponIndex == "WeaponClusterGrenade" then
        RepStrikeNum = 1
      elseif worm.WeaponIndex == "WeaponHolyHandGrenade" then
        RepStrikeNum = 2
      elseif worm.WeaponIndex == "WeaponBananaBomb" then
        RepStrikeNum = 3
      elseif worm.WeaponIndex == "WeaponLandmine" then
        RepStrikeNum = 4
      elseif worm.WeaponIndex == "WeaponDynamite" then
        RepStrikeNum = 5
      elseif worm.WeaponIndex == "WeaponSheep" then
        RepStrikeNum = 6
      elseif worm.WeaponIndex == "WeaponPetrolBomb" then
        RepStrikeNum = 7

        -- elseif worm.WeaponIndex == "WeaponGasCanister" then
        end

      end
      --- ---

      if  worm.WeaponIndex == "WeaponAirstrike" then

        --- --------
        if RepeatingStrike1 == true then
          --- Napalm Stike + Canonball Strike + Super Baseball Stike
          if RepStrikeNum == 1 then

            CopyContainer("kWeaponGasCanister","kWeaponAirstrike")
            -- Napalm Stike
            SetData("Text.kWeaponMingVase", "Napalm Stike")

            SetData("Bomber.NumBombs", 1)
            local ContainerLock, Container = EditContainer("kWeaponAirstrike")

            Container.DisplayName = "Text.kWeaponMingVase"
            Container.NumBomblets = 7
            Container.PayloadGraphicsResourceID = "RedBull"

            Container.BombletWeaponName = "kWeaponClusterBomb"
            Container.DetonatesOnExpiry = true
            Container.LifeTime = 0

            CloseContainer(ContainerLock)
            RepStrikeNum = -1
            ---
          elseif RepStrikeNum == 2 then
            -- Canonball Strike

            SetData("Text.kWeaponMingVase", "Canonball Strike")
            CopyContainer("kWeaponHolyHandGrenade","kWeaponAirstrike")
            SetData("Bomber.NumBombs", 6)

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kWeaponMingVase"

            Container.WeaponGraphicsResourceID = "Radio"
            Container.IsTargetingWeapon = true
            Container.IsBomberWeapon = true

            Container.IsAimedWeapon = false
            Container.HasAdjustableHerd = false
            Container.HasAdjustableFuse = false

            Container.LifeTime = -1
            Container.AnimDraw = "DrawAirstrike"
            Container.AnimFire = "FireAirstrike"

            CloseContainer(ContainerLock)
            RepStrikeNum = -1
            ---
          elseif RepStrikeNum == 3 then
            -- Super Baseball Stike

            SetData("Bomber.NumBombs", 5)
            SetData("Text.kWeaponMingVase", "Super Baseball Stike")
            CopyContainer("kWeaponBananaBomb","kWeaponAirstrike")

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kWeaponMingVase"

            Container.WeaponGraphicsResourceID = "Radio"
            Container.IsTargetingWeapon = true
            Container.IsBomberWeapon = true

            Container.IsAimedWeapon = false
            Container.HasAdjustableHerd = false
            Container.HasAdjustableFuse = false

            Container.AnimDraw = "DrawAirstrike"
            Container.AnimFire = "FireAirstrike"
            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == 4 then
            -- MineStrike

            SetData("Text.kWeaponMingVase", "Mine Strike")
            CopyContainer("kWeaponLandmine","kWeaponAirstrike")
            SetData("Bomber.NumBombs", 5)

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kWeaponMingVase"

            Container.WeaponGraphicsResourceID = "Radio"
            Container.IsTargetingWeapon = true
            Container.IsBomberWeapon = true

            Container.AnimDraw = "DrawAirstrike"
            Container.AnimFire = "FireAirstrike"
            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == 5 then
            -- Dynamite Strike

            SetData("Text.kWeaponMingVase", "Dynamite Strike")
            CopyContainer("kWeaponDynamite","kWeaponAirstrike")
            SetData("Bomber.NumBombs", 5)

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kWeaponMingVase"

            Container.WeaponGraphicsResourceID = "Radio"
            Container.IsTargetingWeapon = true
            Container.IsBomberWeapon = true

            Container.IsAimedWeapon = false
            Container.HasAdjustableHerd = false
            Container.HasAdjustableFuse = false

            Container.LifeTime = 8000
            Container.AnimDraw = "DrawAirstrike"
            Container.AnimFire = "FireAirstrike"

            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == 6 then
            -- Sheep Strike

            SetData("Text.kWeaponMingVase", "Sheep Strike")
            CopyContainer("kWeaponGasCanister","kWeaponAirstrike")
            SetData("Bomber.NumBombs", 5)

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kWeaponMingVase"

            Container.PayloadGraphicsResourceID = "Sheep"
            Container.LaunchSfx = "Sheep.Baa"
            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == 7 then
            -- Petrol Strike

            SetData("Text.kWeaponMingVase", "Petrol Strike")
            CopyContainer("kWeaponPetrolBomb","kWeaponAirstrike")
            SetData("Bomber.NumBombs", 5)

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kWeaponMingVase"

            Container.WeaponGraphicsResourceID = "Radio"
            Container.IsTargetingWeapon = true
            Container.IsBomberWeapon = true

            Container.IsAimedWeapon = false
            Container.HasAdjustableHerd = false
            Container.HasAdjustableFuse = false

            Container.AnimDraw = "DrawAirstrike"
            Container.AnimFire = "FireAirstrike"
            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == -1 then
            SetData("Bomber.NumBombs", 5)

            CopyContainer("kWeaponGasCanister","kWeaponAirstrike")
            RepStrikeNum = 0
          end


        end

        --- --------
        --- --------
        if RepeatingStrike2 == true then

          --- MineStrike + Holy Hand Grenade Strike + Dynamite Strike

          if RepStrikeNum == 1 then
            -- Cluster Bomb Strike
            SetData("Bomber.NumBombs", 5)

            SetData("Text.kUtilityLaserSight", "Cluster Strike")
            CopyContainer("kWeaponClusterGrenade","kWeaponAirstrike")
            local ContainerLock, Container = EditContainer("kWeaponAirstrike")

            Container.DisplayName = "Text.kUtilityLaserSight"
            Container.NumBomblets = 3
            Container.LifeTime = 2000

            Container.WeaponGraphicsResourceID = "Radio"
            Container.IsTargetingWeapon = true
            Container.IsBomberWeapon = true

            Container.IsAimedWeapon = false
            Container.HasAdjustableHerd = false
            Container.HasAdjustableFuse = false

            Container.AnimDraw = "DrawAirstrike"
            Container.AnimFire = "FireAirstrike"
            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == 2 then

            -- Holy Hand Grenade Strike
            SetData("Text.kUtilityLaserSight", "Holy Strike")
            CopyContainer("kWeaponHolyHandGrenade","kWeaponAirstrike")

            SetData("Bomber.NumBombs", 6)
            local ContainerLock, Container = EditContainer("kWeaponAirstrike")

            Container.DisplayName = "Text.kUtilityLaserSight"
            Container.WeaponGraphicsResourceID = "Radio"
            Container.IsTargetingWeapon = true

            Container.IsBomberWeapon = true
            Container.IsAimedWeapon = false
            Container.HasAdjustableHerd = false

            Container.HasAdjustableFuse = false
            Container.LifeTime = -1
            Container.AnimDraw = "DrawAirstrike"

            Container.AnimFire = "FireAirstrike"
            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == 3 then
            -- Super Banana Bomb Strike

            SetData("Bomber.NumBombs", 5)
            SetData("Text.kUtilityLaserSight", "Banana Strike")
            CopyContainer("kWeaponBananaBomb","kWeaponAirstrike")

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kUtilityLaserSight"

            Container.WeaponGraphicsResourceID = "Radio"
            Container.LifeTime = 2500
            Container.NumBomblets = 3
            Container.IsTargetingWeapon = true

            Container.IsBomberWeapon = true
            Container.IsAimedWeapon = false
            Container.HasAdjustableHerd = false

            Container.HasAdjustableFuse = false
            Container.AnimDraw = "DrawAirstrike"
            Container.AnimFire = "FireAirstrike"

            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == 4 then
            -- MineStrike

            SetData("Text.kUtilityLaserSight", "Mine Strike")
            CopyContainer("kWeaponLandmine","kWeaponAirstrike")
            SetData("Bomber.NumBombs", 5)

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kUtilityLaserSight"

            Container.WeaponGraphicsResourceID = "Radio"
            Container.IsTargetingWeapon = true
            Container.IsBomberWeapon = true

            Container.AnimDraw = "DrawAirstrike"
            Container.AnimFire = "FireAirstrike"
            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == 5 then
            -- Dynamite Strike

            SetData("Text.kUtilityLaserSight", "Dynamite Strike")
            CopyContainer("kWeaponDynamite","kWeaponAirstrike")
            SetData("Bomber.NumBombs", 5)

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kUtilityLaserSight"

            Container.WeaponGraphicsResourceID = "Radio"
            Container.IsTargetingWeapon = true
            Container.IsBomberWeapon = true

            Container.IsAimedWeapon = false
            Container.HasAdjustableHerd = false
            Container.HasAdjustableFuse = false

            Container.LifeTime = 8000
            Container.AnimDraw = "DrawAirstrike"
            Container.AnimFire = "FireAirstrike"

            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == 6 then
            -- Sheep Strike

            SetData("Text.kUtilityLaserSight", "Sheep Strike")
            CopyContainer("kWeaponGasCanister","kWeaponAirstrike")
            SetData("Bomber.NumBombs", 5)

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kUtilityLaserSight"

            Container.PayloadGraphicsResourceID = "Sheep"
            Container.LaunchSfx = "Sheep.Baa"
            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == 7 then
            -- Petrol Strike

            SetData("Text.kUtilityLaserSight", "Petrol Strike")
            CopyContainer("kWeaponPetrolBomb","kWeaponAirstrike")
            SetData("Bomber.NumBombs", 5)

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kUtilityLaserSight"

            Container.WeaponGraphicsResourceID = "Radio"
            Container.IsTargetingWeapon = true
            Container.IsBomberWeapon = true

            Container.IsAimedWeapon = false
            Container.HasAdjustableHerd = false
            Container.HasAdjustableFuse = false

            Container.AnimDraw = "DrawAirstrike"
            Container.AnimFire = "FireAirstrike"
            CloseContainer(ContainerLock)
            RepStrikeNum = -1

          elseif RepStrikeNum == -1 then
            SetData("Bomber.NumBombs", 5)

            CopyContainer("kWeaponGasCanister","kWeaponAirstrike")
            RepStrikeNum = 0
          end

        end

        --- --------
      end

    end

    function bomb()
      SendMessage("GameLogic.CreateRandomMine")
    end


    ------------------------------
    function DoWormpotOncePerTurnFunctions()
      local Wormpot = QueryContainer("WormPot")
      if Armageddon == true  then
        local i
        SendMessage("RandomNumber.Get")
        RawRand = GetData("RandomNumber.Uint")
        Rand = 1+math.mod(RawRand, 9)
        for i = 1,Rand do
          StartTimer("bomb", 500*i)
        end
      end

      if (Wormpot.CratesOnly == true) or (Wormpot.LotsOfCrates == true) then
        --   	Print("SendMessage(GameLogic.CrateShower)")
        SendMessage("GameLogic.CrateShower")
      end

      -- if Wormpot.EarthquakeEveryTurn == true then
        if (Wormpot.FallingHurtsMore == true) and (Wormpot.GodMode == true) then
          WaitUntilNoActivity = true
          WaitingForWormpot = true
          SetData("Earthquake.ImpulseMagnitudeMin", 0.01)
          SetData("Earthquake.ImpulseMagnitudeMax", 0.1)
          SetData("Earthquake.Length", 5000)
          SendMessage("Earthquake.Start")
          StartTimer("EndEarthquake", 5000)
        end

        if Wormpot.DoubleDamage == true then
          SetData("DoubleDamage", 1)
        end

        MegaProdnum = 0
        SpeedWalknum = 0
        Smokenum = 0

      end

      ------------------------------
      function SetWormpotModes()
        -- local WormpotLock, Wormpot
        -- local ContainerLock, Container

        local DamageScale, PowerScale

        Armageddon = false
        RepeatingStrike1 = false
        RepStrikeNum = 0
        RepeatingStrike2 = false
        MegaProd = false

        MegaProdnum = 0
        ZoomRope = false
        ZoomRopeOn = false
        DrakulaMode = false

        DrakulaActived = false
        SpeedWalk = false
        SpeedWalknum = 0
        SmokeBomb = false

        Smokenum = 0
        SpeedRope = false

        -- initialise wormpot global vars
        WaitingForWormpot = false


        local Wormpot = QueryContainer("WormPot")

        DamageScale = Wormpot.SuperScale
        PowerScale = Wormpot.PowerScale

        --------------------------------------------------------------------

        -- Super Firearms
        if Wormpot.SuperFirearms == true then
          if Wormpot.PowerFirearms == true then

            -- Mine Cluster Bomb
            local MegaMine = QueryContainer("kWeaponMegaMine")
            SetData("Text.kWeaponSheepStrike", "Mega Cluster Mine")

            --	CopyContainer("kWeaponMegaMine","kWeaponClusterGrenade")
            local ContainerLock, Container = EditContainer("kWeaponClusterGrenade")
            Container.DisplayName = "Text.kWeaponSheepStrike"

            Container.BombletWeaponName = "kWeaponClusterBomb"
            Container.PayloadGraphicsResourceID = MegaMine.PayloadGraphicsResourceID
            Container.WeaponGraphicsResourceID = MegaMine.WeaponGraphicsResourceID
            Container.NumBomblets = 4

            Container.Scale = MegaMine.Scale
            Container.BombletMaxSpeed = 0.2
            Container.BombletMaxConeAngle = 0.2
            Container.BombletMinSpeed = 0.18

            Container.AnimDraw = MegaMine.AnimDraw
            Container.AnimFire = MegaMine.AnimFire
            CloseContainer(ContainerLock)

            CopyContainer("kWeaponMegaMine","kWeaponClusterBomb")
            local ContainerLock, Container = EditContainer("kWeaponClusterBomb")

            Container.Radius = 6
            Container.NumBomblets = 4
            Container.BombletMaxSpeed = 0.22

            Container.LandDamageRadius = 40
            Container.ImpulseMagnitude = 0.4
            Container.BombletMaxConeAngle = 0.38

            Container.BombletMinSpeed = 0.2
            Container.Scale = 2.0
            Container.LifeTime = 1000

            Container.BombletWeaponName = "kWeaponLandmine"
            CloseContainer(ContainerLock)

            -- MagicBullet
            SetData("Text.kUtilityLaserSight", "Magic Bullet")

            local HomPigeon = QueryContainer("kWeaponHomingPidgeon")
            local ContainerLock, Container = EditContainer("kWeaponHomingMissile")

            Container.PayloadGraphicsResourceID = "Airstrike.Payload"
            Container.Scale = 2.0
            Container.LandDamageRadius = 200

            Container.WormDamageRadius = 200
            Container.ImpulseRadius = 200
            --- ?????	Container.CameraId = "PayloadTrackCamera"
            Container.ImpulseMagnitude = 0.5
            Container.WormDamageMagnitude = 100
            Container.LifeTime = 30000
            Container.HomingAcceleration = HomPigeon.HomingAcceleration
            Container.AvoidsLand = HomPigeon.AvoidsLand
            Container.ForwardLandAvoidanceForce = HomPigeon.ForwardLandAvoidanceForce
            Container.VerticalLandAvoidanceForce = HomPigeon.VerticalLandAvoidanceForce
            Container.VerticalLandAvoidanceDistance = HomPigeon.VerticalLandAvoidanceDistance
            Container.ForwardLandAvoidanceDistance = HomPigeon.ForwardLandAvoidanceDistance
            Container.DetonatesOnFirePress = true

            Container.MaxHomingSpeed = 3.0
            -- HomPigeon.MaxHomingSpeed
            Container.Stage3Duration = 0
            Container.Stage2Duration = 8000

            Container.Stage1Duration = 500
            -- HomPigeon.Stage1Duration * 2
            Container.OrientationProportion = HomPigeon.OrientationProportion
            Container.DisplayName = "Text.kUtilityLaserSight"

            CloseContainer(ContainerLock)

            -- ImpulseStike
            SetData("Text.kWeaponMingVase", "Impulse Stike")
            CopyContainer("kWeaponStickyBomb","kWeaponGasCanister")

            SetData("Bomber.NumBombs", 1)
            local ContainerLock, Container = EditContainer("kWeaponGasCanister")

            Container.DisplayName = "Text.kWeaponMingVase"
            Container.WeaponGraphicsResourceID = "Radio"
            Container.IsTargetingWeapon = true

            Container.IsBomberWeapon = true
            Container.IsAimedWeapon = false
            Container.HasAdjustableHerd = false

            Container.HasAdjustableFuse = false
            Container.LandDamageRadius = 1
            Container.Radius = 12

            --		Container.PayloadGraphicsResourceID = "LUNAR17"
            Container.PayloadGraphicsResourceID = "Teleporter"
            Container.WormDamageRadius = 1
            Container.WormDamageMagnitude = 25

            Container.ImpulseMagnitude = 5.0
            Container.DetonationFx = "Part_Teleport"
            --		Container.FxLocator = "Part_StarBridge"
            --	Container.DetonationSfx = "Target.Hit"
            --	Container.LaunchSfx = "Teleporter"

            --	Container.ArielFx = "Spark_Fountains"
            --	Container.ArielFx = "PTeleport_Trail"
            Container.ArielFx = "Part_StarBridge"

            Container.ImpulseRadius = 2000
            Container.Scale = 3.0

            Container.AnimDraw = "DrawAirstrike"
            Container.AnimFire = "FireAirstrike"
            CloseContainer(ContainerLock)
          end


          -- BazookaX5
          local ContainerLock, Container = EditContainer("kWeaponBazooka")
          Container.HasAdjustableHerd = true

          --	Container.LaserEffect = true
          CloseContainer(ContainerLock)
          -- ShotGun x 4
          local ContainerLock, Container = EditContainer("kWeaponShotgun")

          --	Container.LaserEffect = true
          Container.NumberOfBullets = 4
          CloseContainer(ContainerLock)
          -- Laser Uzi

          local ContainerLock, Container = EditContainer("kWeaponUzi")
          Container.LaserEffect = true

          CloseContainer(ContainerLock)

          --	ApplyWormpotDamageScale("kWeaponShotgun",DamageScale)
          ApplyWormpotDamageScale("kWeaponUzi",DamageScale)
          ApplyWormpotDamageScale("kWeaponBlowpipe",DamageScale)

        end

        if Wormpot.PowerFirearms == true then
          ApplyWormpotPowerScale("kWeaponShotgun",PowerScale)

          ApplyWormpotPowerScale("kWeaponUzi",PowerScale)
          CopyContainer("kWeaponUzi","kWeaponBlowpipe")
          --	local Petrol = QueryContainer("kWeaponClusterGrenade")

          -- Laser
          SetData("FE.Tip.Mstrike", "Laser")
          local ContainerLock, Container = EditContainer("kWeaponBlowpipe")

          Container.DisplayName = "FE.Tip.Mstrike"
          Container.NumberOfBullets = 0
          --		Container.Range = 200
          Container.Accuracy = 1.0

          Container.BulletRadius = 100
          Container.DischargeTime = 4000
          Container.WormDamageRadius = 0

          Container.DischargeSoundFX = "Dynamite.Fizz"
          Container.DischargeEndSoundFX = "Dynamite.Fizz"
          Container.CanBeFiredWhenWormMoving = true

          Container.LaserEffect = true
          Container.WeaponGraphicsResourceID = "NinjaRope.Gun"

          --	Container.DischargeFX = Petrol.DetonationFx
          Container.LandCollisionFX = "R_Explosion_Aftersmoke"

          Container.WormCollisionFX = Container.LandCollisionFX
          Container.SecondaryDischargeFX = ""
          Container.WaterCollisionFX = ""
          Container.AnimFire = ""

          Container.LandDamageRadius = 15
          --		Container.bCanMoveBetweenShots = true
          --		Container.Radius = 6
          --		Container.BombletMaxSpeed = 0.42
          --		Container.LandDamageRadius = 40
          --		Container.ImpulseMagnitude = 0.4
          --		Container.BombletMinSpeed = 0.4

          --		Container.Scale = 2.0
          --		Container.BombletWeaponName = "kWeaponLandmine"
          CloseContainer(ContainerLock)

          -- Fire
          --	local ContainerLock, Container = EditContainer("Part_PetrolBomb")
          --	Container.ParticleLife = 1000

          --	Container.ParticleSizeVelocityDelay = 25
          --	Container.ParticleSizeVelocityRandomise = 10
          --	CloseContainer(ContainerLock)

          local Index = 0
          local InvLas = QueryContainer("GM.SchemeData")

          local InventoryName = GetTeamInventoryName(Index)

          while InventoryName ~= "" do


            local lock, inventory = EditContainer(InventoryName)
            inventory.Blowpipe = InvLas.Girder.Ammo
            CloseContainer(lock)


            Index = Index + 1
            InventoryName = GetTeamInventoryName(Index)
          end
          --		ApplyWormpotPowerScale("kWeaponBlowpipe",PowerScale)

        end

        --------------------------------------------------------------------
        -- Super hand to hand weapons
        if Wormpot.SuperHand == true then

          ApplyWormpotDamageScale("kWeaponVikingAxe",DamageScale)
          ApplyWormpotDamageScale("kWeaponBaseballBat",DamageScale)
          ApplyWormpotDamageScale("kWeaponProd",DamageScale)

          ApplyWormpotDamageScale("kWeaponFirePunch",DamageScale)

          -- Zoom Camera
          local ContainerLock, Container = EditContainer("WormTrackCamera")

          Container.MinPreferredDistance = 1000
          --	Container.Camera2ObjectDistance = 1000
          CloseContainer(ContainerLock)
          --	SetData("Camera.Orbit.Height", 1000)
          --	SetData("Camera.Orbit.AdditionalRadius", 1000)


          -- Petrol X5
          local ContainerLock, Container = EditContainer("kWeaponPetrolBomb")
          Container.HasAdjustableHerd = true

          CloseContainer(ContainerLock)
          -- Grenade X5
          local ContainerLock, Container = EditContainer("kWeaponGrenade")

          Container.HasAdjustableHerd = true
          CloseContainer(ContainerLock)
          -- Super Baseball
          SetData("Text.Level.User", "Super Baseball")
          local ContainerLock, Container = EditContainer("kWeaponBananaBomb")

          Container.DisplayName = "Text.Level.User"
          Container.NumBomblets = 1
          Container.LifeTime = -1
          Container.HasAdjustableFuse = false
          Container.DetonatesOnExpiry = false

          Container.DetonatesOnWormImpact = true
          Container.DetonatesOnLandImpact = true
          Container.PayloadGraphicsResourceID = "BaseballBat"

          Container.BombletWeaponName = "kWeaponBananaBomb"
          Container.WeaponGraphicsResourceID = "BaseballBat"
          Container.BombletMaxSpeed = 0.3

          Container.ImpulseMagnitude = 0.4
          Container.BombletMaxConeAngle = 0.785
          Container.BombletMinSpeed = 0.1

          Container.AnimDraw = "DrawDynamite"
          CloseContainer(ContainerLock)

          if Wormpot.PowerHand == true then

            -- Super Jar
            SetData("Text.kWeaponSheepStrike", "Super Jar")
            CopyContainer("kWeaponBananaBomb","kWeaponClusterGrenade")
            local ContainerLock, Container = EditContainer("kWeaponClusterGrenade")

            Container.DisplayName = "Text.kWeaponSheepStrike"
            Container.NumBomblets = 7
            Container.HasAdjustableHerd = false
            Container.DetonatesOnFirePress = true

            Container.DetonatesOnWormImpact = false
            Container.DetonatesAtRest = true
            Container.LifeTime = -1
            Container.HasAdjustableFuse = false
            Container.DetonatesOnExpiry = false
            Container.PayloadGraphicsResourceID = "RedBull"

            Container.WeaponGraphicsResourceID = "RedBull"
            Container.BombletWeaponName = "kWeaponClusterBomb"
            Container.AnimDraw = "DrawMine"

            CloseContainer(ContainerLock)

            CopyContainer("kWeaponPetrolBomb","kWeaponClusterBomb")
            local ContainerLock, Container = EditContainer("kWeaponClusterBomb")

            Container.DetonationSfx = "ExplosionRegular"
            Container.LifeTime = 0
            Container.ImpulseRadius = 100

            Container.ImpulseMagnitude = 0.01
            CloseContainer(ContainerLock)

            -- Canonball
            SetData("Text.kUtilityLaserSight", "Cannonball")
            local ContainerLock, Container = EditContainer("kWeaponHolyHandGrenade")

            Container.DisplayName = "Text.kUtilityLaserSight"
            Container.NumBomblets = 1
            Container.LifeTime = -1

            Container.Scale = 6.0
            Container.Radius = 12
            Container.PayloadGraphicsResourceID = "Mortar.Payload"

            Container.PreDetonationTime = 0
            Container.PreDetonationSfx = ""
            Container.WeaponGraphicsResourceID = ""

            Container.BombletWeaponName = "kWeaponBananette"
            Container.BombletMaxSpeed = 0.3
            Container.BombletMaxConeAngle = 0.0

            Container.BombletMinSpeed = 0.1
            CloseContainer(ContainerLock)

            CopyContainer("kWeaponHolyHandGrenade","kWeaponBananette")

            local ContainerLock, Container = EditContainer("kWeaponBananette")
            Container.HasAdjustableFuse = false
            Container.DetonatesOnExpiry = false

            Container.DetonatesOnWormImpact = true
            Container.DetonatesOnLandImpact = true
            Container.BombletMaxSpeed = 0.3
            Container.ImpulseMagnitude = 0.4
            Container.BombletMaxConeAngle = 0.0
            Container.BombletMinSpeed = 0.1
            CloseContainer(ContainerLock)

            RepeatingStrike1 = true

            CopyContainer("kWeaponAirstrike","kWeaponGasCanister")

            -- Fire
            local ContainerLock, Container = EditContainer("Part_PetrolBomb")

            --	Container.ParticleLife = 5000
            --	Container.ParticleSizeVelocityDelay = 25
            --	Container.ParticleSizeVelocityRandomise = 10
            Container.ParticleCollisionWormDamageMagnitude = Container.ParticleCollisionWormDamageMagnitude * 3
            Container.ParticleCollisionWormImpulseMagnitude = Container.ParticleCollisionWormImpulseMagnitude * 3
            Container.ParticleCollisionWormImpulseYMagnitude = Container.ParticleCollisionWormImpulseYMagnitude * 3
            CloseContainer(ContainerLock)
          end
        end

        if Wormpot.PowerHand == true then
          ApplyWormpotPowerScale("kWeaponVikingAxe",PowerScale)

          ApplyWormpotPowerScale("kWeaponBaseballBat",PowerScale)
          ApplyWormpotPowerScale("kWeaponProd",PowerScale)
          -- Mega Prod

          MegaProd = true
          prodContainer = QueryContainer("kWeaponProd")
          tempMPMagnitude = prodContainer.ImpulseMagnitude
          tempMPRadius = prodContainer.Radius

          ApplyWormpotPowerScale("kWeaponFirePunch",PowerScale)

        end

        -------------------------------------------------------------------
        -- Super cluster weapons
        if Wormpot.SuperClusters == true then

          -- Minomet
          SetData("FE.Wormapedia.VaseTitle", "Minomet")
          local ContainerLock, Container = EditContainer("kWeaponBazooka")

          Container.DisplayName = "FE.Wormapedia.VaseTitle"
          Container.HasAdjustableHerd = true
          Container.NumBomblets = 1
          Container.LandDamageRadius = 1
          Container.WormDamageMagnitude = 0
          Container.ImpulseMagnitude = 0
          Container.WormDamageRadius = 1
          Container.ImpulseRadius = 1
          Container.IsAffectedByWind = false

          Container.PayloadGraphicsResourceID = "Landmine"
          Container.BombletWeaponName = "kWeaponLandmine"
          Container.DetonationSfx = ""
          Container.DetonationFx = ""

          Container.BombletMaxSpeed = 0.1
          Container.BombletMaxConeAngle = 0.4
          CloseContainer(ContainerLock)

          ApplyWormpotDamageScale("kWeaponClusterGrenade",DamageScale)

          -- ClusterBomb x 16
          local ContainerLock, Container = EditContainer("kWeaponClusterGrenade")
          Container.NumBomblets = 16

          Container.BombletMaxSpeed = 0.2
          CloseContainer(ContainerLock)
          -- ApplyWormpotDamageScale("kWeaponClusterBomb",DamageScale)

          ApplyWormpotDamageScale("kWeaponMortar",DamageScale)

          -- Mortar x 10
          local ContainerLock, Container = EditContainer("kWeaponMortar")
          Container.NumBomblets = 10

          CloseContainer(ContainerLock)
          -- ApplyWormpotDamageScale("kWeaponMortarBomblet",DamageScale)

          -- ApplyWormpotDamageScale("kWeaponAirstrike",DamageScale)
          -- Airstrike x10
          SetData("Bomber.NumBombs", 10)


          ApplyWormpotDamageScale("kWeaponBananaBomb",DamageScale)
          -- ApplyWormpotDamageScale("kWeaponBananette",DamageScale)
          -- BananaBomb X5
          local ContainerLock, Container = EditContainer("kWeaponBananaBomb")

          Container.HasAdjustableHerd = true
          CloseContainer(ContainerLock)

          if Wormpot.PowerClusters == true then


            ApplyWormpotDamageScale("kWeaponClusterBomb",DamageScale)

            -- SuperBananaBomb
            SetData("Text.kWeaponSheepStrike", "Super Banana Bomb")

            CopyContainer("kWeaponBananaBomb","kWeaponClusterGrenade")
            local ContainerLock, Container = EditContainer("kWeaponClusterGrenade")

            Container.DisplayName = "Text.kWeaponSheepStrike"
            Container.NumBomblets = 7
            Container.HasAdjustableHerd = false
            Container.DetonatesOnFirePress = true

            Container.DetonatesAtRest = true
            Container.HasAdjustableFuse = false
            Container.DetonatesOnExpiry = false
            Container.BombletWeaponName = "kWeaponClusterBomb"

            Container.LandDamageRadius = 150
            Container.WormDamageRadius = 150
            Container.BombletMaxSpeed = 0.4

            Container.BombletMinSpeed = 0.3
            Container.ImpulseRadius = 150
            Container.ImpulseMagnitude = 0.25

            CloseContainer(ContainerLock)

            CopyContainer("kWeaponClusterGrenade","kWeaponClusterBomb")
            local ContainerLock, Container = EditContainer("kWeaponClusterBomb")

            Container.NumBomblets = 0
            Container.ImpulseMagnitude = 0.1
            CloseContainer(ContainerLock)

            -- Mega Grenade

            SetData("Text.kWeaponMingVase", "Mega Grenade")
            local ContainerLock, Container = EditContainer("kWeaponGrenade")

            Container.DisplayName = "Text.kWeaponMingVase"
            Container.Scale = Container.Scale * 10
            Container.Radius = Container.Radius * 10

            Container.MaxPower = Container.MaxPower * 2
            Container.ImpulseMagnitude = 1.5
            Container.ImpulseRadius = Container.ImpulseRadius * 2.5

            Container.LandDamageRadius = Container.LandDamageRadius * 2.5
            Container.WormDamageRadius = Container.WormDamageRadius * 2.5

            Container.WormDamageMagnitude = Container.WormDamageMagnitude * 2
            CloseContainer(ContainerLock)

            -- Cluster Sticky Bomb

            SetData("Text.Level.User", "Cluster Sticky Bomb")
            CopyContainer("kWeaponStickyBomb","kWeaponGasCanister")
            local ContainerLock, Container = EditContainer("kWeaponGasCanister")

            Container.DisplayName = "Text.Level.User"
            Container.NumBomblets = 16
            Container.HasAdjustableHerd = false
            Container.DetonatesOnFirePress = true

            Container.DetonatesAtRest = true
            Container.HasAdjustableFuse = false
            Container.DetonatesOnExpiry = false
            Container.BombletWeaponName = "kWeaponStickyBomb"

            Container.LandDamageRadius = 0
            Container.WormDamageRadius = 0
            Container.BombletMaxSpeed = 0.2
            Container.BombletMaxConeAngle = 0.6
            Container.BombletMinSpeed = 0.2
            Container.ImpulseRadius = 0
            Container.ImpulseMagnitude = 0
            Container.Scale = Container.Scale * 4
            Container.Radius = Container.Radius * 4
            CloseContainer(ContainerLock)
          end

        end

        if Wormpot.PowerClusters == true then
          ApplyWormpotPowerScale("kWeaponClusterGrenade",PowerScale)

          ApplyWormpotPowerScale("kWeaponClusterBomb",PowerScale)
          ApplyWormpotPowerScale("kWeaponMortar",PowerScale)
          ApplyWormpotPowerScale("kWeaponMortarBomblet",PowerScale)

          ApplyWormpotPowerScale("kWeaponAirstrike",PowerScale)
          ApplyWormpotPowerScale("kWeaponBananaBomb",PowerScale)
          ApplyWormpotPowerScale("kWeaponBananette",PowerScale)

        end

        --------------------------------------------------------------------
        -- Super animal weapons
        if Wormpot.SuperAnimals == true then

          --	SetData("Gravity", 0,00001)
          --	local ContainerLock, Container = EditContainer("kWeaponAirstrike")
          --	Container.PayloadGraphicsResourceID = "Donkey"
          --      CloseContainer(ContainerLock)

          -- SheepStrike
          if Wormpot.PowerAnimals == true then



            SetData("Text.kWeaponSheepStrike", "Sheep Strike")
            --	CopyContainer("kWeaponOldWoman","kWeaponAirstrike")
            SetData("Bomber.NumBombs", 5)

            local ContainerLock, Container = EditContainer("kWeaponAirstrike")
            Container.DisplayName = "Text.kWeaponSheepStrike"

            Container.PayloadGraphicsResourceID = "Sheep"
            Container.LaunchSfx = "Sheep.Baa"
            CloseContainer(ContainerLock)


            --		local inventory = QueryContainer("GM.SchemeData")
            if Wormpot.GodMode == true then

              Armageddon = true

              CopyContainer("kWeaponConcreteDonkey","kWeaponLandmine")
              --	CopyContainer("kWeaponClusterGrenade","kWeaponLandmine")
              local ContainerLock, Container = EditContainer("kWeaponLandmine")

              Container.NumBomblets = 1
              Container.LifeTime = 1
              Container.PayloadGraphicsResourceID = "Donkey"

              Container.PreDetonationTime = 1
              Container.PreDetonationSfx = "Donkey"
              Container.WeaponGraphicsResourceID = ""

              Container.LaunchSfx = "Donkey"
              Container.BombletWeaponName = "kWeaponConcreteDonkey"
              Container.BombletMaxSpeed = 1
              Container.BombletMaxConeAngle = 0.0
              Container.BombletMinSpeed = 1
              CloseContainer(ContainerLock)

              local Lock, inventory = EditContainer("Inventory.WeaponDelays")

              inventory.Landmine = 99
              CloseContainer(Lock)

            end

          end

          --	local ContainerLock, Container = EditContainer("kWeaponAirstrike")

          --	Container.PayloadGraphicsResourceID = "Cow"
          --        CloseContainer(ContainerLock)

          -- Bazooka Sheep
          local ContainerLock, Container = EditContainer("kWeaponBazooka")

          Container.PayloadGraphicsResourceID = "SuperSheep"
          Container.LaunchSfx = "Sheep.Release"
          CloseContainer(ContainerLock)

          -- Pigeon Bomb

          SetData("Text.kWeaponMingVase", "Pigeon Bomb")
          --- FE.WeaponPanel
          --		SetData("Q.Bana", "Pigeon Bomb")
          local ContainerLock, Container = EditContainer("kWeaponBananaBomb")

          Container.DisplayName = "Text.kWeaponMingVase"
          Container.PayloadGraphicsResourceID = "Pigeon"
          Container.BombletWeaponName = "kWeaponHomingPidgeon"

          Container.WeaponGraphicsResourceID = "Pigeon"
          Container.AnimDraw = "DrawMine"
          CloseContainer(ContainerLock)

          --		-- OldWoman Bomb
          --		CopyContainer("kWeaponOldWoman","kWeaponClusterBomb")
          --		local ContainerLock, Container = EditContainer("kWeaponClusterBomb")
          --		Container.StartsArmed = false
          --	--	Container.IsDirectionalWeapon = false
          --		Container.LifeTime = 5000
          --	        CloseContainer(ContainerLock)


          ApplyWormpotDamageScale("kWeaponSheep",DamageScale)

          ApplyWormpotDamageScale("kWeaponSuperSheep",DamageScale)
          ApplyWormpotDamageScale("kWeaponOldWoman",DamageScale)
          ApplyWormpotDamageScale("kWeaponMadCow",DamageScale)

          if Armageddon == false then
            ApplyWormpotDamageScale("kWeaponConcreteDonkey",DamageScale)

          end
          --	ApplyWormpotDamageScale("kWeaponHomingPidgeon",DamageScale)
        end

        if Wormpot.PowerAnimals == true then

          ApplyWormpotPowerScale("kWeaponSheep",PowerScale)
          ApplyWormpotPowerScale("kWeaponSuperSheep",PowerScale)
          ApplyWormpotPowerScale("kWeaponOldWoman",PowerScale)

          ApplyWormpotPowerScale("kWeaponMadCow",PowerScale)
          if Armageddon == false then

            ApplyWormpotPowerScale("kWeaponConcreteDonkey",PowerScale)
          end
          ApplyWormpotPowerScale("kWeaponHomingPidgeon",PowerScale)

        end
        --------------------------------------------------------------------
        -- Super explosive weapons
        if Wormpot.SuperExplosives == true then


          if Wormpot.PowerExplosives == true then
            RepeatingStrike2 = true
            CopyContainer("kWeaponAirstrike","kWeaponGasCanister")
          end


          ApplyWormpotDamageScale("kWeaponBazooka",DamageScale)
          ApplyWormpotDamageScale("kWeaponDynamite",DamageScale)
          ApplyWormpotDamageScale("kWeaponGrenade",DamageScale)

          ApplyWormpotDamageScale("kWeaponHolyHandGrenade",DamageScale)
          ApplyWormpotDamageScale("kWeaponLandmine",DamageScale)
          ApplyWormpotDamageScale("kWeaponHomingMissile",DamageScale)

          ApplyWormpotDamageScale("kWeaponGasCanister",DamageScale)
          ApplyWormpotDamageScale("kWeaponLotteryStrike",DamageScale)
          ApplyWormpotDamageScale("kWeaponMegaMine",DamageScale)

          ApplyWormpotDamageScale("kWeaponStickyBomb",DamageScale)
          ApplyWormpotParticleDamageScale("ParticleGasTrail",DamageScale)
          ApplyWormpotParticleDamageScale("ParticleGas",DamageScale)

          ApplyWormpotDamageScale("kWeaponPetrolBomb",DamageScale)
          ApplyWormpotParticleDamageScale("Part_PetrolBomb",DamageScale)
          --		MaxPowerUp
          if Wormpot.PowerHand == true then

            local Power = 3.0
            ApplyWormpotMaxPower("kWeaponGrenade",Power)
            ApplyWormpotMaxPower("kWeaponBazooka",Power)

            ApplyWormpotMaxPower("kWeaponHolyHandGrenade",Power)
            ApplyWormpotMaxPower("kWeaponGasCanister",Power)
            ApplyWormpotMaxPower("kWeaponBananaBomb",Power)

            ApplyWormpotMaxPower("kWeaponClusterGrenade",Power)
            ApplyWormpotMaxPower("kWeaponPetrolBomb",Power)

          end

        end

        if Wormpot.PowerExplosives == true then
          ApplyWormpotPowerScale("kWeaponBazooka",PowerScale)

          ApplyWormpotPowerScale("kWeaponDynamite",PowerScale)
          ApplyWormpotPowerScale("kWeaponGrenade",PowerScale)
          ApplyWormpotPowerScale("kWeaponHolyHandGrenade",PowerScale)

          ApplyWormpotPowerScale("kWeaponLandmine",PowerScale)
          ApplyWormpotPowerScale("kWeaponHomingMissile",PowerScale)
          ApplyWormpotPowerScale("kWeaponGasCanister",PowerScale)

          ApplyWormpotPowerScale("kWeaponLotteryStrike",PowerScale)
          ApplyWormpotPowerScale("kWeaponMegaMine",PowerScale)
          ApplyWormpotPowerScale("kWeaponStickyBomb",PowerScale)

          ApplyWormpotParticlePowerScale("ParticleGasTrail",PowerScale)
          ApplyWormpotParticlePowerScale("ParticleGas",PowerScale)
          ApplyWormpotPowerScale("kWeaponPetrolBomb",PowerScale)

          ApplyWormpotParticlePowerScale("Part_PetrolBomb",PowerScale)
        end
        --------------------------------------------------------------------
        -- Super fire weapons ??
        --	if Wormpot.SuperFire == true then

          --		ApplyWormpotDamageScale("kWeaponPetrolBomb",DamageScale)
          --		ApplyWormpotParticleDamageScale("Part_PetrolBomb",DamageScale)
          --        end

          --	if Wormpot.PowerFire == true then
            --		ApplyWormpotPowerScale("kWeaponPetrolBomb",PowerScale)
            --		ApplyWormpotParticlePowerScale("Part_PetrolBomb",PowerScale)
            --	end

            --------------------------------------------------------------------

            -- One shot one kill (i.e set health to 1!)
            if  Wormpot.OneShotOneKill then
              --- +Health
              --	WormPoisonMagnitude
              local ContainerLock, Container = EditContainer("kWeaponBlowpipe")

              Container.WormPoisonMagnitude = -10
              Container.WormDamageMagnitude = -1
              CloseContainer(ContainerLock)

              local WormIndex = 0
              local WormContainerName = GetWormContainerName(WormIndex)


              while WormContainerName ~= "" do

                local lock, worm = EditContainer(WormContainerName)

                worm.Energy = 1
                CloseContainer(lock)

                WormIndex = WormIndex + 1
                WormContainerName = GetWormContainerName(WormIndex)

              end

              local Index = 0
              local InventoryName = GetTeamInventoryName(Index)


              while InventoryName ~= "" do

                local lock, inventory = EditContainer(InventoryName)

                inventory.Blowpipe = 4
                CloseContainer(lock)

                Index = Index + 1
                InventoryName = GetTeamInventoryName(Index)

              end

              local Lock, inventory = EditContainer("Inventory.WeaponDelays")
              local Delay = 5
              inventory.Bazooka = Delay
              inventory.Grenade = Delay
              inventory.ClusterGrenade = Delay
              inventory.Airstrike = Delay
              inventory.Dynamite = Delay
              inventory.HolyHandGrenade = Delay
              inventory.BananaBomb = Delay
              inventory.Landmine = Delay
              inventory.HomingMissile = Delay
              inventory.HomingPidgeon = Delay
              inventory.Sheep = Delay
              inventory.SuperSheep = Delay
              inventory.MadCow = Delay
              inventory.OldWoman = Delay
              inventory.Girder = Delay
              inventory.BridgeKit = Delay
              inventory.Shotgun = Delay
              inventory.Uzi = Delay
              inventory.NuclearBomb = Delay
              inventory.GasCanister = Delay
              inventory.FirePunch = Delay
              inventory.Prod = Delay
              inventory.PetrolBomb = Delay
              inventory.BaseballBat = Delay
              inventory.Mortar = Delay
              inventory.Earthquake = Delay
              inventory.VikingAxe = Delay
              inventory.ConcreteDonkey = Delay
              inventory.LotteryStrike = Delay
              inventory.DoctorsStrike = Delay
              inventory.MegaMine = Delay
              inventory.StickyBomb = Delay
              inventory.ScalesOfJustice = Delay
              inventory.ChangeWorm = Delay
              inventory.Redbull = Delay
              CloseContainer(Lock)


            end

            -----------------------------------------------------------
            if Wormpot.CratesOnly == true then
              SendMessage("GameLogic.ClearInventories")
              SendMessage("GameLogic.CrateShower")
            end
            -----------------------------------------------------------
            if Wormpot.LotsOfCrates == true then
              SendMessage("GameLogic.CrateShower")
            end
            --------------------------------------------------------------------
            -- Double Damage
            if Wormpot.DoubleDamage == true then
              SetData("DoubleDamage",1)
            end

            -----------------------------------------------------------
            -- Falling really hurts
            if Wormpot.FallingHurtsMore == true then
              -- Work Worm land Crash
              SetData("Worm.DeathLandDamageRadius", 100)

              if Wormpot.StickyMode == true then
                -- Gravity = -0.00025
                SetData("Gravity", -0.001)
                --Gravity Slow	-0.00015
                SetData("Gravity.Slow", -0.0005)
              else
                fDamage = GetData("Worm.FallDamageRatio")
                fDamage = fDamage * Wormpot.FallingScale
                SetData("Worm.FallDamageRatio",fDamage)
              end
            end

            -----------------------------------------------------------
            -- No retreat time
            if Wormpot.NoRetreatTime == true then
              -- Set the retreat time to zero

              SetData("DefaultRetreatTime",0)
              SetData("RetreatTime",0)
              SetData("TurnTime", 0)

              --- Camera E mode Off
              --   	SendStringMessage("Camera.Disable", "Blimp")

              -- Now remove the surrender from the inventories

              -- Team inventory
              local Index = 0
              local InventoryName = GetTeamInventoryName(Index)

              while InventoryName ~= "" do

                local Lock,Container = EditContainer(InventoryName)

                Container.Surrender = 0
                CloseContainer(Lock)

                Index = Index + 1
                InventoryName = GetTeamInventoryName(Index)

              end

              -- Alliance inventory
              Index = 0
              InventoryName = GetAllianceInventoryName(Index)


              while InventoryName ~= "" do
                local Lock,Container = EditContainer(InventoryName)

                Container.Surrender = 0
                CloseContainer(Lock)

                Index = Index + 1
                InventoryName = GetAllianceInventoryName(Index)

              end

              -- Worms
              Index = 0
              InventoryName = GetWormInventoryName(Index)


              while InventoryName ~= "" do

                local Lock,Container = EditContainer(InventoryName)

                Container.Surrender = 0
                CloseContainer(Lock)

                Index = Index + 1
                InventoryName = GetAllianceInventoryName(Index)

              end
            end
            -----------------------------------------------------------
            -- GodMode =)
            if Wormpot.DoubleHealthCrates == true then

              -- Team inventory
              local Index = 0
              local InventoryName = GetTeamInventoryName(Index)

              local InvScheme = QueryContainer("GM.SchemeData")

              while InventoryName ~= "" do


                SetFullDamage(InventoryName,Wormpot.NoRetreatTime,Wormpot.SuperAnimals,InvScheme.SuperSheep.Ammo)

                --	Wormpot.SpecialistWorms = 0
                Index = Index + 1
                InventoryName = GetTeamInventoryName(Index)
              end
            end
            -----------------------------------------------------------
            -- Energy or enemy
            if Wormpot.EnergyOrEnemy == true then

              local WormIndex = 0
              local WormContainerName = GetWormContainerName(WormIndex)

              local PoisonRate = GetData("Worm.Poison.Default")

              -- Poison each worm
              while WormContainerName ~= "" do


                local lock, worm = EditContainer(WormContainerName)
                worm.PoisonRate = PoisonRate
                CloseContainer(lock)


                SendIntMessage("Worm.Poison",WormIndex);

                WormIndex = WormIndex + 1
                WormContainerName = GetWormContainerName(WormIndex)

              end
            end
            -----------------------------------------------------------
            -- Slippy mode
            if Wormpot.SlippyMode == true then

              SetStickySlippyMode(Wormpot.SlippyModeScale)

              ZoomRope = true
              ZoomRopeOn = true

              local ropeContainer = QueryContainer("NinjaCamera")
              tempRopeZoom = ropeContainer.DistFromObject

              SetData("FE.Wormapedia.NapalmTitle", "Ninja Rope")

              local ContainerLock, Container = EditContainer("kUtilityNinjaRope")
              Container.DisplayName = "FE.Wormapedia.NapalmTitle"

              CloseContainer(ContainerLock)


              SetData("Ninja.NumShots", -1)
              SetData("Jetpack.MaxAltitude", 9000)

              SetData("Jetpack.InitFuel", 25000)
              if Wormpot.DoubleDamage == true then

                SetData("Ninja.MaxLength", 1000)
              end

              if Wormpot.WindEffectMore == true then

                SpeedRope = true
                SetData("Ninja.SwingAmount", 0.00016)
                SetData("Ninja.GravityMultiplier", 2.0)

              end

              --	if Wormpot.PowerHand == true then
                SpeedWalk = true
                TempWalkSpeed = GetData("Worm.Walk.Speed")


                --	end
              end

              -----------------------------------------------------------
              -- Sticky mode
              if Wormpot.StickyMode == true then

                -- Note - Sticky mode scale is a value between 0 & 1. Multiplying it by 20
                -- gives a 'better' result here ie is an arbitrary value that works ok!

                -- Rope Editor
                SetData("Ninja.NumShots", -1)


                -- Reverse
                ---	ReverseFR("kWeaponLandmine","kWeaponGrenade")
                ---	ReverseFR("kWeaponDynamite","kWeaponBazooka")
                -- Rope Bazooka
                CopyContainer("kWeaponBazooka","kWeaponDynamite")
                local ContainerLock, Container = EditContainer("kWeaponDynamite")

                Container.CanBeFiredWhenWormMoving = true
                CloseContainer(ContainerLock)

                SetStickySlippyMode(Wormpot.StickyModeScale)
              end


              -----------------------------------------------------------
              -- Wind effects more weapons
              if Wormpot.WindEffectMore == true then

                SetWeaponWind("kWeaponAirstrike",true)

                SetWeaponWind("kWeaponBananaBomb",true)
                SetWeaponWind("kWeaponBananette",true)
                SetWeaponWind("kWeaponClusterBomb",true)

                SetWeaponWind("kWeaponClusterGrenade",true)
                SetWeaponWind("kWeaponConcreteDonkey",true)
                --		SetWeaponWind("kWeaponGasCanister",true)

                SetWeaponWind("kWeaponGrenade",true)
                SetWeaponWind("kWeaponHolyHandGrenade",true)
                SetWeaponWind("kWeaponMadCow",true)

                SetWeaponWind("kWeaponMortarBomblet",true)
                SetWeaponWind("kWeaponOldWoman",true)
                SetWeaponWind("kWeaponPetrolBomb",true)

                SetWeaponWind("kWeaponSheep",true)
                SetWeaponWind("kWeaponLotteryStrike",true)
                SetWeaponWind("kWeaponDoctorsStrike",true)

                SetWeaponWind("kWeaponStickyBomb",true)

                -- Smoke Bomb --
                local SmokeGas = QueryContainer("kWeaponGasCanister")

                SmokeTempName = SmokeGas.DisplayName
                SmokeTempFx = SmokeGas.DetonationFx
                SmokeTempAFx = SmokeGas.ArielFx
                SmokeBomb = true

                --	CopyContainer("kWeaponEarthquake","kWeaponNuclearBomb")

                --CopyContainer("Part_Sinkhole","Part_OilySmoke")
                --		local ContainerLock, Container = EditContainer("Part_OilySmoke")
                --		Container.EmitterLifeTime = 60000
                --		Container.EmitterMaxParticles = 100
                --		Container.ParticleIsEffectedByWind = true
                --		Container.ParticleCollisionWormDamageMagnitude = -1
                --	Container.ParticleCollisionWormImpulseMagnitude = 0.5
                --	Container.ParticleCollisionWormDamageMagnitude = 10
                --	Container.ParticleCollisionWormImpulseYMagnitude = 0.5

                --	Container.ParticleCollisionWormPoisonMagnitude = -5
                --	Container.ParticleCollisonRadius = 5
                --	Container.ParticleCollisionRadius = 5
                --		CloseContainer(ContainerLock)

              end
              -----------------------------------------------------------
              -- David and goliath - One worm has lots of energy & the rest have little energy


              if ( Wormpot.DavidAndGoliath == true ) then

                local inventory = QueryContainer("GM.SchemeData")


                if inventory.Girder.Ammo == 0 then
                  -- Drakula Mode
                  DrakulaMode = true

                  DrakulaEnergyWorm = { 0, 0, 0, 0,
                  0, 0, 0, 0,
                  0, 0, 0, 0,
                  0, 0, 0, 0,
                  0, 0, 0}

                  DrakulaTeam = 0
                  DrakulaName = ""
                else

                  local GM = QueryContainer("GM.GameInitData")

                  local LargestTeam = GM.T1_NumOfWorms

                  -- First. Find the largest number of worms in one team
                  if GM.NumberOfTeams > 1 then
                    if GM.T2_NumOfWorms > LargestTeam then

                      LargestTeam = GM.T2_NumOfWorms
                    end
                  end

                  if GM.NumberOfTeams > 2 then
                    if GM.T3_NumOfWorms > LargestTeam then
                      LargestTeam = GM.T3_NumOfWorms
                    end
                  end

                  if GM.NumberOfTeams > 3 then
                    if GM.T4_NumOfWorms > LargestTeam then
                      LargestTeam = GM.T4_NumOfWorms
                    end
                  end

                  if GM.NumberOfTeams > 4 then
                    if GM.T5_NumOfWorms > LargestTeam then
                      LargestTeam = GM.T5_NumOfWorms
                    end
                  end

                  if GM.NumberOfTeams > 5 then
                    if GM.T6_NumOfWorms > LargestTeam then
                      LargestTeam = GM.T6_NumOfWorms
                    end
                  end

                  LargestTeam = LargestTeam * 100

                  if GM.NumberOfTeams >= 1 then
                    SetDavidAndGolithHealth(LargestTeam,GM.T1_NumOfWorms,0)
                  end

                  if GM.NumberOfTeams >= 2 then
                    SetDavidAndGolithHealth(LargestTeam,GM.T2_NumOfWorms,1)
                  end

                  if GM.NumberOfTeams >= 3 then
                    SetDavidAndGolithHealth(LargestTeam,GM.T3_NumOfWorms,2)
                  end

                  if GM.NumberOfTeams >= 4 then
                    SetDavidAndGolithHealth(LargestTeam,GM.T4_NumOfWorms,3)
                  end

                  if GM.NumberOfTeams >= 5 then
                    SetDavidAndGolithHealth(LargestTeam,GM.T5_NumOfWorms,4)
                  end

                  if GM.NumberOfTeams >= 6 then
                    SetDavidAndGolithHealth(LargestTeam,GM.T6_NumOfWorms,5)
                  end
                end
              end
              -----------------------------------------------------------

              if Wormpot.SpecialistWorms == true then

                --	local ContainerLock, Container = EditContainer("GasFire")
                --	Container.ParticleCollisionWormPoisonMagnitude = -4
                --	Container.ParticleCollisionWormImpulseMagnitude = 0.5

                --	Container.ParticleCollisionWormDamageMagnitude = 10
                --	Container.ParticleCollisionWormImpulseYMagnitude = 0.5
                --	Container.ParticleCollisionWormPoisonMagnitude = -5
                --	CloseContainer(ContainerLock)
                --	CopyContainer("Part_PetrolBomb","WeaponGasCan")

                ---	local ContainerLock, Container = EditContainer("ParticleGasTrail")
                ---	Container.ParticleCollisionWormPoisonMagnitude = -4
                ---	CloseContainer(ContainerLock)

                --	SetData("Worm.Poison.Default", -10)

                local inventory = QueryContainer("GM.SchemeData")

                if inventory.Girder.Ammo == 0 then
                  --- Camera Q mode Off
                  SendStringMessage("Camera.Disable", "Head")
                else
                  local GM = QueryContainer("GM.GameInitData")
                  if GM.NumberOfTeams >= 1 then
                    SetSpecialistTeam(GM.T1_NumOfWorms,0)
                  end
                  if GM.NumberOfTeams >= 2 then
                    SetSpecialistTeam(GM.T2_NumOfWorms,1)
                  end
                  if GM.NumberOfTeams >= 3 then
                    SetSpecialistTeam(GM.T3_NumOfWorms,2)
                  end
                  if GM.NumberOfTeams >= 4 then
                    SetSpecialistTeam(GM.T4_NumOfWorms,3)
                  end
                  if GM.NumberOfTeams >= 5 then
                    SetSpecialistTeam(GM.T5_NumOfWorms,4)
                  end
                  if GM.NumberOfTeams >= 6 then
                    SetSpecialistTeam(GM.T6_NumOfWorms,5)
                  end
                end
              end
              -- Hell vortex
              SetData("Particle.DetailObject", "Vortex")
              SetData("Particle.Name", "Part_Vortex")

              SendMessage("Particle.NewEmitter")
              SetData("Particle.DetailObject", "Sink")
              SetData("Particle.Name", "Part_Sinkhole")

              SendMessage("Particle.NewEmitter")
              SetData("Particle.DetailObject", "zprop1")
              SetData("Particle.Name", "Part_Propellor")

              SendMessage("Particle.NewEmitter")
              SetData("Particle.DetailObject", "zprop2")
              SetData("Particle.Name", "Part_Propellor")

              SendMessage("Particle.NewEmitter")
              SetData("Particle.DetailObject", "zprop3")
              SetData("Particle.Name", "Part_Propellor")

              SendMessage("Particle.NewEmitter")
              SetData("Particle.DetailObject", "zprop4")
              SetData("Particle.Name", "Part_Propellor")

              SendMessage("Particle.NewEmitter")
              SetData("Particle.DetailObject", "zprop5")
              SetData("Particle.Name", "Part_Propellor")

              SendMessage("Particle.NewEmitter")
              SetData("Particle.DetailObject", "zprop6")
              SetData("Particle.Name", "Part_Propellor")

              SendMessage("Particle.NewEmitter")
              SetData("Particle.DetailObject", "zprop7")
              SetData("Particle.Name", "Part_Propellor")

              SendMessage("Particle.NewEmitter")
              SetData("Particle.DetailObject", "zprop8")
              SetData("Particle.Name", "Part_Propellor")

              SendMessage("Particle.NewEmitter")
              SetData("Particle.DetailObject", "JET")
              SetData("Particle.Name", "Part_CrockJet")

              SendMessage("Particle.NewEmitter")


              SetData("Particle.DetailObject", "Sparks")
              SetData("Particle.Name", "Part_ElectricSpark")

              SendMessage("Particle.NewEmitter")
              SetData("Particle.DetailObject", "Birds")
              SetData("Particle.Name", "Part_BirdSky")

              SendMessage("Particle.NewEmitter")

              SetData("Particle.DetailObject", "Tap1")
              SetData("Particle.Name", "Part_Tap")

              SendMessage("Particle.NewEmitter")

            end

            function SetSpecialistTeam(NumOfWorms, LocalTeamIndex)

              local WormIndex = 0
              local WormContainerName = GetWormContainerName(WormIndex)

              local CurrentWorm = 1
              local SpecialistWormTypeTable = { 0,1,1,5,5,5, 0,2,3,6,6,6, 0,0,4,3,3,3, 0,0,0,4,4,4, 0,0,0,0,6,6, 0,0,0,0,0,3 }

              local Index = 0
              local SpecialistWormType = 0

              if NumOfWorms > 1 then
                while WormContainerName ~= "" do
                  local WormLock, WormContainer = EditContainer(WormContainerName)


                  if WormContainer.TeamIndex == LocalTeamIndex then
                    local InventoryLock, Inventory = EditContainer(GetTeamInventoryName(LocalTeamIndex))


                    index = ((CurrentWorm - 1)  * 6 ) + NumOfWorms
                    SpecialistWormType = SpecialistWormTypeTable[index]


                    if SpecialistWormType > 0 then
                      -- Firstly disable all this worms weapons
                      DisallowAllWeapons(WormContainer)

                      -- Now enable the appropriate weapons for the worm type

                      if SpecialistWormType == 3 or SpecialistWormType == 2 then

                        -- Melee
                        WormContainer.AllowShotgun = 1
                        WormContainer.AllowPetrolBomb = 1
                        WormContainer.AllowUzi = 1
                        WormContainer.AllowAirstrike = 1
                        WormContainer.AllowLandmine = 1
                        WormContainer.AllowFirePunch = 1
                        WormContainer.AllowProd = 1
                        WormContainer.AllowVikingAxe = 1

                        Inventory.Shotgun = -1
                        Inventory.PetrolBomb = 2
                        Inventory.Uzi = -1
                        Inventory.Airstrike = 1
                        Inventory.Landmine = 2
                        Inventory.FirePunch = -1
                        Inventory.Prod = -1
                        Inventory.VikingAxe = 2
                      end


                      if SpecialistWormType == 4 or SpecialistWormType == 2 then
                        -- Engineer

                        WormContainer.AllowNinjaRope = 1
                        WormContainer.AllowGirder = 1
                        WormContainer.AllowDynamite = 1
                        WormContainer.AllowParachute = 1
                        WormContainer.AllowBaseballBat = 1
                        WormContainer.AllowSheep = 1
                        WormContainer.AllowTeleport = 1

                        Inventory.NinjaRope = 5
                        Inventory.Girder = 3
                        Inventory.Dynamite = 1
                        Inventory.Parachute = 2
                        Inventory.BaseballBat = 1
                        Inventory.Sheep = 1
                        Inventory.Teleport = 2
                      end


                      if SpecialistWormType == 5 or SpecialistWormType == 1 then
                        -- CANNONADE
                        WormContainer.AllowBazooka = 1
                        WormContainer.AllowHomingMissile = 1
                        WormContainer.AllowMortar = 1
                        Inventory.Bazooka = -1
                        Inventory.HomingMissile = 1
                        Inventory.Mortar = 5
                      end


                      if SpecialistWormType == 6 or SpecialistWormType == 1 then
                        -- Grenadier
                        WormContainer.AllowGrenade = 1
                        WormContainer.AllowClusterGrenade = 1
                        Inventory.Grenade = -1
                        Inventory.ClusterGrenade = 3
                      end

                    end


                    CloseContainer(InventoryLock)
                    CurrentWorm = CurrentWorm + 1

                    if CurrentWorm > NumOfWorms then
                      CloseContainer(WormLock)

                      local DelayLock, DelayContainer = EditContainer("Inventory.WeaponDelays")

                      DelayContainer.HomingMissile = 1
                      DelayContainer.Airstrike = 5
                      CloseContainer(DelayLock)
                      break
                      -- ?
                    end
                  end

                  CloseContainer(WormLock)
                  WormIndex = WormIndex + 1
                  WormContainerName = GetWormContainerName(WormIndex)

                end
              end

            end
            ------------------------------
            function SetDavidAndGolithHealth(LargestTeam, NumOfWorms, LocalTeamIndex)

              local SmallHealth = LargestTeam / ( NumOfWorms * 2 );

              local BigHealth = LargestTeam - ( (NumOfWorms - 1) * SmallHealth);

              local WormIndex = 0
              local SetBigHealth = true
              local WormContainerName = GetWormContainerName(WormIndex)

              -- Set the health of each worm in the team
              while WormContainerName ~= "" do
                local lock, worm = EditContainer(WormContainerName)

                if worm.TeamIndex == LocalTeamIndex then
                  if SetBigHealth == true then

                    worm.Energy = BigHealth
                    SetBigHealth = false
                  else
                    worm.Energy = SmallHealth
                  end

                end

                CloseContainer(lock)

                WormIndex = WormIndex + 1
                WormContainerName = GetWormContainerName(WormIndex)

              end

              SetBigHealth = true
            end
            ------------------------------
            function ApplyWormpotDamageScale(ContainerName, Scale)
              local ContainerLock, Container = EditContainer(ContainerName)
              Container.WormDamageMagnitude = Container.WormDamageMagnitude * Scale
              Container.LandDamageRadius = Container.LandDamageRadius * Scale
              CloseContainer(ContainerLock)
            end
            ------------------------------
            function ApplyWormpotParticleDamageScale(ContainerName, Scale)
              local ContainerLock, Container = EditContainer(ContainerName)
              Container.ParticleCollisionWormDamageMagnitude = Container.ParticleCollisionWormDamageMagnitude * Scale
              CloseContainer(ContainerLock)
            end
            ------------------------------

            function ApplyWormpotPowerScale(ContainerName, Scale)
              local ContainerLock, Container = EditContainer(ContainerName)
              Container.ImpulseMagnitude = Container.ImpulseMagnitude * Scale
              CloseContainer(ContainerLock)
            end
            ------------------------------
            function ApplyWormpotParticlePowerScale(ContainerName, Scale)
              local ContainerLock, Container = EditContainer(ContainerName)
              Container.ParticleCollisionWormImpulseMagnitude = Container.ParticleCollisionWormImpulseMagnitude * Scale
              Container.ParticleCollisionWormImpulseYMagnitude = Container.ParticleCollisionWormImpulseYMagnitude * Scale
              CloseContainer(ContainerLock)
            end
            ------------------------------
            function ApplyWormpotMaxPower(ContainerName, Scale)
              local ContainerLock, Container = EditContainer(ContainerName)
              Container.MaxPower = Container.MaxPower * Scale
              CloseContainer(ContainerLock)
            end
            ------------------------------
            function SetWeaponWind(ContainerName, IsAffectedByWind)
              local ContainerLock, Container = EditContainer(ContainerName)
              Container.IsAffectedByWind = IsAffectedByWind
              CloseContainer(ContainerLock)
            end
            ------------------------------------------------------------------------
            function SetParticleWind(ContainerName, IsAffectedByWind)
              local ContainerLock, Container = EditContainer(ContainerName)
              Container.ParticleIsEffectedByWind = IsAffectedByWind
              CloseContainer(ContainerLock)
            end
            ------------------------------
            function SetStickySlippyMode(Scale)
              local f = GetData("Worm.SlideStopVel")
              f = f * Scale
              SetData("Worm.SlideStopVel",f)

              f = GetData("Worm.BounceMultiplier")
              f = f * ( 1.0 / Scale)

              SetData("Worm.BounceMultiplier",f)

              local muliplier = (1.0 / Scale)

              ApplyWormpotStickyScale("kWeaponBananaBomb", muliplier)
              ApplyWormpotStickyScale("kWeaponClusterGrenade", muliplier)
              ApplyWormpotStickyScale("kWeaponDynamite", muliplier)

              ApplyWormpotStickyScale("kWeaponGasCanister", muliplier)
              ApplyWormpotStickyScale("kWeaponGrenade", muliplier)
              ApplyWormpotStickyScale("kWeaponHolyHandGrenade", muliplier)

              -- Holly verry Sticky

            end
            ------------------------------
            function ApplyWormpotStickyScale(ContainerName, Scale)
              local ContainerLock, Container = EditContainer(ContainerName)
              local NewParallelMinBounceDamping = Container.ParallelMinBounceDamping * Scale
              local NewTangentialMinBounceDamping = Container.TangentialMinBounceDamping * Scale

              if NewParallelMinBounceDamping > 1 then
                NewParallelMinBounceDamping = 1
              end

              if NewTangentialMinBounceDamping > 1 then       NewTangentialMinBounceDamping = 1
            end


            Container.ParallelMinBounceDamping = NewParallelMinBounceDamping
            Container.TangentialMinBounceDamping = NewTangentialMinBounceDamping

            CloseContainer(ContainerLock)

          end
          ------------------------------

          function SetFullDamage(InventoryName,WType,Animaltype,Count)
            local lock, inventory = EditContainer(InventoryName)

            --	Count = inventory.SuperSheep
            inventory.SuperSheep = 0
            inventory.ConcreteDonkey = Count
            if WType == true then
              inventory.Bazooka = Count
              inventory.Grenade = Count
              inventory.ClusterGrenade = Count
              inventory.Airstrike = Count
              inventory.Dynamite = Count
              inventory.HolyHandGrenade = Count
              inventory.BananaBomb = Count
              inventory.Landmine = Count
              inventory.HomingMissile = Count
              inventory.HomingPidgeon = Count
              inventory.Sheep = Count
              inventory.SuperSheep = Count
              inventory.Parachute = Count
              inventory.Teleport = Count
              inventory.Jetpack = Count
              inventory.SkipGo = Count
              inventory.MadCow = Count
              inventory.OldWoman = Count
              inventory.Girder = Count
              inventory.BridgeKit = Count
              inventory.Shotgun = Count
              inventory.Uzi = Count
              inventory.NuclearBomb = Count
              inventory.GasCanister = Count
              inventory.NinjaRope = Count
              inventory.FirePunch = Count
              inventory.Prod = Count
              inventory.PetrolBomb = Count
              inventory.BaseballBat = Count
              inventory.Mortar = Count
              inventory.Earthquake = Count
              inventory.VikingAxe = Count
            end


            if WType == true then
              inventory.LowGravity = Count
              inventory.Freeze = Count
              inventory.QuickWalk = Count
              inventory.SuperSheep = Count
              inventory.LotteryStrike = Count
              inventory.DoctorsStrike = Count
              inventory.MegaMine = Count
              inventory.StickyBomb = Count
              inventory.ScalesOfJustice = Count
              inventory.Binoculars = Count
              inventory.Blowpipe = Count
              inventory.Surrender = Count
              inventory.ChangeWorm = Count
              inventory.Redbull = Count
            end


            if Animaltype == false then
              inventory.LowGravity = Count
              inventory.Freeze = Count
              inventory.QuickWalk = Count
              inventory.SuperSheep = Count
              inventory.LotteryStrike = Count
              inventory.DoctorsStrike = Count
              inventory.MegaMine = Count
              inventory.StickyBomb = Count
              inventory.ScalesOfJustice = Count
              inventory.Binoculars = Count
              inventory.Blowpipe = Count
              inventory.Surrender = Count
              inventory.ChangeWorm = Count
              inventory.Redbull = Count
            end

            CloseContainer(lock)

          end
          ------------------------------
          function DisallowAllWeapons(WormContainer)
            WormContainer.AllowBazooka = 0;
            WormContainer.AllowGrenade = 0;
            WormContainer.AllowClusterGrenade = 0;
            WormContainer.AllowAirstrike = 0;
            WormContainer.AllowDynamite = 0;
            WormContainer.AllowHolyHandGrenade = 0;
            WormContainer.AllowBananaBomb = 0;
            WormContainer.AllowLandmine = 0;
            WormContainer.AllowShotgun = 0;
            WormContainer.AllowUzi = 0;
            WormContainer.AllowBaseballBat = 0;
            WormContainer.AllowProd = 0;
            WormContainer.AllowVikingAxe = 0;
            WormContainer.AllowFirePunch = 0;
            WormContainer.AllowHomingMissile = 0;
            WormContainer.AllowMortar = 0;
            WormContainer.AllowHomingPidgeon = 0;
            WormContainer.AllowEarthquake = 0;
            WormContainer.AllowSheep = 0;
            WormContainer.AllowMineStrike = 0;
            WormContainer.AllowPetrolBomb = 0;
            WormContainer.AllowGasCanister = 0;
            WormContainer.AllowSheepStrike = 0;
            WormContainer.AllowMadCow = 0;
            WormContainer.AllowOldWoman = 0;
            WormContainer.AllowConcreteDonkey = 0;
            WormContainer.AllowNuclearBomb = 0;
            WormContainer.AllowArmageddon = 0;
            WormContainer.AllowMagicBullet = 0;
            WormContainer.AllowSuperSheep = 0;
            WormContainer.AllowGirder = 0;
            WormContainer.AllowBridgeKit = 0;
            WormContainer.AllowNinjaRope = 0;
            WormContainer.AllowParachute = 0;
            WormContainer.AllowScalesOfJustice = 0;
            WormContainer.AllowLowGravity = 0;
            WormContainer.AllowQuickWalk = 0;
            WormContainer.AllowLaserSight = 0;
            WormContainer.AllowTeleport = 0;
            WormContainer.AllowJetpack = 0;
            WormContainer.AllowSkipGo = 1;
            WormContainer.AllowSurrender = 1;
            WormContainer.AllowChangeWorm = 0;
            --  WormContainer.AllowRedbull = 0;

            WormContainer.AllowFreeze = 0;
            WormContainer.AllowLotteryStrike = 0;
            WormContainer.AllowDoctorsStrike = 0;
            WormContainer.AllowMegaMine = 0;
            WormContainer.AllowStickyBomb = 0;
            WormContainer.AllowBinoculars = 0;
            WormContainer.AllowBlowpipe = 0;
          end
